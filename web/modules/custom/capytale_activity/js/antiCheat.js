(function ($, Drupal, drupalSettings) {
  $(document).ready(function ($) {
    var $pwd = $("#passwd");
    $crypt = $("#crypt");
    var bcrypt = dcodeIO.bcrypt;
    var salt = bcrypt.genSaltSync(10);

    $pwd.on("input", function () {
      var hash = bcrypt.hashSync($pwd.val(), salt);
      $crypt.val(hash);
    });
  });
})(jQuery, Drupal, drupalSettings);
