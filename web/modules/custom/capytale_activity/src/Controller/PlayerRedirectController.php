<?php

namespace Drupal\capytale_activity\Controller;

use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException as NFE;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException as ADE;
/**
 * Controller de redirection vers le player
 */
class PlayerRedirectController implements ContainerInjectionInterface
{
  /** @var \Drupal\Core\PageCache\ResponsePolicy\KillSwitch $ks */
  protected $ks;
  /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $etm */
  protected $etm;
  /** @var \Drupal\capytale_activity\Activity\ActivityManager $am */
  protected $am;

  public function __construct(
    $ks,
    $etm,
    $am
  ) {
    $this->ks = $ks;
    $this->etm = $etm;
    $this->am = $am;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $c)
  {
    return new static(
      $c->get('page_cache_kill_switch'),
      $c->get('entity_type.manager'),
      $c->get('capytale_activity.manager'),
    );
  }

  /** 
   * réponds à la route '/c-act/n/{nid}/play/{mode}'
   */
  public function play($nid, $mode)
  {
    $this->ks->trigger();
    /** @var \Drupal\node\NodeStorageInterface $nodeStorage */
    $nodeStorage = $this->etm->getStorage('node');
    /** @var \Drupal\node\NodeInterface $n */
    $n = $nodeStorage->load($nid);
    if (!$n) throw new NFE();
    if (!$n->access('view')) throw new ADE();
    $ab = $this->am->getActivityBunch($n);
    if (!$ab) throw new NFE();
    $url = $ab->getPlayerUrl($mode);
    return new TrustedRedirectResponse($url);
  }
}
