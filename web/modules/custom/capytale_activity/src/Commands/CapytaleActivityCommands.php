<?php

namespace Drupal\capytale_activity\Commands;

use Drush\Commands\DrushCommands;

class CapytaleActivityCommands extends DrushCommands
{
  /** @var \Drupal\node\NodeStorageInterface $nsi */
  protected $nsi;
  /** @var \Drupal\Core\Database\Connection $db */
  protected $db;
  /** @var \Symfony\Component\DependencyInjection\ContainerInterface $container */
  protected $container;

  public function __construct()
  {
    parent::__construct();
    $container = \Drupal::getContainer();
    $this->nsi = $container->get('entity_type.manager')->getStorage('node');
    $this->db = $container->get('database');
    $this->container = $container;
  }

  /**
   * Command description here.
   *
   * @usage capytale:c-act-test
   *   Test command.
   * @param $arg
   * @options go Go!
   * @command capytale:c-act-test
   */
  public function c_act_test($arg = false, $options = ['go' => false])
  {
    if ($options['go'] == false) {
      $this->output()->writeln("No go!");
      return;
    }
    $this->output()->writeln("Go!");
    $this->output()->writeln(var_export($arg, true));
    /** @var \Drupal\capytale_activity\Evaluation\EvaluationServiceInterface $evalService */
    $evalService = $this->container->get('c_act.evaluationservice');
    $nid = \intval($arg);
    $node = $this->nsi->load($nid);
    $evals = $evalService->createEvaluation($node);
    $evals->setEvalLabel('test')
      ->setScore(12.5)
      ->setScoreMax(20)
      ->setActivityProgress('Started')
      ->setGradingProgress('fullygraded')
      ->setComment('commentaire de test')
      ->setLocalDateTime('2019-01-01T12:00:00');
    $evalService->saveEvaluation($evals);
    $this->output()->writeln("Done!");
  }
}
