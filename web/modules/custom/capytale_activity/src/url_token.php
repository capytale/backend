<?php
namespace Drupal\capytale_activity;
use Drupal\Core\Site\Settings;
class url_token {
    const KEY_SETTING = 'c-act_url_key';
    const METHOD = 'aes-128-ctr';
    public static function get_token($id, $uuid) {
        $key = Settings::get(self::KEY_SETTING);
        $salt = $key['salt'];
        $key = $key['key'];
        $id = \intval($id);
        $id_bs = \pack('J', $id);
        $iv = \md5($uuid . $salt, true);
        $extra = \ord(\md5($uuid, true)[0]);
        $chk = \pack('J', 0);
        $s_len = \strlen($salt);
        $s = $extra;
        $s_inc = 1 + ($extra % ($s_len - 1));
        for ($i = 0; $i < 8; ++$i) {
            $s = ($s + $s_inc) % $s_len;
            $chk[$i] =  \chr(\ord($id_bs[$i]) ^ \ord($salt[$s]));
        }
        $enc = \openssl_encrypt($id_bs . $chk, self::METHOD, $key, \OPENSSL_RAW_DATA, $iv);
        return \strtr(\base64_encode($iv . $enc . chr($extra)), '+/', '-_');
    }

    /**
     * @param \Drupal\node\NodeStorageInterface $nsi
     * @param string $tk
     * @return \Drupal\node\NodeInterface|false
     */
    public static function getNode($nsi, $tk) {
        $decodeTk = self::decode($tk);
        $nid = $decodeTk->id();
        if (!$nid) return false;
        /** @var \Drupal\node\NodeInterface $node */
        $node = $nsi->load($nid);
        if (!$node) return false;
        if (!$decodeTk->validate($node->uuid())) return false;
        return $node;
    }

    public static function decode($tk) {
        return new static($tk);
    }
    public function id() {
        if (! $this->_done) $this->do_decode();
        return $this->_id;
    }
    protected $_done = false;
    protected $_tk;
    protected $_id;
    protected $_extra;
    protected $_iv;
    protected function __construct($tk) {
        $this->_tk = \strtr($tk, '-_', '+/');
    }
    protected function do_decode() {
        if ($this->_done) return;
        $this->_done = true;
        $tk = \base64_decode($this->_tk, true);
        unset($this->_tk);
        $key = Settings::get(self::KEY_SETTING);
        $salt = $key['salt'];
        $key = $key['key'];
        $iv = \substr($tk, 0, 16);
        $enc = \substr($tk, 16, 16);
        $extra = \ord($tk[32]);
        $id_bs = \openssl_decrypt($enc, self::METHOD, $key, \OPENSSL_RAW_DATA, $iv);
        $chk = substr($id_bs, 8, 8);
        $id_bs = substr($id_bs, 0, 8);
        $s_len = \strlen($salt);
        $s = $extra;
        $s_inc = 1 + ($extra % ($s_len - 1));
        $ok = true;
        for ($i = 0; $i < 8; ++$i) {
            $s = ($s + $s_inc) % $s_len;
            if ($chk[$i] !== \chr(\ord($id_bs[$i]) ^ \ord($salt[$s]))) $ok = false;
        }
        if (!$ok) {
            $this->_id = false;
            return;
        }
        $this->_extra = $extra;
        $this->_iv = $iv;
        $this->_id = \unpack('J', $id_bs)[1];
    }
    public function validate($uuid) {
        $salt = Settings::get(self::KEY_SETTING)['salt'];
        if (! $this->_done) $this->do_decode();
        if (false === $this->_id) return false;
        $iv = \md5($uuid . $salt, true);
        $extra = \ord(\md5($uuid, true)[0]);
        $ok = true;
        if ($extra !== $this->_extra) $ok = false;
        if ($iv !== $this->_iv) $ok = false;
        return $ok;
    }
}