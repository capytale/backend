<?php

namespace Drupal\capytale_activity\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\capytale_activity\AntiCheat\AntiCheatDefinition as ACD;


/**
 * Field formatter "anti_cheat_default".
 *
 * @FieldFormatter(
 *   id = "anti_cheat_default",
 *   label = @Translation("AntiCheat default"),
 *   field_types = {
 *     "anti_cheat",
 *   }
 * )
 */
class AntiCheatDefaultFormatter extends FormatterBase
{

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings()
  {
    return array(
      'anti_cheat' => 'csv',
    ) + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state)
  {

    $output['anti_cheat'] = array(
      '#title' => t('AntiCheat'),
      '#type' => 'select',
      '#options' => array(
        'csv' => t('Comma separated values'),
        'list' => t('Unordered list'),
      ),
      '#default_value' => $this->getSetting('anti_cheat'),
    );

    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary()
  {

    $summary = array();

    // Determine ingredients summary.
    $anti_cheat_summary = FALSE;
    switch ($this->getSetting('anti_cheat')) {

      case 'csv':
        $anti_cheat_summary = 'Comma separated values';
        break;

      case 'list':
        $anti_cheat_summary = 'Unordered list';
        break;
    }

    // Display anti_cheat summary.
    if ($anti_cheat_summary) {
      $summary[] = t('AntiCheat display: @format', array(
        '@format' => t($anti_cheat_summary),
      ));
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode)
  {

    $output = array();

    // Iterate over every field item and build a renderable array
    // (I call them rarray for short) for each item.
    foreach ($items as $delta => $item) {

      $build = array();

      // Render AntiCheat.
      // Here as well, we follow the same format as above.
      // We build a container, within which, we render the property
      // label (AntiCheat) and the actual values for the anti_cheat
      // as per configuration.
      $anti_cheat_format = $this->getSetting('anti_cheat');
      $build['anti_cheat'] = array(
        '#type' => 'container',
        '#attributes' => array(
          'class' => array('anti_cheat'),
        ),
        'value' => array(
          '#type' => 'container',
          '#attributes' => array(
            'class' => array('field__item'),
          ),
          // The buildAntiCheat method takes responsibility of generating
          // markup for anti_cheat as per the format set in field
          // configuration. We use $this->getSetting('anti_cheat') above to
          // read the configuration.
          'text' => $this->buildAntiCheat($anti_cheat_format, $item),
        ),
      );

      $output[$delta] = $build;
    }

    return $output;
  }

  /**
   * Builds a renderable array or string of anti_cheat.
   *
   * @param string $format
   *   The format in which the anti_cheat are to be displayed.
   *
   * @return array
   *   A renderable array of anti_cheat.
   */
  public function buildAntiCheat($format, FieldItemInterface $item)
  {
    // Instead of having a switch-case we build a dynamic method name
    // as per a pre-determined format. In this way, if we will to add
    // a new format in the future, all we will have to do is create a
    // new method named "buildAntiCheatFormatName()".
    $callback = 'buildAntiCheat' . ucfirst($format);
    return $this->$callback($item);
  }

  /**
   * Format anti_cheat as CSV.
   */
  public function buildAntiCheatCsv(FieldItemInterface $item)
  {
    $anti_cheat = $item->getAntiCheatNames();
    #kint($item);

    $tab = [];
    foreach ($anti_cheat as $criterion => $description) {
      $tab[] = isset($item->$criterion) ? "{$anti_cheat[$criterion]}: {$item->$criterion}" : "";
    };

    return array(
      '#markup' => implode(', ', $tab),
    );
  }

  /**
   * Format anti_cheat as an unordered list.
   */
  public function buildAntiCheatList(FieldItemInterface $item)
  {
    // "item_list" is a very handy render type.
    return array(
      '#theme' => 'item_list',
      '#items' => $item->getAntiCheatNames(),
    );
  }
}
