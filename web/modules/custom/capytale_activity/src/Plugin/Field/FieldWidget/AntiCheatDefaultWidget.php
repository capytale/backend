<?php

namespace Drupal\capytale_activity\Plugin\Field\FieldWidget;

use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Field\WidgetInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\capytale_activity\AntiCheat\AntiCheatDefinition as ACD;


/**
 * Field widget "anti_cheat_default".
 *
 * @FieldWidget(
 *   id = "anti_cheat_default",
 *   label = @Translation("AntiCheat default"),
 *   field_types = {
 *     "anti_cheat",
 *   }
 * )
 */
class AntiCheatDefaultWidget extends WidgetBase implements WidgetInterface
{

    /**
     * {@inheritdoc}
     */
    public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state)
    {

        $anti_cheat = ACD::ANTICHEAT_DEFINITION;

        // $item is where the current saved values are stored.
        $item = &$items[$delta];

        // $element is already populated with #title, #val, #delta,
        // #required, #field_parents, etc.
        //$element += array(
        //    '#type' => 'fieldset',
        //);

        $element['anti_cheat'] = array(
            '#type' => 'fieldset',
            '#process' => array(__CLASS__ . '::processAntiCheatFieldset'),
        );
        $element['#attached']['library'][] = 'capytale_activity/bcryptjs';
        $element['#attached']['library'][] = 'capytale_activity/antiCheat';

        foreach ($anti_cheat as $key => $val) {
            $default = isset($item->$key) ? $item->$key : $val['form']['#default_value'];
            $element['anti_cheat'][$key] = $val['form'];
            $element['anti_cheat'][$key]['#default_value'] = $default;
        }

        $element['anti_cheat']['password'] = array(
            '#id' => "passwd",
            '#type' => 'password',
            '#title' => 'Code de déverrouillage<br/><span class="small ital">Obligatoire pour débloquer l\'écran des élèves</small>',
            '#size' => 2,
            '#attributes' => array('class' => array('passwd-label')),
            '#states' => [
                'enabled' => [':input[id=enable]' => ['checked' => TRUE], ],
                'required' => [':input[id=enable]' => ['checked' => TRUE], ]
            ],
        );
        $element['anti_cheat']['password']['#placeholder'] = isset($item->$key) ? '••••' : '';

        return $element;
    }

    /**
     * Form widget process callback.
     */
    public static function processAntiCheatFieldset($element, FormStateInterface $form_state, array $form)
    {

        $elem_key = array_pop($element['#parents']);

        return $element;
    }
}
