<?php

namespace Drupal\capytale_activity\Plugin\Field\FieldType;

use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\capytale_activity\AntiCheat\AntiCheatDefinition as ACD;

/**
 * Field type "anti_cheat".
 *
 * @FieldType(
 *   id = "anti_cheat",
 *   label = @Translation("anti_cheat"),
 *   description = @Translation("Anti_cheat field."),
 *   category = @Translation("AntiCheat"),
 *   default_widget = "anti_cheat_default",
 *   default_formatter = "anti_cheat_default",
 * )
 */
class AntiCheat extends FieldItemBase implements FieldItemInterface
{

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition)
  {
    $output = array();

    // Make a column for every key.
    foreach (ACD::ANTICHEAT_DEFINITION as $key => $val) {
      $output['columns'][$key] = $val['schema'];
    }

    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition)
  {
    foreach (ACD::ANTICHEAT_DEFINITION as $key => $val) {
      $properties[$key] = DataDefinition::create($val['type'])
        ->setLabel($val['form']['#title']);
    }

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty()
  {
    $item = $this->getValue();

    foreach (ACD::ANTICHEAT_DEFINITION as $key => $val) {
      if (isset($item[$key]) && $item[$key] != 0) {
        return FALSE;
      }
    }

    return TRUE;
  }

  /**
   * Returns the array of anti_cheat.
   */
  public function getAntiCheatNames()
  {
    $output = array();

    foreach (ACD::ANTICHEAT_DEFINITION as $key => $val) {
      if ($this->$key) {
        $output[$key] = $val['form']['#title'];
      }

      return $output;
    }
  }
}
