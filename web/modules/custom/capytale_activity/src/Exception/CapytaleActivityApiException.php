<?php

namespace Drupal\capytale_activity\Exception;

use Symfony\Component\HttpFoundation\Response as R;

/**
 * Class CapytaleActivityApiException.
 * 
 * Erreurs renvoyées par l'API.
 */
class CapytaleActivityApiException extends \Exception
{
  protected $httpStatus;
  /**
   * Access denied.
   */
  const ACCESS_DENIED = 1;
  /**
   * Bad request.
   */
  const BAD_REQUEST = 2;
  /**
   * Not found.
   */
  const NOT_FOUND = 3;

  public function __construct($code, $msg = "")
  {
    list($status, $msg) = $this->format($code, $msg);
    $this->httpStatus = $status;
    parent::__construct($msg, $code);
  }

  private function format($code, $msg)
  {
    switch ($code) {
      case self::ACCESS_DENIED:
        $msg = empty($msg) ? 'Accès refusé' : $msg;
        return [R::HTTP_FORBIDDEN, $msg];
      case self::BAD_REQUEST:
        $msg = empty($msg) ? 'Requête invalide' : $msg;
        return [R::HTTP_BAD_REQUEST, $msg];
      case self::NOT_FOUND:
        $msg = empty($msg) ? 'Ressource non trouvée' : $msg;
        return [R::HTTP_NOT_FOUND, $msg];
    }
    if (empty($msg)) $msg = 'Erreur inconnue';
    return [R::HTTP_BAD_REQUEST, $msg];
  }

  public function getHttpStatus()
  {
    return $this->httpStatus;
  }
}
