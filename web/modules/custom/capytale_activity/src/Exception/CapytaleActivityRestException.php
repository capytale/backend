<?php

namespace Drupal\capytale_activity\Exception;

class CapytaleActivityRestException extends \Exception
{
    const BAD_REQUEST = 0;
    const FILE_WRITE = 1;
    const SIZE = 2;
    const READ_ONLY = 3;
    const REP_IS_FILE = 4;
    const MIGRATION_PENDING = 5;
    const BAD_LENGTH = 6;
    public function __construct($code, $msg = "")
    {
        parent::__construct($this->format($code, $msg), $code);
    }

    protected function format($code, $msg)
    {
        switch ($code) {
            case self::BAD_REQUEST:
                return empty($msg) ? 'Bad request.' : $msg;
            case self::FILE_WRITE:
                return 'Impossible d\'écrire le fichier ' . $msg;
            case self::SIZE:
                return 'La longueur des données dépace la limite';
            case self::READ_ONLY:
                return $msg . ' est en lecture seule';
            case self::REP_IS_FILE:
                return $msg . '  est un fichier mais devrait être un répertoire';
            case self::MIGRATION_PENDING:
                return $msg . 'La migration est en cours. Veuillez rééssayer';
            case self::BAD_LENGTH:
                return $msg . ' Le flux n\'a pas la longueur annoncée.';
        }
        if (!empty($msg)) return $msg;
        return 'Erreur inconnue';
    }
}
