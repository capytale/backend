<?php

namespace Drupal\capytale_activity\Evaluation\Impl;

use Drupal\capytale_activity\Evaluation\NewEvaluationInterface;
use Drupal\capytale_activity\Exception\CapytaleActivityException;

class NewEvaluation implements NewEvaluationInterface
{
    /** @var \Drupal\node\NodeInterface $node Le student_assignment */
    protected $node;
    /** @var int $graderUid */
    protected $graderUid;
    /** @var string $evalLabel */
    protected $evalLabel;
    /** @var string $evalTitle */
    protected $evalTitle;
    /** @var int $revisionNumber */
    protected $revisionNumber;
    /** @var float $score */
    protected $score;
    /** @var float $scoreMax */
    protected $scoreMax;
    /** @var string $scoreLiteral */
    protected $scoreLiteral;
    /** @var string $comment */
    protected $comment;
    /** @var int $unixTimeStampServer */
    protected $unixTimeStampServer;
    /** @var string $localDateTime */
    protected $localDateTime;

    /** @inheritdoc */
    public function __construct($unixTimeStampServer, $node)
    {
        $this->unixTimeStampServer = $unixTimeStampServer;
        $this->node = $node;
    }

    /** @inheritdoc */
    public function getActivityNid()
    {
        return $this->node->field_activity->target_id;
    }

    /** @inheritdoc */
    public function getAssignmentNid()
    {
        return $this->node->id();
    }

    /** @inheritdoc */
    public function getUid()
    {
        return $this->node->getOwnerId();
    }

    /** @inheritdoc */
    public function getGraderUid()
    {
        return $this->graderUid;
    }

    /** @inheritdoc */
    public function getEvalLabel()
    {
        if (empty($this->evalLabel))
            return self::DEFAULT_LABEL;
        else
            return $this->evalLabel;
    }

    /** @inheritdoc */
    public function getEvalTitle()
    {
        if (empty($this->evalTitle))
            return null;
        else
            return $this->evalTitle;
    }

    /** @inheritdoc */
    public function getRevisionNumber()
    {
        return $this->revisionNumber;
    }

    /** @inheritdoc */
    public function getScore()
    {
        return $this->score;
    }

    /** @inheritdoc */
    public function getScoreMax()
    {
        return $this->scoreMax;
    }

    /** @inheritdoc */
    public function getScoreLiteral()
    {
        if (empty($this->scoreLiteral))
            return null;
        else
            return $this->scoreLiteral;
    }

    /** @inheritdoc */
    public function getComment()
    {
        if (empty($this->comment))
            return null;
        else
            return $this->comment;
    }

    /** @inheritdoc */
    public function getUnixTimeStampServer()
    {
        return $this->unixTimeStampServer;
    }

    /** @inheritdoc */
    public function getLocalDateTime()
    {
        return $this->localDateTime;
    }

    /** @inheritdoc */
    public function setGraderUid($graderUid)
    {
        $this->graderUid = \intval($graderUid);
        return $this;
    }

    /** @inheritdoc */
    public function setEvalLabel($evalLabel)
    {
        $label = \Drupal\Component\Utility\Html::escape($evalLabel);
        if (empty($label)) throw new CapytaleActivityException('Le label de l\'évaluation ne peut pas être vide.');
        $this->evalLabel = $label;
        return $this;
    }

    /** @inheritdoc */
    public function setEvalTitle($evalTitle)
    {
        $this->evalTitle = \Drupal\Component\Utility\Xss::filter($evalTitle);
        return $this;
    }

    /** @inheritdoc */
    public function setScore($score)
    {
        $this->score = \floatval($score);
        return $this;
    }

    /** @inheritdoc */
    public function setScoreMax($scoreMax)
    {
        $this->scoreMax = \floatval($scoreMax);
        return $this;
    }

    /** @inheritdoc */
    public function setScoreLiteral($scoreLiteral)
    {
        $this->scoreLiteral = \Drupal\Component\Utility\Xss::filter($scoreLiteral);
        return $this;
    }

    /** @inheritdoc */
    public function setComment($comment)
    {
        $this->comment = \Drupal\Component\Utility\Xss::filter($comment);
        return $this;
    }

    /** @inheritdoc */
    public function setLocalDateTime($localDateTime)
    {
        $d = new \DateTime($localDateTime);
        $this->localDateTime = $d->format(\DateTime::RFC3339);
        return $this;
    }

    /** @inheritdoc */
    public function isEmpty()
    {
        return (!isset($this->score)) && empty($this->scoreLiteral) && empty($this->comment);
    }
}
