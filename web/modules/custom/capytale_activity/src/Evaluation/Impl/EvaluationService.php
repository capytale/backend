<?php

namespace Drupal\capytale_activity\Evaluation\Impl;

use Drupal\capytale_activity\Evaluation\EvaluationServiceInterface;
use Drupal\capytale_activity\Evaluation\Impl\Evaluation;

class EvaluationService implements EvaluationServiceInterface
{
    const TABLE = 'capytale_evaluation';
    const TABLE_HISTORIQUE = 'capytale_evaluation_historique';

    /** @var \Drupal\Core\Database\Connection $db */
    protected $db;

    /** @var \Drupal\Component\Datetime\TimeInterface */
    protected $timeService;

    /**
     * @param \Drupal\Core\Database\Connection $db
     * @param \Drupal\Component\Datetime\TimeInterface $timeService
     */
    public function __construct($db, $timeService)
    {
        $this->db = $db;
        $this->timeService = $timeService;
    }

    /** @inheritdoc */
    public function createEvaluation($node)
    {
        return new NewEvaluation($this->timeService->getRequestTime(), $node);
    }

    /** @inheritdoc */
    public function saveEvaluation($evaluation)
    {
        $trans = $this->db->startTransaction();
        try {
            $tbl = self::TABLE;
            $sql =
                "SELECT revisionNumber
FROM {{$tbl}}
WHERE
  activityNid = :activityNid AND
uid = :uid AND
  evalLabel = :evalLabel";

            $res = $this->db->query($sql, [
                ':activityNid' => $evaluation->getActivityNid(),
                ':uid' => $evaluation->getUid(),
                ':evalLabel' => $evaluation->getEvalLabel(),
            ]);

            $revisionNumber = $res->fetchField();
            if ($revisionNumber === false) {
                $revisionNumber = 1;
            } else {
                $revisionNumber++;
            }

            $sql =
                "INSERT INTO {{$tbl}}
(
activityNid,
assignmentNid,
uid,
graderUid,
evalLabel,
evalTitle,
revisionNumber,
score,
scoreMax,
scoreLiteral,
comment,
unixTimeStampServer,
localDateTime
)
VALUES
(
:activityNid,
:assignmentNid,
:uid,
:graderUid,
:evalLabel,
:evalTitle,
:revisionNumber,
:score,
:scoreMax,
:scoreLiteral,
:comment,
:unixTimeStampServer,
:localDateTime)
ON DUPLICATE KEY
UPDATE
assignmentNid = :assignmentNid,
graderUid = :graderUid,
evalTitle = :evalTitle,
revisionNumber = :revisionNumber,
score = :score,
scoreMax = :scoreMax,
scoreLiteral = :scoreLiteral,
comment = :comment,
unixTimeStampServer = :unixTimeStampServer,
localDateTime = :localDateTime";

            $this->db->query($sql, [
                ':activityNid' => $evaluation->getActivityNid(),
                ':assignmentNid' => $evaluation->getAssignmentNid(),
                ':uid' => $evaluation->getUid(),
                ':graderUid' => $evaluation->getGraderUid(),
                ':evalLabel' => $evaluation->getEvalLabel(),
                ':evalTitle' => $evaluation->getEvalTitle(),
                ':revisionNumber' => $revisionNumber,
                ':score' => $evaluation->getScore(),
                ':scoreMax' => $evaluation->getScoreMax(),
                ':scoreLiteral' => $evaluation->getScoreLiteral(),
                ':comment' => $evaluation->getComment(),
                ':unixTimeStampServer' => $evaluation->getUnixTimeStampServer(),
                ':localDateTime' => $evaluation->getLocalDateTime(),
            ]);

            $tbl = self::TABLE_HISTORIQUE;
            $sql =
                "INSERT INTO {{$tbl}}
(activityNid,
assignmentNid,
uid,
graderUid,
evalLabel,
revisionNumber,
score,
scoreMax,
scoreLiteral,
comment,
unixTimeStampServer,
localDateTime
)
VALUES
(:activityNid,
:assignmentNid,
:uid,
:graderUid,
:evalLabel,
:revisionNumber,
:score,
:scoreMax,
:scoreLiteral,
:comment,
:unixTimeStampServer,
:localDateTime)";

            $this->db->query($sql, [
                ':activityNid' => $evaluation->getActivityNid(),
                ':assignmentNid' => $evaluation->getAssignmentNid(),
                ':uid' => $evaluation->getUid(),
                ':graderUid' => $evaluation->getGraderUid(),
                ':evalLabel' => $evaluation->getEvalLabel(),
                ':revisionNumber' => $revisionNumber,
                ':score' => $evaluation->getScore(),
                ':scoreMax' => $evaluation->getScoreMax(),
                ':scoreLiteral' => $evaluation->getScoreLiteral(),
                ':comment' => $evaluation->getComment(),
                ':unixTimeStampServer' => $evaluation->getUnixTimeStampServer(),
                ':localDateTime' => $evaluation->getLocalDateTime(),
            ]);


            unset($trans);
        } catch (\Exception $e) {
            $trans->rollback();
            throw $e;
        }
    }

    /** @inheritdoc */
    public function saveEvaluations($evaluations)
    {
        foreach ($evaluations as $eval) {
            $this->saveEvaluation($eval);
        }
    }

    /** @inheritdoc */
    public function getEvaluations($nid, $uid)
    {
        $tbl = self::TABLE;
        $sql =
            "SELECT
activityNid,
assignmentNid,
uid,
graderUid,
evalLabel,
evalTitle,
revisionNumber,
score,
scoreMax,
scoreLiteral,
comment,
unixTimeStampServer,
localDateTime
FROM {{$tbl}}
WHERE
 activityNid = :activityNid AND uid = :uid";

        $res = $this->db->query($sql, [
            ':activityNid' => $nid,
            ':uid' => $uid,
        ]);

        $evaluations = [];
        while ($row = $res->fetchObject()) {
            $evaluations[] = new Evaluation($row);
        }
        return $evaluations;
    }
}
