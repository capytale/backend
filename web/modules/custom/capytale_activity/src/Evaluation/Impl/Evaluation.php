<?php

namespace Drupal\capytale_activity\Evaluation\Impl;

use Drupal\capytale_activity\Evaluation\EvaluationInterface;

class Evaluation implements EvaluationInterface
{

    private $activityNid;
    private $assignmentNid;
    private $uid;
    private $graderUid;
    private $evalLabel;
    private $evalTitle;
    private $revisionNumber;
    private $score;
    private $scoreMax;
    private $scoreLiteral;
    private $comment;
    private $unixTimeStampServer;
    private $localDateTime;

    /** @inheritdoc */
    public function __construct($rec)
    {
        $this->activityNid = (int)$rec->activityNid;
        $this->assignmentNid = (int)$rec->assignmentNid;
        $this->uid = (int)$rec->uid;
        $this->graderUid = (int)$rec->graderUid;
        $this->evalLabel = $rec->evalLabel;

        if (empty($rec->evalTitle)) $this->evalTitle = null;
        else $this->evalTitle = $rec->evalTitle;

        $this->revisionNumber = (int)$rec->revisionNumber;
        $this->score = (int)$rec->score;
        $this->scoreMax = (int)$rec->scoreMax;

        if (empty($rec->scoreLiteral)) $this->scoreLiteral = null;
        else $this->scoreLiteral = $rec->scoreLiteral;

        if (empty($rec->comment)) $this->comment = null;
        else $this->comment = $rec->comment;

        $this->unixTimeStampServer = (int)$rec->unixTimeStampServer;

        if (empty($rec->localDateTime)) $this->localDateTime = null;
        else $this->localDateTime = $rec->localDateTime;
    }

    /** @inheritdoc */
    public function getActivityNid()
    {
        return $this->activityNid;
    }

    /** @inheritdoc */
    public function getAssignmentNid()
    {
        return $this->assignmentNid;
    }

    /** @inheritdoc */
    public function getUid()
    {
        return $this->uid;
    }

    /** @inheritdoc */
    public function getGraderUid()
    {
        return $this->graderUid;
    }

    /** @inheritdoc */
    public function getEvalLabel()
    {
        return $this->evalLabel;
    }

    /** @inheritdoc */
    public function getEvalTitle()
    {
        return $this->evalTitle;
    }

    /** @inheritdoc */
    public function getRevisionNumber()
    {
        return $this->revisionNumber;
    }

    /** @inheritdoc */
    public function getScore()
    {
        return $this->score;
    }

    /** @inheritdoc */
    public function getScoreMax()
    {
        return $this->scoreMax;
    }

    /** @inheritdoc */
    public function getScoreLiteral()
    {
        return $this->scoreLiteral;
    }

    /** @inheritdoc */
    public function getComment()
    {
        return $this->comment;
    }

    /** @inheritdoc */
    public function getUnixTimeStampServer()
    {
        return $this->unixTimeStampServer;
    }

    /** @inheritdoc */
    public function getLocalDateTime()
    {
        return $this->localDateTime;
    }
}
