<?php

namespace Drupal\capytale_activity\Evaluation;

interface EvaluationInterface
{
    /**
     * Label pour une activité qui ne contient qu'une seule évaluation.
     */
    const DEFAULT_LABEL = '_default';
    /**
     * Label pour l'évaluation manuelle globale de la copie.
     */
    const REVIEW_LABEL = '_review';
    /**
     * Retourne le nid de l'activity concerné par l'évaluation.
     * 
     * @return int
     */
    public function getActivityNid();

    /**
     * Retourne le nid du student_assignment concerné par l'évaluation.
     * 
     * @return int
     */
    public function getAssignmentNid();

    /**
     * Retourne l'uid de l'utilisateur évalué dans l'activity.
     * 
     * @return int
     */
    public function getUid();

    /**
     * Retourne l'uid de l'utilisateur qui a éxécuté le test.
     * 
     * @return int
     */
    public function getGraderUid();

    /**
     * Retourne le label de l'évaluation.
     * 
     * @return string
     */
    public function getEvalLabel();

    /**
     * Retourne le titre de l'évaluation.
     * 
     * @return string|null
     */
    public function getEvalTitle();

    /**
     * Retourne le numéro de révision de l'évaluation dans l'historique.
     * 
     * @return int
     */
    public function getRevisionNumber();

    /**
     * Retourne le score de l'évaluation.
     * 
     * @return int|null
     */
    public function getScore();

    /**
     * Retourne le score maximum de l'évaluation.
     * 
     * @return int|null
     */
    public function getScoreMax();

    /**
     * Retourne le score non chiffré de l'évaluation.
     * 
     * @return string|null
     */
    public function getScoreLiteral();

    /**
     * Retourne le commentaire de l'évaluation.
     * 
     * @return string|null
     */
    public function getComment();

    /**
     * Retourne le timestamp du serveur au moment de l'évaluation.
     * 
     * @return int
     */
    public function getUnixTimeStampServer();

    /**
     * Retourne la date et l'heure locale au moment de l'évaluation.
     * 
     * @return string
     */
    public function getLocalDateTime();
}
