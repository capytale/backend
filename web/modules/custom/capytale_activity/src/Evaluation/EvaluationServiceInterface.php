<?php

namespace Drupal\capytale_activity\Evaluation;

interface EvaluationServiceInterface
{
    /**
     * Ajoute une évaluation en base.
     * Ne vérifie pas les droits d'accès !!
     *
     *  @param \Drupal\capytale_activity\Evaluation\NewEvaluationInterface $evaluation
     */
    public function saveEvaluation($evaluation);

    /**
     * Ajoute plusieurs évaluations en base.
     * Ne vérifie pas les droits d'accès !!
     * 
     * @param \Drupal\capytale_activity\Evaluation\NewEvaluationInterface[] $evaluations
     */
    public function saveEvaluations($evaluations);

    /**
     * Crée une évaluation pour le student_assignment passé en paramètre.
     * Le champ "unixTimeStampServer" est initialisé avec le temps de la requête.
     * 
     * @param \Drupal\node\NodeInterface $node Le student_assignment concerné par l'évaluation.
     * @return \Drupal\capytale_activity\Evaluation\NewEvaluationInterface
     */
    public function createEvaluation($node);

    /**
     * Retourne la liste des évaluations pour le student dont le uid est passé en paramètre sur
     * l'activité passée en paramètre.
     * 
     * @param int|string $nid id de l'activityNode concernée. Attention, ce n'est pas le nid de l'assignment.
     * @param int|string $uid uid du student.
     * @return \Drupal\capytale_activity\Evaluation\EvaluationInterface[]
     */
    public function getEvaluations($nid, $uid);
}
