<?php

namespace Drupal\capytale_activity\Evaluation;

interface NewEvaluationInterface extends EvaluationInterface
{
    /**
     * Affecte le label de l'évaluation.
     *
     * @param int $graderUid
     * @return \Drupal\capytale_activity\Evaluation\newEvaluationInterface
     */
    public function setGraderUid($graderUid);

    /**
     * Affecte le label de l'évaluation.
     *
     * @param string $evalLabel
     * @return \Drupal\capytale_activity\Evaluation\newEvaluationInterface
     */
    public function setEvalLabel($evalLabel);

    /**
     * Affecte le titre de l'évaluation.
     *
     * @param string $evalTitle
     * @return \Drupal\capytale_activity\Evaluation\newEvaluationInterface
     */
    public function setEvalTitle($evalTitle);

    /**
     * Affecte le score de l'évaluation.
     *
     * @param float $score
     * @return \Drupal\capytale_activity\Evaluation\newEvaluationInterface
     */
    public function setScore($score);

    /**
     * Affecte le score maximum de l'évaluation.
     *
     * @param float $scoreMax
     * @return \Drupal\capytale_activity\Evaluation\newEvaluationInterface
     */
    public function setScoreMax($scoreMax);

    /**
     * Affecte le score literal de l'évaluation.
     *
     * @param string $scoreLiteral
     * @return \Drupal\capytale_activity\Evaluation\newEvaluationInterface
     */
    public function setScoreLiteral($scoreLiteral);

    /**
     * Affecte le commentaire de l'évaluation.
     *
     * @param string $comment
     * @return \Drupal\capytale_activity\Evaluation\newEvaluationInterface
     */
    public function setComment($comment);

    /**
     * Affecte la date et l'heure locale au moment de l'évaluation.
     * 
     * @param string $localDateTime
     * @return \Drupal\capytale_activity\Evaluation\newEvaluationInterface
     */
    public function setLocalDateTime($localDateTime);

    /**
     * Indique si l'évaluation est vide.
     * 
     * @return bool
     */
    public function isEmpty();
}
