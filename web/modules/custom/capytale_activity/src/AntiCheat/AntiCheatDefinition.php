<?php

namespace Drupal\capytale_activity\AntiCheat;


/**
 * Returns the array of anti_cheat.
 */


class AntiCheatDefinition
{
  const ANTICHEAT_DEFINITION = [
    'enabled' => [
      'type' => "integer",
      'schema' => [
        'description' => "",
        'type' => 'int',
        'size' => 'tiny',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'form' => [
        '#id' => "enable",
        '#type' => 'checkbox',
        '#title' => 'Activer le mode anti-triche',
        '#default_value' => 0,
      ],
    ],
    'passwd' => [
      'type' => "string",
      'schema' => [
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE,
        'default' => '',
      ],
      'form' => [
        '#id' => "crypt",
        '#type' => 'textfield',
        '#title' => '',
        '#default_value' => '',
        '#states'=> [
          'enabled' => [':input[id=enable]' => ['checked' => TRUE]],
          'required' => [':input[id=enable]' => ['checked' => TRUE], ]
        ],
      ],
    ]
  ];
}
