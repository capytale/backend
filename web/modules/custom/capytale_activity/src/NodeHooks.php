<?php

namespace Drupal\capytale_activity;

class NodeHooks
{
    /**
     * Fait en sorte que les private tags des autres utilisateurs ne soient pas supprimés.
     * 
     * @param \Drupal\node\NodeInterface $node
     */
    public static function keepTags($node)
    {
        /** @var \Drupal\node\NodeInterface $original */
        $original = $node->original;
        $etiqField = $original->get('field_etiquettes');
        if ($etiqField->isEmpty()) return;
        $tids = [];
        foreach ($etiqField as $v) {
            $tids[] = $v->target_id;
        }
        $tids = implode(',', $tids);
        if (\preg_match('/[^0-9,]/', $tids)) throw new \Exception();
        $sql =
            "SELECT tid
FROM {user_term}
WHERE tid IN ({$tids})
AND uid <> :uid";

        $uid = \Drupal::currentUser()->id();
        $db = \Drupal::database();
        $tids = $db->query($sql, [':uid' => $uid])->fetchCol();
        if (empty($tids)) return;

        $etiqField = $node->get('field_etiquettes');
        $existing = [];
        foreach ($etiqField as $v) {
            $existing[] = $v->target_id;
        }
        $missing = array_diff($tids, $existing);
        foreach ($missing as $tid) {
            $etiqField->appendItem($tid);
        }
    }
}
