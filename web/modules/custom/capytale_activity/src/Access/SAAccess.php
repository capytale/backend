<?php

namespace Drupal\capytale_activity\Access;

use Drupal\Core\Access\AccessResult;

use Drupal\capytale_activity\Access\NodeAccess;
use Drupal\node\Plugin\views\filter\Access;

/**
 * Classe pour gérer les droits d'accès aux student_assignment
 */
class SAAccess extends NodeAccess
{
    public function access($op)
    {
        // Dans tous les cas, il faut être authentifié
        if (!$this->account->isAuthenticated()) return AccessResult::forbidden();

        if ($this->isOwner()) {
            // Pour le propriétaire,
            $refAct = $this->refAct();
            // S'il n'y a pas d'activité parente, il a le droit de 'delete'
            if (!$refAct) return self::allowedOnlyIf($op === 'delete');
            // Cas particulier s'il est aussi prof, il a tous les droits :
            if ($refAct->access('c-sa:review')) return AccessResult::allowed();
            // Cas particulier 'delete' si l'activité a été anonymisée
            if ($op === 'delete') {
                $access = $refAct->access('c-sa:delete.*');
                if ($access) return AccessResult::allowed();
            }
            // S'il n'est qu'élève :
            // D'abord l'accès au niveau du student_assignment lui même
            $allowedOps = $this->workflowAllowedOps();
            $access = \in_array($op, $allowedOps);
            if (!$access) return AccessResult::forbidden();
            // Cas particulier 'delete' en mode AntiCheat
            if ($op === 'delete') {
                $access = \in_array('delete!', $allowedOps);
                if ($access) return self::allowedOnlyIf($refAct->access('c-sa:delete.corrected'));
            }
            // Ensuite l'accès au niveau de l'activité parente
            // @see \Drupal\capytale_activity\Access\ActivityAccess
            return self::allowedOnlyIf($refAct->access('c-sa:' . $op));
        } else {
            // Pour les autres, ils ne peuvent pas 'delete'
            if ($op === 'delete') return AccessResult::forbidden();
            // et ils ont tous les autres droits s'ils sont profs, c'est à dire
            // s'ils ont le droit 'c-sa:review' sur l'activité parente si elle existe bien
            $refAct = $this->refAct();
            if (!$refAct) return AccessResult::forbidden();
            return self::allowedOnlyIf($refAct->access('c-sa:review'));
        }
    }

    /**
     * Donne les opérations autorisées en tenant compte du workflow
     * 
     * @return array
     */
    protected function workflowAllowedOps()
    {
        $wf = \intval($this->node->field_workflow->value);
        if ($wf == 100) return ['view', 'update', 'delete'];
        if ($wf == 200) return ['view'];
        if ($wf == 250) return [];
        if ($wf == 300) return ['view', 'delete', 'delete!']; // 'delete!' pour le mode AntiCheat
        return [];
    }

    /**
     * Donne l'activité parente
     *
     * @return \Drupal\node\NodeInterface | null
     */
    protected function refAct()
    {
        /** @var \Drupal\Core\Field\FieldItemListInterface $field */
        $field = $this->node->get('field_activity');
        if ($field->isEmpty()) return null;
        return $field->entity;
    }
}
