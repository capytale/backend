<?php

namespace Drupal\capytale_activity\Access;

use Drupal\Core\Access\AccessResult;

use Drupal\capytale_activity\Access\ActivityAccess;
use Drupal\capytale_activity\Access\SAAccess;

/**
 * Classe de base pour gérer les droits d'accès.
 * Deux descendants :
 * - ActivityAccess pour activity, console_activity et notebook_activity
 * - SAAccess pour student_assignment
 * 
 * La méthode statique @see NodeAccess::AccessHook est appelée
 * depuis le .module
 */
abstract class NodeAccess
{
  const ACTIVITIES = [
    'activity',
    'console_activity',
    'notebook_activity',
  ];
  const SA = 'student_assignment';

  /** @var \Drupal\node\NodeInterface $node */
  protected $node;
  /** @var \Drupal\Core\Session\AccountInterface $account */
  protected $account;

  /**
   * Méthode statique appelée depuis le hook situé dans le .module
   * Elle instancie l'un des deux descendants de cette classe
   * selon le type de node à gérer
   *
   * @param \Drupal\node\NodeInterface $node
   * @param string $op
   * @param \Drupal\Core\Session\AccountInterface $account
   * @return \Drupal\Core\Access\AccessResultInterface
   */
  public static function AccessHook($node, $op, $account)
  {
    $type = $node->bundle();
    if ($type === self::SA) {
      return (new SAAccess($node, $account))->access($op);
    } else if (\in_array($type, self::ACTIVITIES)) {
      return (new ActivityAccess($node, $account))->access($op);
    } else {
      return AccessResult::neutral();
    }
  }

  protected static function allowedOnlyIf($condition)
  {
    if ($condition) return AccessResult::allowed();
    return AccessResult::forbidden();
  }

  /**
   * @param \Drupal\node\NodeInterface $node
   * @param \Drupal\Core\Session\AccountInterface $account
   */
  public function __construct($node, $account)
  {
    $this->node = $node;
    $this->account = $account;
  }

  /**
   * @return \Drupal\Core\Access\AccessResultInterface
   */
  public abstract function access($op);

  /**
   * Détermine si l'utilisateur courant est propriétaire du node
   *
   * @return bool
   */
  protected function isOwner()
  {
    return $this->account->id() === $this->node->getOwnerId();
  }
}
