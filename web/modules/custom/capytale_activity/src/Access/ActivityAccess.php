<?php

namespace Drupal\capytale_activity\Access;

use Drupal\Core\Access\AccessResult;

use Drupal\capytale_activity\Access\NodeAccess;

/**
 * Classe pour gérer les droits d'accès aux activités
 *
 * En plus des droits Drupal 'view', 'update' et 'delete', cette classe
 * prend en charge le droit de cloner : 'c-a:clone'
 * 
 * Cette classe prend également en charge des droits portant sur les enfants,
 * c'est à dire les student_assignment liés à une activité.
 *
 * Les opérations portants sur les enfants commencent par 'c-sa:'
 *
 * Donnés à tout le monde en fonction du mode et du time range :
 *
 *  'c-sa:create' : droit de créer un student_assignment
 *  'c-sa:view' : droit de voir un student_assignment
 *  'c-sa:update' : droit de modifier un student_assignment
 *  'c-sa:delete' : droit de supprimer un student_assignment
 *  'c-sa:delete.corrected' : droit de supprimer un student_assignment qui est corrigée. Ce droit est donné même si le mode AntiCheat est actif.
 *  'c-sa:delete.*' : droit de supprimer un student_assignment dans tous les cas. Ce droit est donné même si l'activité est anonymisée.
 *
 * Donné uniquement aux VIP :
 *  'c-sa:review' : droit de corriger un student_assignment
 */
class ActivityAccess extends NodeAccess
{
    public function access($op)
    {
        // L'accès à la bib nécéssite obligatoirement d'être authentifié
        if (!$this->account->isAuthenticated()) return AccessResult::forbidden();

        // Cas de la publication Bib et WEB et cas du clonage
        if ($op === 'view' || $op === 'c-a:clone') {
            if ($this->node->field_status_shared->value) {
                if ($this->node->field_status_web->value) {
                    return AccessResult::allowed();
                }
                if ($this->isTeacher()) {
                    return AccessResult::allowed();
                }
            }
        }

        // S'il s'agit d'une opération sur les SA enfants ($op commence par 'c-sa:')
        $childOp = $this->childOperation($op);
        if ($childOp) return $this->childAccess($childOp);

        // Ici, il s'agit d'une opération drupal sur le node 'activity' ou 'console_activity'
        // ou 'notebook_activity' lui même.

        // Cas des VIP
        if ($this->isOwner()) return AccessResult::allowed();
        if (($op != 'delete') && $this->isAssociate()) return AccessResult::allowed();

        // Cas des élèves
        if ($op === 'view') {
            if (\in_array('view', $this->trmodeAllowedChildOps())) return self::allowedOnlyIf($this->hasAssignment());
        }

        // Tous les cas possibles d'accès ont été éliminés donc l'accès est interdit.
        return AccessResult::forbidden();
    }


    /**
     * Gére le cas des opérations sur les SA enfants
     * Lorsque cette fonction est appellée, le cas non authentifié a
     * déjà été éliminé. @see self::access()
     *
     * @param string $op l'opération demandée (sans le 'c-sa:') : 'create', 'view', 'update', 'delete' , 'delete!' ou 'review'
     * @return \Drupal\Core\Access\AccessResult
     */
    protected function childAccess($op)
    {
        // 'review' uniquement pour les VIP
        if ($op === 'review') return self::allowedOnlyIf($this->isVip());
        // 'delete.*' est autorisé si l'activité est anonymisée (c'est à dire le prof l'a supprimée)
        if ($op === 'delete.*') {
            if ((int)$this->node->getOwnerId() === 0) return AccessResult::allowed();
        }
        // 'delete' n'est pas autorisé si le mode AntiCheat est actif (mais 'delete!' peut l'être si copie corrigée)
        if ($op === 'delete') {
            /** @var \Drupal\Core\Field\FieldItemListInterface $acField */
            $acField = $this->node->field_anti_cheat;
            if (!$acField->isEmpty()) {
                if ($acField->enabled) return AccessResult::forbidden();
            }
        }
        // Les droits des autres opérations dépendent uniquement du mode
        // et du time range
        return self::allowedOnlyIf(\in_array($op, $this->trmodeAllowedChildOps()));
    }

    /**
     * Donne les opérations autorisées sur les SA enfants
     * en tenant compte du mode et du time range
     *
     * @return array
     */
    protected function trmodeAllowedChildOps()
    {
        if (!$this->node->field_status_clonable->value) return [];
        $field = $this->node->field_access_tr_mode;
        if (!$field->isEmpty()) {
            $mode = $field->value;
            if ($mode === 'complete') {
                if ($this->isInTimerange()) return ['create', 'view', 'update', 'delete', 'delete.coorected'];
                else return ['view'];
            }
            if ($mode === 'lock') {
                if ($this->isInTimerange()) return ['create', 'view', 'update', 'delete', 'delete.corrected'];
                else return [];
            }
        }
        // mode 'none' ou pas de valeur
        return ['create', 'view', 'update', 'delete', 'delete.corrected'];
    }

    /**
     * Détermine si l'utilisateur courant a un student_assignment
     *
     * @return bool
     */
    protected function hasAssignment()
    {
        $sql =
            "SELECT EXISTS(
SELECT 1
FROM {node_field_data} nfd
JOIN {node__field_activity} nfa ON nfa.entity_id = nfd.nid AND NOT nfa.deleted
WHERE nfd.uid = :uid
AND nfd.type = :type
AND nfa.field_activity_target_id = :nid)";
        $db = \Drupal::database();
        $q = $db->query($sql, ['uid' => $this->account->id(), 'type' => self::SA, 'nid' => $this->node->id()]);
        return !!$q->fetchField();
    }

    /**
     * Détermine s'il s'agit d'une opération portant sur un SA enfant,
     * c'est à dire si elle commence par 'c-sa:'
     *
     * @param string $op
     * @return string|false l'opération sur l'enfant 'create', 'view', 'review', 'update' ou 'delete'
     */
    protected function childOperation($op)
    {
        $parts = \explode(':', $op, 2);
        if (\count($parts) !== 2) return false;
        if ($parts[0] !== 'c-sa') return false;
        return $parts[1];
    }

    protected $_isTeacher;
    protected function isTeacher()
    {
        if (isset($this->_isTeacher)) return $this->_isTeacher;
        $roles = $this->account->getRoles(true);
        if (\in_array('teacher', $roles)) {
            $this->_isTeacher = true;
            return true;
        } else {
            $this->_isTeacher = false;
            return false;
        }
    }

    protected $_isAssociate;
    /**
     * Détermine si l'utilisateur courant est un associé.
     * Renvoie false si c'est le propriétaire @see self::isVip()
     * (mais renvoie true si le propriétaire se trouve aussi dans les associés)
     *
     * @return bool
     */
    protected function isAssociate()
    {
        if (isset($this->_isAssociate)) return $this->_isAssociate;
        $uid = $this->account->id();
        foreach ($this->node->get('field_associates') as $associate) {
            if ($associate->target_id === $uid) {
                $this->_isAssociate = true;
                return true;
            }
        }
        $this->_isAssociate = false;
        return false;
    }

    /**
     * Détermine si l'utilisateur courant est propriétaire ou associé.
     *
     * @return bool
     */
    protected function isVip()
    {
        return $this->isOwner() || $this->isAssociate();
    }

    /**
     * Détermine si l'instant présent est dans le time range
     * du node
     *
     * @return bool
     */
    protected function isInTimerange()
    {
        $field = $this->node->get('field_access_timerange');
        if (!$field) return true;
        $now = \time();
        $start = \strtotime($field->value . 'Z');
        if ($now < $start) return false;
        $end = \strtotime($field->end_value . 'Z');
        if ($now > $end) return false;
        return true;
    }
}
