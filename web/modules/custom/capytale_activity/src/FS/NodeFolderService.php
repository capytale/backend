<?php

namespace Drupal\capytale_activity\FS;

use Drupal\Core\Site\Settings;
use Drupal\capytale_activity\FS\NodeFolder;
use Drupal\capytale_activity\Exception\CapytaleActivityException as CAErr;
use Drupal\Core\File\FileSystemInterface as FSI;

class NodeFolderService {
    const PATH_SETTING = 'c-act_node_folder';
    protected $root;
    /** @var FSI $fs */
    protected $fs;
    public function __construct($fs)
    {
        $this->root = rtrim(Settings::get(self::PATH_SETTING), '/');
        $this->fs = $fs;
    }

    /**
     * @param string|int $id
     * @return NodeFolder
     */
    public function get($id) {
        $id = \intval($id);
        $id_ds = \sprintf('%u', $id);
        $l = \strlen($id_ds);
        if ($l < 5) {
            $l1 = '0';
            if ($l < 3) $l2 = '0';
            else if ($l === 3) $l2 = $id_ds[0];
            else $l2 = \substr($id_ds, 0, 2);
        } else {
            $l = $l - 4;
            $l1 = \substr($id_ds, 0, $l);
            if ($id_ds[$l] === '0') $l2 = $id_ds[$l + 1];
            else $l2 = \substr($id_ds, $l, 2);
        }
        return new NodeFolder("{$this->root}/{$l1}/{$l2}/{$id_ds}", $this->fs);
    }
}