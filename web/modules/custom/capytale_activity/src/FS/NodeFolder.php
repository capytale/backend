<?php

namespace Drupal\capytale_activity\FS;

use Drupal\capytale_activity\Exception\CapytaleActivityRestException as CAErr;
use Drupal\capytale_activity\FS\FieldFile as FF;
use Drupal\Core\File\FileSystemInterface as FSI;

class NodeFolder {
    /** @var FSI $fs */
    protected $fs;
    protected $_path;
    protected $verif = false;
    public function __construct($path, $fs)
    {
        $this->_path = $path;
        $this->fs = $fs;
    }
    public function ensure_exists() {
        if ($this->verif) return;
        $path = $this->_path;
        $this->fs->prepareDirectory($this->_path, FSI::CREATE_DIRECTORY);
        $this->verif = true;
    }
    public function path() {
        return $this->_path;
    }
    public function get($name) {
        $this->ensure_exists();
        return new FF($this, $name, $this->fs);
    }
    public function delete() {
        if (\file_exists($this->_path))
            $this->fs->deleteRecursive($this->_path);
    }
}