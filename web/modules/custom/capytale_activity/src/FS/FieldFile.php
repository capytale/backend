<?php

namespace Drupal\capytale_activity\FS;

use Drupal\Component\Utility\Crypt;
use Drupal\capytale_activity\FS\NodeFolder as NF;
use Drupal\capytale_activity\Exception\CapytaleActivityRestException as CAErr;
use Drupal\Core\File\FileSystemInterface as FSI;

class FieldFile {
    const MAX_LEN = 20000000; // en octets
    /** @var FSI $fs */
    protected $fs;
    /** @var NF $nf */
    protected $nf;
    protected $name;
    public function __construct($nf, $name, $fs)
    {
        $this->nf = $nf;
        $this->name = $name;
        $this->fs = $fs;
    }
    public function file_path() {
        return $this->nf->path() . '/' . $this->name;
    }
    public function is_null() {
        return ! \file_exists($this->file_path());
    }
    /**
     * @param resource $stream
     * @param int $length
     * @param int $max_len
     */
    public function set_content_from_stream($stream, $length, $max_len = self::MAX_LEN) {
        try {
            $len = 0;
            $temp_file = $this->nf->path() . '/' . Crypt::randomBytesBase64(16);
            $fs = \fopen($temp_file, 'w');
            if (false === $fs) {
                throw new CAErr(CAErr::FILE_WRITE, $temp_file);
            }
            try {
                while (!\feof($stream)) {
                    $copied = \stream_copy_to_stream($stream, $fs, 8192);
                    if (false === $copied) {
                        throw new CAErr(CAErr::FILE_WRITE, $temp_file);
                    }
                    $len += $copied;
                    if ($len > $max_len) {
                        throw new CAErr(CAErr::SIZE);
                    }
                }
                if ($len !== $length) {
                    throw new CAErr(CAErr::BAD_LENGTH, "Content-Length: {$length}, Octets reçus: {$len}");
                }
            } catch(\Exception $e) {
                \fclose($fs);
                $fs = false;
                \unlink($temp_file);
                throw $e;
            } finally {
                if (false !== $fs) \fclose($fs);
            }
            \rename($temp_file, $this->file_path());
            return $len;
        } finally {
            \fclose($stream);
        }
    }

    public function set_content_from_string($content, $max_len = self::MAX_LEN) {
        $len = \strlen($content);
        if ($len > $max_len) {
            throw new CAErr(CAErr::SIZE);
        }
        $temp_file = $this->nf->path() . '/' . Crypt::randomBytesBase64(16);
        $copied = \file_put_contents($temp_file, $content);
        if (false === $copied) {
            \unlink($temp_file);
            throw new CAErr(CAErr::FILE_WRITE, $temp_file);
        }
        \rename($temp_file, $this->file_path());
        return $copied;
    }

    /**
     * @param FieldFile $field
     * @return bool
     */
    public function set_content_from_field($field)
    {
        if ($field->is_null())
        {
            return $this->set_null();
        } else {
            return \copy($field->file_path(), $this->file_path());
        }
    }

    public function set_null() {
        @$this->fs->unlink($this->file_path());
        return true;
    }

    public function truncate() {
        $temp_file = $this->nf->path() . '/' . Crypt::randomBytesBase64(16);
        $copied = \file_put_contents($temp_file, '');
        if (false === $copied) {
            \unlink($temp_file);
            throw new CAErr(CAErr::FILE_WRITE, $temp_file);
        }
        \rename($temp_file, $this->file_path());
        return $copied;
    }

}