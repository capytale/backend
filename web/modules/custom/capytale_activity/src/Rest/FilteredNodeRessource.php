<?php

namespace Drupal\capytale_activity\Rest;

use Drupal\rest\Plugin\rest\resource\EntityResource;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Entity\EntityInterface;
use Drupal\rest\ModifiedResourceResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Psr\Log\LoggerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\rest\ResourceResponseInterface;

class FilteredNodeRessource extends EntityResource
{

    /** @var \Drupal\capytale_activity\Activity\ActivityManager $am */
    private $am;
    /** @var \Drupal\Component\Datetime\TimeInterface */
    protected $ts;

    /**
     * Constructs a Drupal\rest\Plugin\rest\resource\EntityResource object.
     *
     * @param array $configuration
     *   A configuration array containing information about the plugin instance.
     * @param string $plugin_id
     *   The plugin_id for the plugin instance.
     * @param mixed $plugin_definition
     *   The plugin implementation definition.
     * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
     *   The entity type manager
     * @param array $serializer_formats
     *   The available serialization formats.
     * @param \Psr\Log\LoggerInterface $logger
     *   A logger instance.
     * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
     *   The config factory.
     * @param \Drupal\Component\Plugin\PluginManagerInterface $link_relation_type_manager
     *   The link relation type manager.
     * @param \Drupal\capytale_activity\Activity\ActivityManager $am
     *   The activity manager.
     * @param \Drupal\Component\Datetime\TimeInterface $ts
     *   The time service.
     */
    public function __construct(
        array $configuration,
        $plugin_id,
        $plugin_definition,
        EntityTypeManagerInterface $entity_type_manager,
        $serializer_formats,
        LoggerInterface $logger,
        ConfigFactoryInterface $config_factory,
        PluginManagerInterface $link_relation_type_manager,
        $am,
        $ts
    ) {
        parent::__construct(
            $configuration,
            $plugin_id,
            $plugin_definition,
            $entity_type_manager,
            $serializer_formats,
            $logger,
            $config_factory,
            $link_relation_type_manager
        );
        $this->am = $am;
        $this->ts = $ts;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
    {
        return new static(
            $configuration,
            $plugin_id,
            $plugin_definition,
            $container->get('entity_type.manager'),
            $container->getParameter('serializer.formats'),
            $container->get('logger.factory')->get('rest'),
            $container->get('config.factory'),
            $container->get('plugin.manager.link_relation_type'),
            $container->get('capytale_activity.manager'),
            $container->get('datetime.time')
        );
    }

    public function patch(EntityInterface $original_entity, EntityInterface $entity = NULL, Request $request = null)
    {
        /** @var \Drupal\node\NodeInterface $original_entity */
        /** @var \Drupal\node\NodeInterface $entity */
        if (empty($request)) return parent::patch($original_entity, $entity);
        $ab = $this->am->getActivityBunch($original_entity);
        if (!$ab) return parent::patch($original_entity, $entity);
        $cfg = $ab->getCfg();
        $isSa = $ab->isSa();
        $noret = $request->query->get('_noecho');
        $ret = parent::patch($original_entity, $entity);

        if ($ret instanceof ResourceResponseInterface) {
            /** @var \Drupal\node\NodeInterface $n */
            $n = $ret->getResponseData();
            $created = $n->getCreatedTime();
            $modified = $n->getChangedTime();
            if ($modified <= $created) {
                $modified = $this->ts->getRequestTime();
                if ($modified <= $created) {
                    $modified = $created + 1;
                }
                $n->setChangedTime($modified);
                $n->save();
            }
            $this->filterOut($n, $cfg, $isSa);
        }
        if (empty($noret)) return $ret;
        if ($ret instanceof ResourceResponseInterface) {
            return new ModifiedResourceResponse(null, $ret->getStatusCode(), $ret->headers->all());
        } else {
            return $ret;
        }
    }

    public function get(EntityInterface $entity, Request $request)
    {
        /** @var \Drupal\node\NodeInterface $entity */
        $ab = $this->am->getActivityBunch($entity);
        if (!$ab) return parent::get($entity, $request);
        $this->filterOut($entity, $ab->getCfg(), $ab->isSa());
        return parent::get($entity, $request);
    }

    private function filterIn($entity, $cfg, $isSa)
    {
        $fields = &$entity->_restSubmittedFields;
        unset($fields['comment']);
        unset($fields['field_modules']);
        unset($fields['field_nb_clone']);
        unset($fields['field_enseignement']);
        unset($fields['field_status_shared']);
        unset($fields['field_status_web']);
        unset($fields['field_abstract']);
        unset($fields['field_niveau']);
        unset($fields['field_theme']);
        if (isset($cfg->lti)) {
            unset($fields['field_content']);
        }
        unset($fields['field_code']);
        //unset($fields['field_activity_type']);  // attention python3 <-> python3-old
        unset($fields['field_activity']);
    }

    /**
     * Filter out some fields from the entity before it is sent.
     * 
     * @param \Drupal\node\NodeInterface $entity
     * @param \stdClass $cfg
     * @param bool $isSa
     */
    private function filterOut($entity, $cfg, $isSa)
    {
        if ($isSa) {
        } else {
            $entity->set('comment', NULL);
            $entity->set('field_modules', NULL);
            $entity->set('field_nb_clone', NULL);
            $entity->set('field_enseignement', NULL);
            $entity->set('field_status_shared', NULL);
            $entity->set('field_status_web', NULL);
            $entity->set('field_abstract', NULL);
            $entity->set('field_niveau', NULL);
            $entity->set('field_theme', NULL);
        }
        if (isset($cfg->lti)) {
            $entity->set('field_content', NULL);
        }
    }
}
