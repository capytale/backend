<?php

namespace Drupal\capytale_activity\Activity;

use Drupal\Component\Utility\UrlHelper;

use Drupal\capytale_activity\Activity\ActivityBunchInterface;
use Drupal\capytale_activity\Activity\ActivityManager as AM;
use \Drupal\capytale_activity\Exception\CapytaleActivityException as CAE;

class ActivityBunch implements ActivityBunchInterface
{
    /** @var \stdClass $cfg */
    protected $cfg;

    /** @var bool $_isSa */
    protected $_isSa;

    /** @var \Drupal\node\NodeInterface $node */
    protected $node;

    public function __construct($node, $cfg, $isSa)
    {
        $this->node = $node;
        $this->cfg = $cfg;
        $this->_isSa = $isSa;
    }

    public function isSa()
    {
        return $this->_isSa;
    }

    public function id()
    {
        return $this->node->id();
    }

    public function getNode()
    {
        return $this->node;
    }

    public function getType()
    {
        return $this->cfg->type;
    }

    public function getCfg()
    {
        return $this->cfg;
    }

    public function getAssignmentNode()
    {
        if (!$this->_isSa) return false;
        return $this->node;
    }

    public function getActivityNode()
    {
        if ($this->_isSa) return $this->node->field_activity->entity;
        return $this->node;
    }

    public function getPlayerUrl($mode)
    {
        $assignmentModes = [AM::ASSIGNMENT, AM::REVIEW];
        if ($this->isSa()) {
            $ok = \in_array($mode, $assignmentModes);
        } else {
            $ok = !\in_array($mode, $assignmentModes);
        }
        if (!$ok) throw new CAE("mode '$mode' not allowed for this activity");
        $cfg = $this->getCfg();
        $nid = $this->id();
        if (isset($cfg->lti)) return '/p/vittascience/?id=' . $nid;
        if (isset($cfg->players)) {
            $player = $cfg->players[$mode];
        } else {
            $player = $cfg->player;
        }
        if ($mode === AM::DETACHED) return $player;
        if (\is_array($player)) {
            $cnt = \count($player);
            $query = ($cnt >= 2) ? $player[1] : [];
            $anchor = ($cnt >= 3) ? $player[2] : '';
            $player = $player[0];
        } else {
            $query = [];
            $anchor = '';
        }
        $query['id'] = $nid;

        // Gestion des options.
        // Attention, seuls les "notebook_activity" sont pris en charge pour le moment.
        $conf = false;
        $node = $this->getActivityNode();
        if ($node->bundle() == "notebook_activity") {
            $conf = $node->get('field_config')->value;
        }
        if ($conf) {
            $conf = json_decode($conf, TRUE);
            $extensions = [];
            if (!empty($conf['admonition'])) $extensions[] = "admonition";
            if (!empty($conf['linenumbers'])) $extensions[] = "linenumbers";
            if (!empty($conf['romd']) && (($mode === AM::ASSIGNMENT) || ($mode === AM::VIEW))) $extensions[] = "romd";
            if (!empty($conf['sequenced']) && (($mode === AM::ASSIGNMENT) || ($mode === AM::VIEW))) $extensions[] = "sequenced";
            if (!empty($extensions)) $query['extensions'] = implode(',', $extensions);
        }
        $qs = UrlHelper::buildQuery($query);
        // For better readability, replace '%2C' by ','
        $qs = str_replace('%2C', ',', $qs);
        return $player . '?' . $qs . $anchor;
    }

    public function getAssignment($user)
    {
        if ($this->isSa()) throw new CAE("this activity is already a student assignment");
        $nid = $this->id();
        $saNode = \Drupal::entityQuery('node')
            ->condition('type', AM::ASSIGNMENT_NODE_TYPE)
            ->condition('field_activity', $nid)
            ->condition('uid', $user->id())
            ->execute();
        if (empty($saNode)) return null;
        $saNode = \Drupal\node\Entity\Node::load(\reset($saNode));
        return new self($saNode, $this->cfg, true);
    }

    public function createAssignment($user)
    {
        if ($this->isSa()) throw new CAE("this activity is already a student assignment");
        $assignment = $this->getAssignment($user);
        if ($assignment) return $assignment;
        $nid = $this->id();
        $saNode = \Drupal\node\Entity\Node::create([
            'type' => 'student_assignment',
            'title' => $this->node->getTitle(),
            'field_activity' => $nid,
            'uid' => $user->id(),
        ]);
        $saNode->save();
        return new self($saNode, $this->cfg, true);
    }
}
