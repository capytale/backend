<?php

namespace Drupal\capytale_activity\Activity;

use Drupal\capytale_activity\Activity\ActivityManager;

class Fields {
    /** @var \Drupal\capytale_activity\FS\NodeFolderService $nfs */
    protected $nfs;
    /** @var \Drupal\capytale_activity\Activity\ActivityManager $amng */
    protected $amng;

    public function __construct($nfs, $amng) {
        $this->nfs = $nfs;
        $this->amng = $amng;
    }

    /**
     * @param \Drupal\node\NodeInterface $n
     * @param string $name
     * @return Field|false
     */
    public function get($n, $name) {
        $def = $this->get_fields_def($n);
        if ((false === $def) || (! \array_key_exists($name, $def))) return false;
        $nf = $this->nfs->get($n->id());
        $ff = $nf->get($name);
        return new Field($n, $ff, $def[$name]);
    }
    public function get_fields_def($n) {
        $type = $this->amng->get_type($n);
        list($type, $is_sa) = ActivityManager::parse_type($type);
        $cfg = $this->amng->getCfg($type);
        if (isset($cfg->fields)) {
            return $cfg->fields;
        } else {
            return false;
        }
    }

    /**
     * @param \Drupal\node\NodeInterface $from
     * @param \Drupal\node\NodeInterface $to
     */
    public function clone_fields($from, $to)
    {
        $def = $this->get_fields_def($from);
        if ($def === false) return;
        $nf_from = $this->nfs->get($from->id());
        $nf_to = $this->nfs->get($to->id());
        foreach (\array_keys($def) as $field_name) {
            $ff_from = $nf_from->get($field_name);
            $ff_to = $nf_to->get($field_name);
            $ff_to->set_content_from_field($ff_from);
        }
    }
}

class Field {
    protected $def;
    /** @var \Drupal\node\NodeInterface $n */
    protected $n;
    /** @var \Drupal\capytale_activity\FS\FieldFile $ff */
    protected $ff;
    public function __construct($n, $ff, $def) {
        $this->def = $def;
        $this->ff = $ff;
        $this->n = $n;
    }
    public function access($op, $return_as_object = FALSE) {
        return $this->n->access($op, NULL, $return_as_object);
    }
    public function max_len() {
        return $this->def->maxlen;
    }
    public function contentType() {
        if (! isset($this->def->type)) return 'application/octet-stream';
        return $this->def->type;
    }
    /** @return \Drupal\capytale_activity\FS\FieldFile */
    public function file() {
        return $this->ff;
    }
}

