<?php
$cfg->name = 'Notebook OCaml';
$cfg->icon = [
    'path' => 'logo_ocaml.svg',
    'style' => ['color' => '#de7932', 'background-color' => '#ffffff'],
];
$cfg->helpUrl = 'https://capytale2.ac-paris.fr/p/basthon/n/?id=330758&kernel=ocaml';
$cfg->description = 'Le langage OCaml dans un notebook.';
$cfg->summary = '';
$cfg->tags = ['Jupyter'];


$cfg->bundle = 'notebook_activity';
$cfg->player = ['/p/basthon/n/', ['kernel' => 'ocaml']];
$cfg->attached_files_ext = 'txt sql db sqlite csv png jpg bmp svg gif html pdf css zip tar tgz gz aiff wav ml ttf json';
$cfg->fields = ['content' => (object)['maxlen' => 20000000, 'type' => 'text/json']];
$cfg->options = ['admonition', 'linenumbers'];
$cfg->exportable = true;
