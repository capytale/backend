<?php
$cfg->name = 'Pyxel Studio';
$cfg->icon = [
    'path' => 'logo_pyxelstudio.svg',
    'style' => ['color' => '#000000', 'background-color' => '#ffffff'],
];

$cfg->helpUrl = 'https://capytale2.ac-paris.fr/wiki/doku.php?id=pyxelstudio';
$cfg->description = 'Studio de création de jeux et d\'applications avec le moteur de jeu Pyxel.';
$cfg->summary = 'Studio de création de jeux et d\'applications avec le moteur de jeu Pyxel.';
$cfg->tags = ['Jeu', 'Python'];
$cfg->antiCheat = true; // optionnel (false si absent)



$cfg->bundle = 'activity';
$cfg->player = '/p/pyxelstudio/';