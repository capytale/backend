<?php
$cfg->name = 'Carte BBC microbit';
$cfg->icon = [
    'path' => 'logo_microbit.svg',
    'style' => ['color' => '#22b573', 'background-color' => '#ffffff'],
];

$cfg->helpUrl = 'https://capytale2.ac-paris.fr/wiki/doku.php?id=microbit';
$cfg->description = 'Programmation de la carte BBC microbit avec le module Vittascience intégré dans Capytale.';
$cfg->summary = 'Programmation de la carte BBC microbit avec le module Vittascience intégré dans Capytale.';
$cfg->tags = ['Blocs', 'Scratch', 'Carte', 'Électronique', 'Vittascience', 'Python'];
$cfg->antiCheat = false; // optionnel (false si absent)



$cfg->lti = (object)[
    'registration' => 'vittascience',
    'deployment' => 'microbit',
];