<?php
$cfg->name = 'Carte M5Stack';
$cfg->icon = [
    'path' => 'logo_m5stack.svg',
    'style' => ['color' => '#22b573', 'background-color' => '#ffffff'],
];

$cfg->helpUrl = 'https://capytale2.ac-paris.fr/wiki/doku.php?id=m5stack';
$cfg->description = 'Programmation de la carte M5Stack avec le module Vittascience intégré dans Capytale.';
$cfg->summary = 'Programmation de la carte M5Stack avec le module Vittascience intégré dans Capytale.';
$cfg->tags = ['Blocs', 'Scratch', 'Carte', 'Électronique', 'Vittascience'];
$cfg->antiCheat = false; // optionnel (false si absent)



$cfg->lti = (object)[
    'registration' => 'vittascience',
    'deployment' => 'm5stack',
];