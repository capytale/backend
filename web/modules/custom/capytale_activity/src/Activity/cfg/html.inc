<?php
$cfg->hidden = true;

$cfg->name = 'HTML/CSS/JS';
$cfg->icon = [
    'path' => 'logo_html.svg',
    'style' => ['color' => '#0b5394', 'background-color' => '#ffffff'],
];

$cfg->helpUrl = 'https://capytale2.ac-paris.fr/wiki/doku.php?id=html';
$cfg->description = 'Découvrir les langaes du Web : html, CSS et JavaScript.';
$cfg->summary = 'Découvrir les langaes du Web : html, CSS et JavaScript.';

$cfg->player = '/p/html/';
$cfg->attached_files_ext = 'png jpg bmp svg gif html';
$cfg->fields = [
    'html' => (object)['maxlen' => 20000000],
    'css' => (object)['maxlen' => 20000000],
    'js' => (object)['maxlen' => 20000000]
];
$cfg->exportable = true;
