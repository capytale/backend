<?php
$cfg->name = 'ESP32';
$cfg->icon = [
    'path' => 'logo_esp32.svg',
    'style' => ['color' => '#22b573', 'background-color' => '#ffffff'],
];
$cfg->helpUrl = 'https://capytale2.ac-paris.fr/wiki/doku.php?id=esp32';
$cfg->description = 'Programmation de la carte ESP32 avec le module Vittascience intégré dans Capytale.';
$cfg->summary = 'Programmation de la carte ESP32 avec le module Vittascience intégré dans Capytale.';
$cfg->tags = ['Blocs', 'Scratch', 'Carte', 'Électronique', 'Vittascience'];
$cfg->antiCheat = false; // optionnel (false si absent)



$cfg->lti = (object)[
    'registration' => 'vittascience',
    'deployment' => 'esp32',
];