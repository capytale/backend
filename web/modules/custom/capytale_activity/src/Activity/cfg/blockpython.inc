<?php
$cfg->hidden = true;

$cfg->name = 'Block Python';
$cfg->icon = [
    'path' => 'logo_false.svg',
    'style' => ['color' => '#898dc5', 'background-color' => '#ffffff'],
];
$cfg->tags = ['Blocs', 'Scratch', 'Python'];




$cfg->player = '/p/blockpython/';

