<?php
$cfg->name = 'Arduino';
$cfg->icon = [
    'path' => 'logo_arduino.svg',
    'style' => ['color' => '#22b573', 'background-color' => '#ffffff'],
];

$cfg->helpUrl = 'https://capytale2.ac-paris.fr/wiki/doku.php?id=arduino';
$cfg->description = 'Programmation de la carte Arduino avec le module Vittascience intégré dans Capytale.';
$cfg->summary = 'Programmation de la carte Arduino avec le module Vittascience intégré dans Capytale.';
$cfg->tags = ['Blocs', 'Scratch', 'Carte', 'Électronique', 'Vittascience'];
$cfg->antiCheat = false; // optionnel (false si absent)



$cfg->lti = (object)[
    'registration' => 'vittascience',
    'deployment' => 'arduino',
];