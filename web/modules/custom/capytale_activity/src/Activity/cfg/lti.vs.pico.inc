<?php
$cfg->name = 'Carte Raspberry Pi Pico';
$cfg->icon = [
    'path' => 'logo_pipico.svg',
    'style' => ['color' => '#22b573', 'background-color' => '#ffffff'],
];

$cfg->helpUrl = 'https://capytale2.ac-paris.fr/wiki/doku.php?id=pico';
$cfg->description = 'Programmation de la carte Raspberry Pi Pico avec le module Vittascience intégré dans Capytale.';
$cfg->summary = 'Programmation de la carte Raspberry Pi Pico avec le module Vittascience intégré dans Capytale.';
$cfg->tags = ['Blocs', 'Scratch', 'Carte', 'Électronique', 'Vittascience'];
$cfg->antiCheat = false; // optionnel (false si absent)


$cfg->lti = (object)[
    'registration' => 'vittascience',
    'deployment' => 'pico',
];
