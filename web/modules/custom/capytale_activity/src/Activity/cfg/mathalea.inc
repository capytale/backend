<?php
$cfg->name = 'MathALÉA';
$cfg->icon = [
    'path' => 'logo_mathalea.svg',
    'style' => ['color' => '#000000', 'background-color' => '#ffffff'],
];

$cfg->helpUrl = 'https://capytale2.ac-paris.fr/wiki/doku.php?id=mathalea';
$cfg->description = 'L\'exerciseur de mathématiques MathALÉA enrichi des fonctionnalités pédagogiques de Capytale.';
$cfg->summary = 'L\'exerciseur de mathématiques MathALÉA enrichi des fonctionnalités pédagogiques de Capytale.';
$cfg->tags = ['Exerciseur', 'Mathématiques'];
$cfg->detailedEvaluation = true;
$cfg->antiCheat = true; // optionnel (false si absent)



$cfg->bundle = 'activity';
$cfg->player = '/p/mathalea/';