<?php
$cfg->name = 'Bloc Python';
$cfg->icon = [
    'path' => 'logo_blocpython.svg',
    'style' => ['color' => '#22b573', 'background-color' => '#ffffff'],
];

$cfg->helpUrl = 'https://capytale2.ac-paris.fr/wiki/doku.php?id=blocpython';
$cfg->description = 'Transition de la programmation par blocs vers la programmation textuelle en Python avec le module Vittascience intégré dans Capytale.';
$cfg->summary = 'Transition de la programmation par blocs vers la programmation textuelle en Python avec le module Vittascience intégré dans Capytale.';
$cfg->tags = ['Blocs', 'Scratch', 'Vittascience', 'Python'];
$cfg->antiCheat = false; // optionnel (false si absent)



$cfg->lti = (object)[
    'registration' => 'vittascience',
    'deployment' => 'python',
];