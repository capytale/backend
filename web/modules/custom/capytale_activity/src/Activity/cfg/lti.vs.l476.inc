<?php
$cfg->name = 'Carte Nucleo-L476RG';
$cfg->icon = [
    'path' => 'logo_l476.svg',
    'style' => ['color' => '#22b573', 'background-color' => '#ffffff'],
];

$cfg->helpUrl = 'https://capytale2.ac-paris.fr/wiki/doku.php?id=l476';
$cfg->description = 'Programmation de la carte Nucleo-L476RG avec le module Vittascience intégré dans Capytale.';
$cfg->summary = 'Programmation de la carte Nucleo-L476RG avec le module Vittascience intégré dans Capytale.';
$cfg->tags = ['Blocs', 'Scratch', 'Carte', 'Électronique', 'Vittascience'];
$cfg->antiCheat = false; // optionnel (false si absent)



$cfg->lti = (object)[
    'registration' => 'vittascience',
    'deployment' => 'l476',
];