<?php
$cfg->name = 'Robot Buddy';
$cfg->icon = [
    'path' => 'logo_buddy.svg',
    'style' => ['color' => '#22b573', 'background-color' => '#ffffff'],
];

$cfg->helpUrl = 'https://capytale2.ac-paris.fr/wiki/doku.php?id=buddy';
$cfg->description = 'Programmation du robot Buddy avec le module Vittascience intégré dans Capytale.';
$cfg->summary = 'Programmation du robot Buddy avec le module Vittascience intégré dans Capytale.';
$cfg->tags = ['Blocs', 'Scratch', 'Robot', 'Électronique', 'Vittascience'];
$cfg->antiCheat = false; // optionnel (false si absent)



$cfg->lti = (object)[
    'registration' => 'vittascience',
    'deployment' => 'buddy',
];