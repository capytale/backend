<?php
$cfg->name = 'Code Puzzle';
$cfg->icon = [
    'path' => 'logo_codepuzzle.svg',
    'style' => ['color' => '#79c824', 'background-color' => '#ffffff'],
];
$cfg->helpUrl = 'https://capytale2.ac-paris.fr/wiki/doku.php?id=codepuzzle';
$cfg->description = 'Générateur et gestionnare de puzzles de Parsons.';
$cfg->summary = 'Générateur et gestionnare de puzzles de Parsons.';
$cfg->tags = ['Puzzle', 'Python'];
$cfg->antiCheat = false; // optionnel (false si absent)

$cfg->players = [
    'create' => '/p/codepuzzle/',
    'assignment' => '/p/codepuzzle/eleve.html',
    'review' => '/p/codepuzzle/evaluation.html',
    'view' => '/p/codepuzzle/view.html',
    'detached' => '/p/codepuzzle/no.html'
];
