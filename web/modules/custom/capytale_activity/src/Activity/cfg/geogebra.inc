<?php
$cfg->name = 'GeoGebra';
$cfg->icon = [
    'path' => 'logo_geogebra.svg',
    'style' => ['color' => '#9999ff', 'background-color' => '#ffffff'],
];
$cfg->helpUrl = 'https://capytale2.ac-paris.fr/wiki/doku.php?id=geogebra';
$cfg->description = 'Géométrie dynamique avec Geogebra enrichi des fonctionnalités pédagogiques de Capytale.';
$cfg->summary = 'Géométrie dynamique avec Geogebra enrichi des fonctionnalités pédagogiques de Capytale.';
$cfg->tags = ['Géométrie', 'Tableur', '3d'];
$cfg->antiCheat = true; // optionnel (false si absent)

$cfg->player = '/p/geogebra/';
$cfg->fields = ['content' => (object)['maxlen' => 20000000]];
$cfg->exportable = true;

