<?php
$cfg->name = 'Exemple';
$cfg->fullName = 'Exemple'; // égal à $cfg->name
$cfg->status = [
    'beta',
    'new',
    'hot'
];
$cfg->icon = [
    'path' => 'logo_exemple.svg',
    'style' => ['color' => '#000000', 'background-color' => '#ffffff'],
];
$cfg->helpUrl = 'https://player-help.com'; // égal à wiki
$cfg->summary = 'Description résumée avec les bons mots clés';
$cfg->description = 'Description complète avec les bons mots clés'; // Pas utile immédiatement
$cfg->tags = ['keyword1', 'keyword2']; // optionnel

$cfg->hidden = false; // optionnel (false si absent)
$cfg->exportable = true; // optionnel (false si absent)
$cfg->detailedEvaluation = true; // optionnel (false si absent)

$cfg->antiCheat = true; // optionnel (false si absent)



// Si un niveau et un sujet ont été choisis, on effectue le produit des deux poids.

$cfg->bundle = 'activity'; // c'est la valeur par défaut
$cfg->player = '/p/exemple/'; // ou plus complexe, cf basthon ou web
$cfg->attached_files_ext = false;
$cfg->fields = ['content' => (object)['maxlen' => 20000000]];

$cfg->role = 'tester'; // optionnel
