<?php
$cfg->name = 'Notebook Python';
$cfg->icon = [
    'path' => 'logo_nb.svg',
    'style' => ['color' => '#ff6402', 'background-color' => '#ffffff'],
];

$cfg->helpUrl = 'https://capytale2.ac-paris.fr/wiki/doku.php?id=notebook';
$cfg->description = 'Python dans un notebook Jupyter : une alternance de cellules de texte riche et de cellules de code exécutables.';
$cfg->summary = 'Python dans un notebook Jupyter : une alternance de cellules de texte riche et de cellules de code exécutables.';
$cfg->tags = ['Jupyter'];


$cfg->bundle = 'notebook_activity';
$cfg->player = ['/p/basthon/n/', ['kernel' => 'python3']];
$cfg->attached_files_ext = 'py npy pyc txt sql db sqlite csv png jpg bmp svg gif html pdf css zip tar tgz gz aiff wav ml ttf json dat bin';
$cfg->fields = ['content' => (object)['maxlen' => 20000000, 'type' => 'text/json']];
$cfg->options = ['admonition', 'sequenced', 'linenumbers', 'romd'];
$cfg->exportable = true;
