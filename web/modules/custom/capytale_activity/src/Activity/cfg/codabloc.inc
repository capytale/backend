<?php
$cfg->name = 'Codabloc';
$cfg->icon = [
    'path' => 'logo_codabloc.svg',
    'style' => ['color' => '#898dc5', 'background-color' => '#ffffff'],
];
$cfg->helpUrl = 'https://capytale2.ac-paris.fr/wiki/doku.php?id=codabloc';
$cfg->description = 'L\'environnement Scratch intégré dans Capytale pour apprendre à programmer avec des blocs.';
$cfg->summary = 'L\'environnement Scratch intégré dans Capytale pour apprendre à programmer avec des blocs.';
$cfg->tags = ['Blocs', 'Scratch'];

$cfg->player = '/p/codabloc/';
$cfg->fields = ['content' => (object)['maxlen' => 20000000]];
$cfg->exportable = true;