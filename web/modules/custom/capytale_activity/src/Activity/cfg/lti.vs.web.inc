<?php
$cfg->name = 'Bloc WEB';
$cfg->icon = [
    'path' => 'logo_blocweb.svg',
    'style' => ['color' => '#22b573', 'background-color' => '#ffffff'],
];

$cfg->helpUrl = 'https://capytale2.ac-paris.fr/wiki/doku.php?id=blocweb';
$cfg->description = 'Transition de la programmation par blocs vers la programmation textuelle pour les langages du WEB avec le module Vittascience intégré dans Capytale.';
$cfg->summary = 'Transition de la programmation par blocs vers la programmation textuelle pour les langages du WEB avec le module Vittascience intégré dans Capytale.';
$cfg->tags = ['Blocs', 'Scratch', 'Html', 'Css', 'Js', 'JavaScript', 'Vittascience'];
$cfg->antiCheat = false; // optionnel (false si absent)



$cfg->lti = (object)[
    'registration' => 'vittascience',
    'deployment' => 'web',
];