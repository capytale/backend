<?php
$cfg->name = 'Robot Mbot';
$cfg->icon = [
    'path' => 'logo_mbot.svg',
    'style' => ['color' => '#22b573', 'background-color' => '#ffffff'],
];
$cfg->helpUrl = 'https://capytale2.ac-paris.fr/wiki/doku.php?id=mBot';
$cfg->description = 'Programmation du Robot mBot avec le module Vittascience intégré dans Capytale.';
$cfg->summary = 'Programmation du Robot mBot avec le module Vittascience intégré dans Capytale.';
$cfg->tags = ['Blocs', 'Scratch', 'Robot', 'Électronique', 'Vittascience'];
$cfg->antiCheat = false; // optionnel (false si absent)



$cfg->lti = (object)[
    'registration' => 'vittascience',
    'deployment' => 'mBot',
];