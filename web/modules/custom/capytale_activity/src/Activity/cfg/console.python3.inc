<?php
$cfg->name = 'Console Python';
$cfg->icon = [
    'path' => 'logo_console.svg',
    'style' => ['color' => '#000000', 'background-color' => '#ffffff'],
];
$cfg->helpUrl = 'https://capytale2.ac-paris.fr/wiki/doku.php?id=codabloc';
$cfg->description = 'L\'environnement classique de programmation : un script Python à gauche et une console d\'exécution à droite.';
$cfg->summary = 'L\'environnement classique de programmation : un script Python à gauche et une console d\'exécution à droite.';
$cfg->tags = ['Script', 'Console', 'Python'];

$cfg->bundle = 'console_activity';
$cfg->player = ['/p/basthon/c/', ['kernel' => 'python3']];
$cfg->attached_files_ext = 'py npy txt db sqlite csv png jpg gif bmp svg html json pdf';
$cfg->exportable = true;
