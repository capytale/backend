<?php
$cfg->hidden = true;



$cfg->name = 'IA';
$cfg->fullName = 'Entraînement I.A.<sup><span style=\"color: red\">beta</span></sup>';
$cfg->icon = [
    'path' => 'logo_iaentrainement.svg',
    'style' => ['color' => '#22b573', 'background-color' => '#ffffff'],
];
$cfg->helpUrl = 'https://capytale2.ac-paris.fr/wiki/doku.php?id=ia';
$cfg->description = 'Découverte du fontionnement de l\'intelligence artificielle via l\'entraînement d\'une IA avec le module Vittascience intégré dans Capytale.';
$cfg->summary = '';



$cfg->lti = (object)[
    'registration' => 'vittascience',
    'deployment' => 'ia',
];
$cfg->role = 'tester_vittscience';
