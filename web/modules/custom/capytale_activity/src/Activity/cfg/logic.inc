<?php
$cfg->name = 'Logic';
$cfg->icon = [
    'path' => 'logo_logic.svg',
    'style' => ['color' => '#000000', 'background-color' => '#ffffff'],
];
$cfg->helpUrl = 'https://capytale2.ac-paris.fr/wiki/doku.php?id=logicCircuit';
$cfg->description = 'Contruction de circuits éléctronique numérique';
$cfg->summary = '';
$cfg->category = [ 'all' => 10, 'lycee' => -1, 'prepa' => 1000 ];
$cfg->player = '/p/logic/';
// $cfg->role = 'tester';
