<?php
$cfg->name = 'WEB';
$cfg->icon = [
    'path' => 'logo_web.svg',
    'style' => ['color' => '#0b5394', 'background-color' => '#ffffff'],
];
$cfg->helpUrl = 'https://capytale2.ac-paris.fr/wiki/doku.php?id=web';
$cfg->description = 'Découvrir les langaes du Web : html, CSS et JavaScript.';
$cfg->summary = 'Découvrir les langaes du Web : html, CSS et JavaScript.';
$cfg->tags = ['Html', 'Css', 'Js', 'JavaScript', 'Site'];



$cfg->player = '/p/web/';
$cfg->attached_files_ext = false;
$cfg->fields = [
    'content' => (object)['maxlen' => 20000000],
];
$cfg->antiCheat = true; // optionnel (false si absent)

