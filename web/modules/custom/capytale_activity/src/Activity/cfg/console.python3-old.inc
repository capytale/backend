<?php
$cfg->hidden = true;

$cfg->name = 'Script-Console (3.8)';
$cfg->icon = [
    'path' => 'logo_console.svg',
    'style' => ['color' => '#898dc5', 'background-color' => '#ffffff'],
];

$cfg->bundle = 'console_activity';
$cfg->player = ['/p/basthon/c/', ['kernel' => 'python3-old']];
$cfg->attached_files_ext = 'py npy txt db sqlite csv png jpg gif bmp svg html json pdf';
$cfg->exportable = true;
