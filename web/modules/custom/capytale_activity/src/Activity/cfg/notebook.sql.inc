<?php
$cfg->name = 'Notebook SQL';
$cfg->icon = [
    'path' => 'logo_sql.svg',
    'style' => ['color' => '#f1c232', 'background-color' => '#ffffff'],
];
$cfg->helpUrl = 'https://capytale2.ac-paris.fr/p/basthon/n/?id=42529&kernel=sql';
$cfg->description = 'Le langage SQL dans un notebook.';
$cfg->summary = 'Le langage SQL dans un notebook.';
$cfg->tags = ['Jupyter'];

$cfg->bundle = 'notebook_activity';
$cfg->player = ['/p/basthon/n/', ['kernel' => 'sql']];
$cfg->attached_files_ext = 'txt sql db sqlite csv png jpg bmp svg gif';
$cfg->fields = ['content' => (object)['maxlen' => 20000000, 'type' => 'text/json']];
$cfg->options = ['admonition', 'linenumbers'];
$cfg->exportable = true;
