<?php
$cfg->name = 'Pixel Art';
$cfg->icon = [
    'path' => 'logo_pixel.svg',
    'style' => ['color' => '#898dc5', 'background-color' => '#ffffff'],
];

$cfg->helpUrl = 'https://capytale2.ac-paris.fr/wiki/doku.php?id=codabloc'; // égal à wiki
$cfg->description = 'Construire un dessin pixelisé avec des blocs de code.';
$cfg->summary = 'Construire un dessin pixelisé avec des blocs de code.';
$cfg->tags = ['Blocs', 'Scratch'];
$cfg->antiCheat = false; // optionnel (false si absent)



$cfg->player = '/p/pixel/';
$cfg->attached_files_ext = false;
