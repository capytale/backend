<?php
$cfg->hidden = true;

$cfg->name = 'Text Puzzle';
$cfg->icon = [
    'path' => 'logo_txtpuzzle.svg',
    'style' => ['color' => '#79c824', 'background-color' => '#ffffff'],
];
$cfg->helpUrl = 'https://capytale2.ac-paris.fr/wiki/doku.php?id=txtpuzzle';
$cfg->description = 'Générateur et gestionnaire de "textes puzzles" (textes à trous, choix multiples, lignes mélangées) pour le français, les langues, les langues anciennes...';
$cfg->summary = 'Générateur et gestionnare de "textes puzzles".';
$cfg->tags = ['Puzzle'];
$cfg->antiCheat = false; // optionnel (false si absent)


$cfg->player = '/p/txtpuzzle/';
