<?php
$cfg->hidden = true;

$cfg->name = 'Notebook (3.8)';
$cfg->icon = [
    'path' => 'logo_nb.svg',
    'style' => ['color' => '#ff6402', 'background-color' => '#ffffff'],
];

$cfg->bundle = 'notebook_activity';
$cfg->player = ['/p/basthon/n/', ['kernel' => 'python3-old']];
$cfg->attached_files_ext = 'py npy pyc txt sql db sqlite csv png jpg bmp svg gif html pdf css zip tar tgz gz aiff wav ml ttf json dat bin';
$cfg->fields = ['content' => (object)['maxlen' => 20000000, 'type' => 'text/json']];
$cfg->options = ['admonition', 'sequenced', 'linenumbers', 'romd'];
$cfg->exportable = true;
