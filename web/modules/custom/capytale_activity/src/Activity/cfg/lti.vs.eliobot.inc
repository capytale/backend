<?php
$cfg->name = 'Eliobot';
$cfg->icon = [
    'path' => 'logo_eliobot.svg',
    'style' => ['color' => '#22b573', 'background-color' => '#ffffff'],
];

$cfg->helpUrl = 'https://capytale2.ac-paris.fr/wiki/doku.php?id=eliobot';
$cfg->description = 'Programmation du robot Eliobot avec le module Vittascience intégré dans Capytale.';
$cfg->summary = 'Programmation du robot Eliobot avec le module Vittascience intégré dans Capytale.';
$cfg->tags = ['Blocs', 'Scratch', 'Robot', 'Électronique', 'Vittascience'];
$cfg->antiCheat = false; // optionnel (false si absent)



$cfg->lti = (object)[
    'registration' => 'vittascience',
    'deployment' => 'eliobot',
];