<?php

namespace Drupal\capytale_activity\Activity;

use Drupal\node\NodeInterface;
use Drupal\Component\Utility\UrlHelper;
use Drupal\node\Entity\Node;
use Drupal\Core\Url;
use Drupal\Core\Site\Settings;
use Drupal\Component\Serialization\Yaml;

use Drupal\capytale_activity\Activity\ActivityBunch;

/**
 * Class ActivityManager.
 *
 * Service léger pour donner les types d'activités et leur configuration.
 */
class ActivityManager
{
  const CACHE_ID = "c-act:activity_types";
  const CREATE = 'create';
  const ASSIGNMENT = 'assignment';
  const REVIEW = 'review';
  const VIEW = 'view';
  const DETACHED = 'detached';

  const NB_NODE_TYPE = 'notebook_activity';
  const NBHOSTING_NODE_TYPE = 'notebook';
  const CONSOLE_NODE_TYPE = 'console_activity';
  const ACTIVITY_NODE_TYPE = 'activity';
  const ASSIGNMENT_NODE_TYPE = 'student_assignment';

  /** @var \Drupal\Core\Cache\CacheBackendInterface $cacheBackend */
  private $cacheBackend;
  private $typeList;
  private $descrList;
  const NS = 'Drupal\capytale_activity\\';

  public function __construct($cacheBackend)
  {
    $this->cacheBackend = $cacheBackend;
  }

  private function loadCfg($type)
  {
    $cfg = new \stdClass();
    $cfg->type = $type;
    $pluginNS = self::NS;
    require __dir__ . '/cfg/' . $type . '.inc';
    // Valeurs par défaut
    if (!isset($cfg->name)) $cfg->name = $type;
    if (!isset($cfg->hidden)) $cfg->hidden = false;
    else $cfg->hidden = (bool) $cfg->hidden;
    if (!isset($cfg->exportable)) $cfg->exportable = false;
    else $cfg->exportable = (bool) $cfg->exportable;
    if (!isset($cfg->detailedEvaluation)) $cfg->detailedEvaluation = false;
    else $cfg->detailedEvaluation = (bool) $cfg->detailedEvaluation;
    if (!isset($cfg->antiCheat)) $cfg->antiCheat = false;
    else $cfg->antiCheat = (bool) $cfg->antiCheat;
    $status = [];
    if (!empty($cfg->status)) {
      if (is_string($cfg->status)) {
        $status[$cfg->status] = true;
      } else {
        foreach ($cfg->status as $s) {
          $status[$s] = true;
        }
      }
    }
    $cfg->status = $status;
    if (!isset($cfg->icon)) $cfg->icon = ['path' => 'logo_false.svg', 'style' => []];
    else if (is_string($cfg->icon)) $cfg->icon = ['path' => $cfg->icon, 'style' => []];
    if (!isset($cfg->helpUrl)) $cfg->helpUrl = 'https://capytale2.ac-paris.fr/wiki/doku.php';
    if (empty($cfg->description)) {
      if (!empty($cfg->summary)) $cfg->description = $cfg->summary;
      else $cfg->description = '';
    }
    if (empty($cfg->summary)) $cfg->summary = $cfg->description;
    if (!isset($cfg->tags)) $cfg->tags = [];
    if (!isset($cfg->bundle)) $cfg->bundle = self::ACTIVITY_NODE_TYPE;
    if (!isset($cfg->attached_files_ext)) $cfg->attached_files_ext = false;
    if (!isset($cfg->options)) $cfg->options = [];
    if (!isset($cfg->role)) $cfg->role = false;
    return $cfg;
  }

  private function addWeights($weights, $cfg)
  {
    $niveau = [];
    if (empty($weights['all'][$cfg->type])) {
      $niveau['all'] = 0;
    } else {
      $niveau['all'] = $weights['all'][$cfg->type] / 100;
    }
    foreach ($weights['niveau'] as $n => $w) {
      if (empty($w[$cfg->type])) {
        $niveau[$n] = 0;
      } else {
        $niveau[$n] = $w[$cfg->type] / 100;
      }
    }
    $cfg->niveau = $niveau;
    $sujet = [];
    foreach ($weights['sujet'] as $n => $w) {
      if (empty($w[$cfg->type])) {
        $sujet[$n] = 0;
      } else {
        $sujet[$n] = $w[$cfg->type] / 100;
      }
    }
    $cfg->sujet = $sujet;
  }

  private function buildList()
  {
    $list = [];
    $all = require __dir__ . '/register.inc';
    $weights = Yaml::decode(file_get_contents(__dir__ . '/cfg/weights.yml'));
    if (!\array_key_exists('all', $weights)) $weights['all'] = [];
    if (!\array_key_exists('niveau', $weights)) $weights['niveau'] = [];
    if (!\array_key_exists('sujet', $weights)) $weights['sujet'] = [];
    foreach ($all() as $type) {
      $cfg = $this->loadCfg($type);
      $this->addWeights($weights, $cfg);
      $list[$type] = $cfg;
    }
    return $list;
  }

  /**
   * Renvoie la liste des types d'activités
   *
   * @return array ['html' => $cfg, 'pixel' => $cfg, ...]
   */
  public function getList()
  {
    if (!isset($this->typeList)) {
      $cache = $this->cacheBackend->get(self::CACHE_ID);
      if ($cache) {
        $this->typeList = $cache->data;
      } else {
        $this->typeList = $this->buildList();
        $this->cacheBackend->set(self::CACHE_ID, $this->typeList);
      }
    }
    return $this->typeList;
  }

  public function hasType($type)
  {
    return \array_key_exists($type, $this->getList());
  }

  public function getCfg($type)
  {
    if (false === $type) return false;
    if (!$this->hasType($type)) return false;
    return $this->getList()[$type];
  }

  /** @return array ['html', 'pixel', ...] */
  public function getAllTypes()
  {
    return \array_keys($this->getList());
  }

  /** @return array ['html' => 'Activité HTML', 'pixel' => 'Activité Pixel Art', ...] */
  public function getDescritionList()
  {
    if (!isset($this->descrList)) $this->descrList = $this->buildDescriptionList();
    return $this->descrList;
  }

  protected function buildDescriptionList()
  {
    $list = [];
    foreach ($this->getList() as $type => $cfg) {
      if (null === $cfg) $cfg = $this->getCfg($type);
      $list[$type] = $cfg->name;
    }
    return $list;
  }

  public static function buildShareCode(NodeInterface $node)
  {
    $nid = $node->id();
    $uuid = $node->get('uuid')->value;
    $uuid = explode('-', $uuid);
    return substr($uuid[0], 0, 4) . '-' . $nid;
  }

  public function prepare_activity($type, NodeInterface $activity)
  {
    $cfg = $this->getCfg($type);
    if (!$cfg) return false;
    if ($activity->isNew()) $activity->save();
    $actId = $activity->id();
    $activity->field_activity_type->value = $type;
    # Cas particulier des élèves
    $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
    if (!in_array('teacher', $user->getRoles())) {
      $activity->get('field_code')->setValue('student_activity');
      $activity->get('field_status_clonable')->setValue(0);
    } else {
      $activity->get('field_code')->setValue(self::buildShareCode($activity));
    }
    $activity->save();
    $ab = new ActivityBunch($activity, $cfg, false);
    return $ab->getPlayerUrl(self::CREATE);
  }

  /**
   * @deprecated
   */
  public function create_assignment(NodeInterface $activity)
  {
    $type = $this->get_activity_type($activity);
    $cfg = $this->getCfg($type);
    if (!$cfg) return false;
    $node_storage = \Drupal::entityTypeManager()->getStorage('node');
    $assignment = $node_storage->create([
      'type'           => 'student_assignment',
      'title'          => $activity->getTitle(),
    ]);
    $assignment->field_activity->entity = $activity;
    $assignment->save();
    return $assignment;
  }

  /**
   * @deprecated
   */
  public function buildPlayerUrl($type, $nid, $mode = self::CREATE)
  {
    /** @var \Drupal\node\NodeStorageInterface $nodeStorage */
    $nodeStorage = \Drupal::entityTypeManager()->getStorage('node');
    /** @var \Drupal\node\NodeInterface $n */
    $n = $nodeStorage->load($nid);
    $ab = $this->getActivityBunch($n);
    return $ab->getPlayerUrl($mode);
  }

  public function buildIconUrl($type, $mode = self::CREATE)
  {
    [$type] = self::parse_type($type);
    return self::build_icon_url($this->getCfg($type), $mode);
  }

  public function AttachedFilesExt($type)
  {
    return $this->getCfg($type)->attached_files_ext;
  }

  public function getActOptions($type)
  {
    return $this->getCfg($type)->options;
  }

  public static $_path;
  public static function path()
  {
    if (!isset(self::$_path)) {
      $mdPath = \Drupal::service('extension.list.module')->getPath('capytale_activity') . '/src/Activity/';
      self::$_path = \Drupal::service('file_url_generator')->generateString($mdPath);
    }
    return self::$_path;
  }

  /**
   * @deprecated
   */
  public static function parse_type($type)
  {
    $parts = \explode('/', $type, 2);
    $n = \count($parts);
    if ($n === 1) return [$parts[0], false];
    if ($n === 2) return [$parts[0], $parts[1] === 'sa'];
    return [false, false];
  }

  /**
   * Renvoie le type d'activité :
   * "html" | "pixel" | "console.python" | "notebook.python" | "notebook.ocaml" | "notebook.sql" | "nbhosting"
   * Si le $node est un student_assignment alors '/sa' est ajouté (par exemple "notebook.python/sa")
   * @see self::parse_type($type)
   *
   * @deprecated utiliser getActivityBunch($n)
   *
   * @param \Drupal\node\NodeInterface $node doit être de type 'student_assignment', 'notebook', 'console_activity', 'notebook_activity' ou 'activity'
   * @return false | "html" | "pixel" | "console.python" | "notebook.python" | "notebook.ocaml"
   * */
  public function get_type($node)
  {
    $bundle = $node->bundle();
    if ($bundle === self::ASSIGNMENT_NODE_TYPE) {
      if ($node->field_activity->isEmpty()) return false;
      $node = $node->field_activity->entity;
      if (!$node) return false;
      $act_type = $this->get_activity_type($node);
      if (false === $act_type) return false;
      return $act_type . '/sa';
    }
    return $this->get_activity_type($node);
  }

  /**
   * Nouvelle façon de gérer les activités. Renvoie un objet ActivityBunch
   *
   * @param \Drupal\node\NodeInterface $n
   * @return null|\Drupal\capytale_activity\Activity\ActivityBunchInterface
   */
  public function getActivityBunch($n)
  {
    if (!$n) return null;
    $bundle = $n->bundle();
    if ($bundle === self::ASSIGNMENT_NODE_TYPE) {
      if ($n->field_activity->isEmpty()) return null;
      $isSa = true;
      $an = $n->field_activity->entity;
    } else {
      if (!\in_array($bundle, [self::NB_NODE_TYPE, self::CONSOLE_NODE_TYPE, self::ACTIVITY_NODE_TYPE])) return null;
      $isSa = false;
      $an = $n;
    }
    if (!$an) return null;
    if ($an->field_activity_type->isEmpty()) return null;
    $type = $an->field_activity_type->value;
    if (!$type) return null;
    $cfg = $this->getCfg($type);
    if (!$cfg) return null;
    return new ActivityBunch($n, $cfg, $isSa);
  }

  /**
   * Cherche une activité à partir de son code
   *
   * @param string $code
   * @return null|\Drupal\capytale_activity\Activity\ActivityBunchInterface
   */
  public function findActivityBunchFromCode($code)
  {
    $nid = substr($code, 5);
    $ok = !empty($nid);
    if ($ok) {
      $ok = \ctype_digit($nid);
    }
    if ($ok) {
      $node_storage = \Drupal::entityTypeManager()->getStorage('node');
      /** @var \Drupal\node\NodeInterface $n */
      $n = $node_storage->load($nid);
      $ok = !empty($n);
    }
    if ($ok) {
      $bundle = $n->bundle();
      $ok = \in_array($bundle, [self::NB_NODE_TYPE, self::CONSOLE_NODE_TYPE, self::ACTIVITY_NODE_TYPE]);
    }
    if ($ok) {
      $codeField = $n->get('field_code');
      $ok = !empty($codeField);
    }
    if ($ok) {
      $ok = $code == $codeField->value;
    }
    if (!$ok) return null;
    return $this->getActivityBunch($n);
  }

  /**
   * Le $node ne doit pas être un student_assignment @see $this->get_type($node)
   * Renvoie le type d'activité :
   * "html" | "pixel" | "console.python" | "notebook.python" | "notebook.ocaml" | "notebook.sql" | "nbhosting"
   *
   * @param \Drupal\node\NodeInterface $node doit être de type 'notebook', 'console_activity', 'notebook_activity' ou 'activity'
   * @return false | "html" | "pixel" | "console.python" | "notebook.python" | "notebook.ocaml"
   */
  public function get_activity_type($node)
  {
    if ($node->field_activity_type->isEmpty()) {
      return false;
    } else {
      return $node->field_activity_type->value;
    }
  }

  private static function buildIconPath($icon)
  {
    return self::path() . 'logo/' . $icon;
  }

  public static function build_icon_url($cfg, $mode = self::CREATE)
  {
    if (false === $cfg) return self::path() . 'logo/logo_false.svg';
    return self::buildIconPath($cfg->icon['path']);
  }

  public static function buildIconInfo($cfg, $mode = self::CREATE)
  {
    $info = $cfg->icon;
    $info['path'] = self::buildIconPath($cfg->icon['path']);
    return $info;
  }

  public static function buildAddUrl($cfg)
  {
    return Url::fromRoute(
      'node.add',
      ['node_type' => $cfg->bundle],
      ['query' => ['type' => $cfg->type]]
    )->toString();
  }
}
