<?php

namespace Drupal\capytale_activity\Activity;

interface ActivityBunchInterface
{
    /**
     * Indique si l'activité est une copie d'élève (StudentAssignment)
     *
     * @return bool
     */
    public function isSa();

    /** @return int|string */
    public function id();

    /** @return string */
    public function getType();

    /** @return \stdClass */
    public function getCfg();

    /** @return \Drupal\node\NodeInterface */
    public function getNode();

    /** @return \Drupal\node\NodeInterface */
    public function getActivityNode();

    /** @return false|\Drupal\node\NodeInterface */
    public function getAssignmentNode();

    /**
     * @param string $mode
     * @return string
     */
    public function getPlayerUrl($mode);

    /**
     * Renvoie l'assigment de l'utilisateur s'il existe déjà, sinon renvoie null
     *
     * @param \Drupal\Core\Session\AccountInterface $user
     * @return null|ActivityBunchInterface
     */
    public function getAssignment($user);

    /**
     * Crée un assignment pour l'utilisateur. Si l'utilisateur a déjà un assignment, renvoie cet assignment.
     *
     * @param \Drupal\Core\Session\AccountInterface $user
     * @return ActivityBunchInterface
     */
    public function createAssignment($user);
}
