<?php

namespace Drupal\capytale_activity;

use Drupal\node\NodeInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use \Drupal\user\Entity\User;

use Drupal\capytale_activity\Exception\CapytaleActivityException as CAE;
use Drupal\capytale_activity\Activity\ActivityManager;
use Drupal\capytale_activity\RedirectResponse;

/**
 * Class FormHooks.
 *
 * Classe statique qui gère les hooks de formulaire.
 */
class FormHooks
{
  public static function handle_prepare_form(NodeInterface $node, $op, FormStateInterface $form_state)
  {
    $request = \Drupal::requestStack()->getCurrentRequest();
    /** @var \Drupal\capytale_activity\Activity\ActivityManager $manager */
    $manager = \Drupal::service('capytale_activity.manager');
    $unfold = [];
    $hide = [];
    $bundle = $node->bundle();
    if ($node->isNew()) {
      // C'est une création d'activité, on doit déterminer le type d'activité
      // cela peut être dans le paramètre d'url 'type' ou 'kernel';
      $request = \Drupal::requestStack()->getCurrentRequest();
      $type = $request->query->get('type', null);
      if (null === $type) {
        // Pour les bundle 'activity', le type ne peut pas être passé par le paramètre 'kernel'.
        if ($bundle === 'activity') throw new CAE('Le type d\'activité doit être spécifié en paramètre d\'URL.');
        $type = $request->query->get('kernel', null);
        if (null === $type) $type = 'python3';
        if ($bundle === 'notebook_activity') $type = 'notebook.' . $type;
        else if ($bundle === 'console_activity') $type = 'console.' . $type;
        else throw new CAE('Type d\'activité inconnu.');
      }
      if (!$manager->hasType($type)) throw new CAE('Type d\'activité inconnu : ' . $type);
      $form_state->set('activity_type', $type);
    } else {
      $nid = $node->id();
      $form_state->set('nid', $nid);
      $type = $manager->get_activity_type($node);
      $form_state->set('activity_type', $type);
      if (!$node->field_attached_files->isEmpty()) $unfold[] = 'wantfiles';
      if (!$node->field_associates->isEmpty()) {
        $unfold[] = 'assoc';
        //ksm($node->field_associates->referencedEntities());
        $associates = array();
        foreach ($node->field_associates->referencedEntities() as $n) {
          $associates[$n->id()] = $n->field_nom->value . " " . $n->field_prenom->value;
        }
        $form_state->set('associates', $associates);
      }
      if (!$node->get('field_access_tr_mode')->value) {
        $form_state->set('default-tr-mode', 'none');
      }
    }
    $ext = $manager->AttachedFilesExt($type);
    if (false === $ext) {
      $hide[] = 'attached_files';
    } else {
      /** @var \Drupal\field\Entity\FieldConfig $fd */
      $fd = $node->getFieldDefinition('field_attached_files');
      $fd->setSetting('file_extensions', $ext);
    }
    $form_state->set('actOptions', self::getActOptions($node, $manager->getActOptions($type)));
    $form_state->set('hide-fields', $hide);
    $form_state->set('unfold-fields', $unfold);
  }

  /**
   * @param NodeInterface $node
   * @param string[] $options
   * @return array|bool
   */
  protected static function getActOptions($node, $options)
  {
    if (empty($options)) return false;
    if ($node->isNew()) {
      $conf = [];
    } else {
      $confField = $node->get('field_config');
      if ($confField->isEmpty()) {
        $conf = [];
      } else {
        $conf = \json_decode($confField->value, true);
        if (empty($conf)) $conf = [];
      }
    }
    $actOptions = [];
    foreach ($options as $opt) {
      if (\array_key_exists($opt, $conf)) {
        $actOptions[$opt] = $conf[$opt];
        unset($conf[$opt]);
      } else {
        $actOptions[$opt] = false;
      }
    }
    return [$actOptions, $conf];
  }

  public static function handle_create_form_alter(&$form, FormStateInterface $form_state, $form_id)
  {
    $form['#cache']['contexts'][] = 'url.query_args:type';
    $form['#cache']['contexts'][] = 'url.query_args:kernel';
    $type = $form_state->get('activity_type');
    /** @var \Drupal\capytale_activity\Activity\ActivityManager $manager */
    $manager = \Drupal::service('capytale_activity.manager');
    if (null === $type) throw new CAE('Le type d\'activité doit être spécifié en paramètre d\'URL.');
    if (!$manager->hasType($type)) throw new CAE('Type d\'activité inconnu.');
    // Add markup to display activity type
    $cfg = $manager->getCfg($type);
    $form['activity_name'] = [
      '#type' => 'markup',
      '#markup' => $cfg->name,
    ];

    $form['#validate'][] = [__CLASS__, 'validateActivityLimit'];

    $form['actions']['submit']['#submit'][] = [__CLASS__, 'post_create_submit'];
    $form['actions']['submit']['#value'] = 'Enregistrer et voir';

    list($actOptions) = $form_state->get('actOptions');
    $hide = $form_state->get('hide-fields');
    $unfold = $form_state->get('unfold-fields');
    self::customize_form($form, $hide, $unfold, $cfg, $actOptions);
  }

  public static function validateActivityLimit(array &$form, FormStateInterface $form_state)
  {

    // Limit node creations per week
    $nbMax = 50;
    $first_day_of_week = strtotime('Last Monday');
    $last_day_of_week = strtotime('Next Monday');
    $begin = strtotime('- 7 days');
    $end = strtotime('+ 1 days');


    // return if is admin
    $groups = \Drupal::currentUser()->getRoles();
    foreach ($groups as $group) {
      if ($group == 'administrator' || $group == 'tester')
        return true;
    }

    $user_uid = \Drupal::currentUser()->id();

    // count number of nodes created by current user in last week
    $query = \Drupal::entityQuery('node');
    $query->condition('uid', $user_uid);
    // $query->condition('created', array($first_day_of_week, $last_day_of_week), 'BETWEEN');
    $query->condition('created', array($begin, $end), 'BETWEEN');
    $count = $query->count()->execute();

    // if number of posts reachs limit, stop the form from saving data
    if ($count >= $nbMax) {
      $form_state->setErrorByName('', t('Vous avez atteint la limite de @count activités créées cette semaine.', array('@count' => $count)));
    }
  }


  public static function handle_edit_form_alter(&$form, FormStateInterface $form_state, $form_id)
  {
    $form['bib_metadata_link'] = [
      '#type' => 'link',
      '#title' => 'Ouvrir les paramètres de publication dans la bibliothèque',
      '#url' => Url::fromRoute('bibliotheque.metadata', ['node' => $form_state->get('nid')]),
      '#attributes' => ['class' => ['use-ajax', 'bbe']],
    ];
    if ($default_tr_mode = $form_state->get('default-tr-mode')) {
      $form['field_access_tr_mode']['widget']['#default_value'] = $default_tr_mode;
    }

    $manager = \Drupal::service('capytale_activity.manager');
    $type = $form_state->get('activity_type');
    if (!$manager->hasType($type)) throw new CAE('Type d\'activité inconnu.');
    $cfg = $manager->getCfg($type);;
    // Add markup to display activity type
    $form['activity_name'] = [
      '#type' => 'markup',
      '#markup' => $cfg->name,
    ];
    $form['actions']['submit']['#submit'][] = [__CLASS__, 'post_edit_submit'];

    list($actOptions) = $form_state->get('actOptions');
    $hide = $form_state->get('hide-fields');
    $unfold = $form_state->get('unfold-fields');
    $associates = $form_state->get('associates');
    self::customize_form($form, $hide, $unfold, $cfg, $actOptions, $associates);

    $form['actions']['submit']['#value'] = 'Enregistrer et voir';

    $form['actions']['submit_two'] = [
      '#type' => 'submit',
      '#value' => 'Enregistrer et revenir à mes activités',
      '#attributes' => ['class' => ['my-form-submit', 'button--primary']],
      '#id' => 'submit-back',
      '#submit' => $form['actions']['submit']['#submit'],
    ];
  }

  /**
   * @param array $form
   * @param string[] $hide
   * @param string[] $unfold
   * @param array|bool $actOptions
   */
  protected static function customize_form(&$form, $hide, $unfold, $cfg, $actOptions = false, $associates = [])
  {
    $form['#theme'] = ['capytale_activity_node_form'];

    $form['revision_information']['#access'] = false;

    $form['field_status_web']['#access'] = false;
    $form['field_status_web']['#states'] = array(
      'visible' => ['input[id=edit-field-status-shared-value]' => ['checked' => TRUE]]
    );
    $form['field_abstract']['#states'] = [
      'visible' => ['input[id=edit-field-status-shared-value]' => ['checked' => TRUE]]
    ];
    $form['field_enseignement']['#states'] = [
      'visible' => [':input[id=edit-field-status-shared-value]' => ['checked' => TRUE]]
    ];
    $form['field_niveau']['#states'] = [
      'visible' => ['input[id=edit-field-status-shared-value]' => ['checked' => TRUE]]
    ];

    if (\in_array('attached_files', $hide)) {
      $form['field_attached_files']['#access'] = false;
    } else {
      $wantfiles = \in_array('wantfiles', $unfold);
      $form['wantfiles'] = [
        '#id' => "wantfiles",
        '#type' => 'checkbox',
        '#title' => 'Joindre des fichiers annexes à l\'activité',
        '#description' => 'Ces fichiers pourront être chargés dans l\'activité',
        '#default_value' => $wantfiles,
      ];
      $form['field_attached_files']['#states'] = [
        'visible' => [':input[id=wantfiles]' => ['checked' => TRUE]]
      ];
    }
    $assoc = \in_array('assoc', $unfold);
    $form['#associates'] = $associates;
    $form['assoc'] = [
      '#id' => "assoc",
      '#type' => 'checkbox',
      '#title' => 'Gérer les associés à qui vous offrez le droit de modifier cette activité',
      '#default_value' => $assoc,
    ];
    $form['field_associates']['#states'] = [
      'visible' => [':input[id=assoc]' => ['checked' => TRUE]]
    ];
    if ($actOptions) {
      $options = [];
      foreach ($actOptions as $opt => $value) {
        if ($opt === 'admonition') {
          $optionId = 'option_' . $opt;
          $options[$optionId] = array(
            '#id' => $optionId,
            '#type' => 'checkbox',
            '#title' => 'Admonitions',
            '#description' => 'Autoriser les blocs de texte encadré <a href="https://capytale2.ac-paris.fr/p/basthon/n/?kernel=python3&id=1202406&extensions=admonitions">(voir un exemple)</a>',
            '#default_value' => $value,
          );
        } else if ($opt === 'linenumbers') {
          $optionId = 'option_' . $opt;
          $options[$optionId] = array(
            '#id' => $optionId,
            '#type' => 'checkbox',
            '#title' => 'Numérotation',
            '#description' => 'Afficher les numéros de ligne dans les blocs de code',
            '#default_value' => $value,
          );
        } else if ($opt === 'sequenced') {
          $optionId = 'option_' . $opt;
          $options[$optionId] = array(
            '#id' => $optionId,
            '#type' => 'checkbox',
            '#title' => 'Séquencé',
            '#description' => '(Beta) Créer un notebook séquencé <a href="https://capytale2.ac-paris.fr/wiki/doku.php?id=notebook_sequence">Plus d\'infos...</a>',
            '#default_value' => $value,
          );
        } else if ($opt === 'romd') {
          $optionId = 'option_' . $opt;
          $options[$optionId] = array(
            '#id' => $optionId,
            '#type' => 'checkbox',
            '#title' => 'Markdown en lecture seule',
            '#description' => 'Interdire la modification des cellules markdown pour les élèves <a href="https://capytale2.ac-paris.fr/wiki/doku.php?id=read_only_markdown">Plus d\'infos...</a>',
            '#default_value' => $value,
          );
        }
        if (!empty($options)) $form['c_act_options'] = $options;
      }
    }
    self::customize_form_to_role($form, $cfg);
  }

  protected static function customize_form_to_role(&$form, $cfg)
  {
    $roles = \Drupal::currentUser()->getRoles();
    # Formulaire simplifié ou non
    if (\in_array('teacher', $roles)) {
      if (!$cfg->antiCheat) $form['field_anti_cheat']['#access'] = false;
      # ----------- Temporaire : voir aussi capytale-activity-node-form.html.twig
      # ---------- pour les testeurs seulement
      if (!\in_array('tester', $roles)) $form['field_anti_cheat']['#access'] = false;

    } else {
      unset($form['bib_metadata_link']);
      $form['assoc']['#title'] = 'Partager avec mon professeur';
      $form['field_status_clonable']['#access'] = false;
      $form['field_access_tr_mode']['#access'] = false;
      $form['field_anti_cheat']['#access'] = false;
    }

    # Formulaire simplifié pour les admin
    if (\in_array('administrator', $roles)) {
      $form['field_etiquettes'] = [];
    }

    # L'affichage dynamique de field_access_tr_mode et field_access_timerange
    # est géré dans le template
  }

  public static function post_create_submit(&$form, FormStateInterface $form_state)
  {
    /** @var \Drupal\node\NodeInterface $node */
    $node = $form_state->getFormObject()->getEntity();
    self::setActOptions($node, $form_state);
    $type = $form_state->get('activity_type');
    $node->get('field_activity_type')->setValue($type);
    $node->save();
    $url = Url::fromRoute('c-act.play', ['nid' => $node->id(), 'mode' => 'create']);
    $form_state->setResponse(new RedirectResponse($url->toString(), 301));
  }

  public static function post_edit_submit(&$form, FormStateInterface $form_state)
  {
    /** @var \Drupal\node\NodeInterface $node */
    $node = $form_state->getFormObject()->getEntity();
    self::setActOptions($node, $form_state);
    $node->save();
    if ($form_state->getTriggeringElement()['#id'] === 'submit-back') {
      // Set redirect to 'my'
      $url = Url::fromRoute('capytale_ui.my');
      $form_state->setResponse(new RedirectResponse($url->toString(), 301));
    } else {
      // Set redirect to activity player
      $url = Url::fromRoute('c-act.play', ['nid' => $form_state->get('nid'), 'mode' => 'create']);
      $form_state->setResponse(new RedirectResponse($url->toString(), 301));
    }
  }

  /**
   * @param \Drupal\node\NodeInterface $node
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  protected static function setActOptions($node, $form_state)
  {
    list($actOptions, $others) = $form_state->get('actOptions');
    if (empty($actOptions)) return;
    foreach ($actOptions as $opt => $value) {
      $optionId = 'option_' . $opt;
      $others[$opt] = $form_state->getValue($optionId) == 1 ? true : false;
    }
    $conf = \json_encode($others);
    $node->get('field_config')->setValue($conf);
  }
}
