<?php

namespace Drupal\capytale_activity\ApiController;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use Drupal\capytale_activity\Exception\CapytaleActivityApiException as CAAE;

/**
 * Controller de l'API de gestion des évaluations de capytale_activity
 */
class EvalController implements ContainerInjectionInterface
{
  const RESP_HEADER = [
    'Cache-Control' => 'no-store'
  ];

  /** @var \Drupal\Core\PageCache\ResponsePolicy\KillSwitch $ks */
  protected $ks;
  /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $etm */
  protected $etm;
  /** @var \Drupal\Component\Datetime\TimeInterface */
  protected $ts;
  /** @var \Drupal\capytale_activity\Evaluation\EvaluationServiceInterface $evalService */
  protected $evalService;
  /** @var \Drupal\Core\Session\AccountProxyInterface $cu */
  protected $cu;
  /** @var \Drupal\Core\Access\CsrfRequestHeaderAccessCheck $csfrChecker */
  protected $csfrChecker;
  /** @var \Drupal\capytale_activity\Activity\ActivityManager  $cam */
  protected $cam;

  public function __construct(
    $ks,
    $etm,
    $ts,
    $es,
    $cu,
    $csfrChecker,
    $cam
  ) {
    $this->ks = $ks;
    $this->etm = $etm;
    $this->ts = $ts;
    $this->evalService = $es;
    $this->cu = $cu;
    $this->csfrChecker = $csfrChecker;
    $this->cam = $cam;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $c)
  {
    return new static(
      $c->get('page_cache_kill_switch'),
      $c->get('entity_type.manager'),
      $c->get('datetime.time'),
      $c->get('c_act.evaluationservice'),
      $c->get('current_user'),
      $c->get('access_check.header.csrf'),
      $c->get('capytale_activity.manager'),
    );
  }

  private function _checkIsAuthenticated()
  {
      if (!$this->cu->isAuthenticated()) throw new CAAE(CAAE::ACCESS_DENIED);
  }

  private function _checkCsrf($request)
  {
      /** @var \Drupal\Core\Access\AccessResultForbidden $access */
      $access = $this->csfrChecker->access($request, $this->cu);
      if (!$access->isAllowed()) throw new CAAE(CAAE::ACCESS_DENIED, $access->getReason());
  }

  /**
   * @param string|int $nid
   * @param string|int $uid
   */
  public function handle($nid, $uid, Request $r)
  {
    $this->ks->trigger();
    try {
      if ((!\ctype_digit($nid)) || ($nid[0] === '0')) throw new CAAE(CAAE::NOT_FOUND);
      $this->_checkIsAuthenticated();
      $method = $r->getMethod();
      switch ($method) {
        case 'GET':
          if (empty($uid)) throw new CAAE(CAAE::BAD_REQUEST);
          if ((!\ctype_digit($uid)) || ($uid[0] === '0')) throw new CAAE(CAAE::NOT_FOUND);
          return $this->get($nid, $uid);
        case 'POST':
          if (!empty($uid)) throw new CAAE(CAAE::BAD_REQUEST);
          $this->_checkCsrf($r);
          return $this->post($nid, $r);
      }
    } catch (CAAE $e) {
      return new JsonResponse(['status' => 'error', 'message' => $e->getMessage()], $e->getHttpStatus());
    } catch (\Exception $e) {
      return new JsonResponse(['status' => 'error', 'message' => $e->getMessage()], 400);
    }
  }

  /**
   * @param string|int $nid
   * @param string|int $uid
   */
  protected function get($nid, $uid)
  {
    /** @var \Drupal\node\NodeStorageInterface $nsi */
    $nsi = $this->etm->getStorage('node');
    /** @var \Drupal\node\NodeInterface $n */
    $n = $nsi->load($nid);
    if (!$n) throw new CAAE(CAAE::NOT_FOUND);
    $ab = $this->cam->getActivityBunch($n);
    if (!$ab) throw new CAAE(CAAE::NOT_FOUND);
    if ($ab->isSa()) throw new CAAE(CAAE::NOT_FOUND);
    if (!$n->access('c-sa:review') && (\intval($this->cu->id()) !== \intval($uid))) throw new CAAE(CAAE::ACCESS_DENIED);
    $evals = $this->evalService->getEvaluations($nid, $uid);
    $data = [];
    foreach ($evals as $eval) {
      $data[] = [
        'uid' => $eval->getUid(),
        'graderUid' => $eval->getGraderUid(),
        'evalLabel' => $eval->getEvalLabel(),
        'evalTitle' => $eval->getEvalTitle(),
        'revisionNumber' => $eval->getRevisionNumber(),
        'score' => $eval->getScore(),
        'scoreMax' => $eval->getScoreMax(),
        'scoreLiteral' => $eval->getScoreLiteral(),
        'comment' => $eval->getComment(),
        'serverDate' => $eval->getUnixTimeStampServer(),
        'date' => $eval->getLocalDateTime(),
      ];
    }
    return new JsonResponse($data, Response::HTTP_OK, self::RESP_HEADER);
  }

  /**
   * @param string|int $nid
   * @param \Symfony\Component\HttpFoundation\Request $r
   */
  protected function post($nid, $r)
  {
    /** @var \Drupal\node\NodeStorageInterface $nsi */
    $nsi = $this->etm->getStorage('node');
    /** @var \Drupal\node\NodeInterface $n */
    $n = $nsi->load($nid);
    if (!$n) throw new CAAE(CAAE::NOT_FOUND);
    $ab = $this->cam->getActivityBunch($n);
    if (!$ab) throw new CAAE(CAAE::NOT_FOUND);
    if (!$ab->isSa()) throw new CAAE(CAAE::NOT_FOUND);
    if (!$n->access('update')) throw new CAAE(CAAE::ACCESS_DENIED);
    $body = $r->getContent();
    $start = $body[0];
    if ($start === '{') {
      $data = [\json_decode($body, true, 3)];
    } else if ($start === '[') {
      $data = \json_decode($body, true, 4);
    } else {
      throw new CAAE(CAAE::BAD_REQUEST);
    }
    $uid = $this->cu->id();
    $evals = [];
    foreach ($data as $d) {
      if (empty($d)) throw new CAAE(CAAE::BAD_REQUEST);
      $eval = $this->evalService->createEvaluation($n);
      $eval->setGraderUid($uid);
      $this->fillEval($eval, $d);
      if ($eval->isEmpty()) throw new CAAE(CAAE::BAD_REQUEST);
      $label = $eval->getEvalLabel();
      if (\array_key_exists($label, $evals)) throw new CAAE(CAAE::BAD_REQUEST);
      $evals[$label] = $eval;
    }
    $this->evalService->saveEvaluations($evals);
    // Todo: status should be Response::HTTP_CREATED (201) but ensure players are ok with that.
    return new Response('', Response::HTTP_NO_CONTENT);
  }

  /**
   * @param \Drupal\capytale_activity\Evaluation\NewEvaluationInterface $eval
   * @param array $data
   */
  protected function fillEval($eval, $data)
  {
    if (\array_key_exists('evalLabel', $data)) {
      $eval->setEvalLabel($data['evalLabel']);
    }
    if (\array_key_exists('score', $data)) {
      $score = $data['score'];
      if (\is_float($score) || \is_int($score)) {
        if ($score < 0) throw new CAAE(CAAE::BAD_REQUEST);
        if (\array_key_exists('scoreMax', $data)) {
          $max = $data['scoreMax'];
          if (!\is_float($max) && !\is_int($max)) throw new CAAE(CAAE::BAD_REQUEST);
          if ($max <= 0) throw new CAAE(CAAE::BAD_REQUEST);
          $eval->setScore($score)->setScoreMax($max);
        } else {
          throw new CAAE(CAAE::BAD_REQUEST);
        }
      } else {
        if (!\is_string($score)) throw new CAAE(CAAE::BAD_REQUEST);
        $eval->setScoreLiteral($score);
      }
    }
    if (\array_key_exists('evalTitle', $data)) {
      $eval->setEvalTitle($data['evalTitle']);
    }
    if (\array_key_exists('comment', $data)) {
      $eval->setComment($data['comment']);
    }
    if (\array_key_exists('date', $data)) {
      $eval->setLocalDateTime($data['date']);
    }
  }
}
