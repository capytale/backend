<?php

namespace Drupal\capytale_activity\ApiController;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\Cache\CacheableMetadata;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException as E404;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException as E403;

/**
 * Fournit la liste des types d'activités
 */
class ListController implements ContainerInjectionInterface
{
  /** @var \Drupal\capytale_activity\Activity\ActivityManager  $mng */
  protected $mng;
  /** @var \Drupal\Core\Session\AccountInterface $cu */
  protected $cu;

  public function __construct(
    $mg,
    $cu
  ) {
    $this->mng = $mg;
    $this->cu = $cu;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $c)
  {
    return new static(
      $c->get('capytale_activity.manager'),
      $c->get('current_user'),
    );
  }

  private function buildItem($cfg) {
    return [
      'id' => $cfg->type,
      'name' => $cfg->name,
      'status' => $cfg->status,
      'icon' => $this->mng::buildIconInfo($cfg),
      'helpUrl' => $cfg->helpUrl,
      'summary' => $cfg->summary,
      'description' => $cfg->description,
      'tags' => $cfg->tags,
      'niveau' => $cfg->niveau,
      'sujet' => $cfg->sujet,
      'bundle' => $cfg->bundle,
    ];
  }

  /**
   * Sert la route capytale_api.activity_list : '/c-act/api/t'
   */
  public function list()
  {
    $repList = [];
    if ($this->cu->isAuthenticated()) {
      $list = [];
      $roleList = [];
      foreach ($this->mng->getList() as $cfg) {
        if ($cfg->hidden) continue;
        if ($cfg->role) {
          if (! \array_key_exists($cfg->role, $roleList)) $roleList[$cfg->role] = [];
          $roleList[$cfg->role][] = $cfg;
        } else {
          $list[] = $cfg;
        }
      }
      $userRoles = $this->cu->getRoles(true);
      foreach ($roleList as $role => $cfgList) {
        if (\in_array($role, $userRoles)) {
          $list = array_merge($list, $cfgList);
        }
      }
      foreach ($list as $cfg) {
        $repList[] = $this->buildItem($cfg);
      }
    }
    $rep = new CacheableJsonResponse($repList, 200);

    $cacheMetatData = new CacheableMetadata();
    $cacheMetatData->addCacheContexts(['user.roles']);
    $cacheMetatData->setCacheMaxAge(3600);
    $rep->addCacheableDependency($cacheMetatData);
    return $rep;
  }

  /**
   * Sert la route capytale_api.activity_info : '/c-act/api/t/{type}'
   */
  public function info($type) {
    $cfg = $this->mng->getCfg($type);
    if (!$cfg) throw new E404();
    $rep = new CacheableJsonResponse($this->buildItem($cfg), 200);
    $cacheMetatData = new CacheableMetadata();
    $cacheMetatData->setCacheMaxAge(3600);
    $rep->addCacheableDependency($cacheMetatData);
    return $rep;
  }
}
