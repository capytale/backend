<?php

namespace Drupal\capytale_activity\ApiController;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;

use Drupal\capytale_activity\Exception\CapytaleActivityRestException as CERR;
use Drupal\capytale_activity\url_token;

/**
 * Controller de l'API de gestion des champs de type file system de capytale_activity
 */
class FieldController implements ContainerInjectionInterface
{
  const API_ACCESS_HDR = 'X-API-ACCESS';
  const API_ACCESS = 'capytale';
  const RESP_HEADER = [
    'Cache-Control' => 'no-store'
  ];
  protected $ks;
  /** @var \Symfony\Component\HttpFoundation\RequestStack $rs */
  protected $rs;
  /** @var \Drupal\node\NodeStorageInterface $nsi */
  protected $nsi;
  /** @var \Drupal\capytale_activity\Activity\Fields $field_service */
  protected $field_service;
  /** @var \Drupal\Component\Datetime\TimeInterface */
  protected $ts;

  public function __construct(
    $ks,
    $rs,
    $nsi,
    $fs,
    $ts
  ) {
    $this->ks = $ks;
    $this->rs = $rs;
    $this->nsi = $nsi;
    $this->field_service = $fs;
    $this->ts = $ts;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $c)
  {
    return new static(
      $c->get('page_cache_kill_switch'),
      $c->get('request_stack'),
      $c->get('entity_type.manager')->getStorage('node'),
      $c->get('capytale_activity.fields'),
      $c->get('datetime.time')
    );
  }

  /**
   * @param string|int $nid
   * @param string $field
   */
  public function handle($nid, $field)
  {
    $this->ks->trigger();
    try {
      if ((!\ctype_digit($nid)) || ($nid[0] === '0')) return new Response('', Response::HTTP_NOT_FOUND);
      $r = $this->rs->getCurrentRequest();
      $method = $r->getMethod();
      switch ($method) {
        case 'GET':
          return $this->get($nid, $field);
        case 'PUT':
          return $this->put($nid, $field);
        case 'DELETE':
          return $this->delete($nid, $field);
      }
    } catch (CERR $e) {
      // Réponse 400 vide en cas d'erreur
      return new Response('', Response::HTTP_BAD_REQUEST);
    } catch (\Exception $e) {
      // Réponse 404 vide en cas d'erreur
      return new Response('', Response::HTTP_NOT_FOUND);
    }
  }

  protected function get($nid, $name)
  {
    // Vérifie la présence du header X-API-ACCESS: capytale
    $r = $this->rs->getCurrentRequest();
    if (self::API_ACCESS !== $r->headers->get(self::API_ACCESS_HDR)) {
      return new Response('', Response::HTTP_BAD_REQUEST);
    }
    /** @var \Drupal\node\NodeInterface $n */
    $n = $this->nsi->load($nid);
    if (!$n) return new Response('', Response::HTTP_NOT_FOUND);
    $field = $this->field_service->get($n, $name);
    if (false === $field) return new Response('', Response::HTTP_NOT_FOUND);
    if (!$field->access('view')) return new Response('', Response::HTTP_FORBIDDEN);
    $file = $field->file();
    $hdr = self::RESP_HEADER;
    $hdr['Content-Type'] = $field->contentType();
    if ($file->is_null())  return new Response('', Response::HTTP_NO_CONTENT);
    BinaryFileResponse::trustXSendfileTypeHeader();
    return new BinaryFileResponse($field->file()->file_path(), 200, $hdr, false);
  }

  protected function put($nid, $name)
  {
    $r = $this->rs->getCurrentRequest();
    $length = $r->headers->get('Content-Length', false);
    if (false === $length) return new Response('', Response::HTTP_LENGTH_REQUIRED);
    if (!\ctype_digit($length)) return new Response('', Response::HTTP_BAD_REQUEST);
    $length = \intval($length);
    /** @var \Drupal\node\NodeInterface $n */
    $n = $this->nsi->load($nid);
    if (!$n) return new Response('', Response::HTTP_NOT_FOUND);
    $field = $this->field_service->get($n, $name);
    if (false === $field) return new Response('', Response::HTTP_NOT_FOUND);
    if (!$field->access('update')) return new Response('', Response::HTTP_FORBIDDEN);
    $maxlen = $field->max_len();
    if ($length > $maxlen) return new Response('', Response::HTTP_REQUEST_ENTITY_TOO_LARGE);
    $file = $field->file();
    if ($length == 0) {
      $file->truncate();
    } else {
      $input = $r->getContent(true);
      $file->set_content_from_stream($input, $length, $maxlen);
    }

    $n->setChangedTime($this->ts->getRequestTime());
    $n->save();
    return new Response('', 204);
  }

  protected function delete($nid, $name)
  {
    /** @var \Drupal\node\NodeInterface $n */
    $n = $this->nsi->load($nid);
    if (!$n) return new Response('', Response::HTTP_NOT_FOUND);
    $field = $this->field_service->get($n, $name);
    if (false === $field) return new Response('', Response::HTTP_NOT_FOUND);
    if (!$field->access('update')) return new Response('', Response::HTTP_FORBIDDEN);
    $file = $field->file();
    $file->set_null();
    $n->setChangedTime($this->ts->getRequestTime());
    $n->save();
    return new Response('', 204);
  }

  public function list($nid)
  {
    if ((!ctype_digit($nid)) || ($nid[0] === '0')) return new Response('', Response::HTTP_NOT_FOUND);
    /** @var \Drupal\node\NodeInterface $n */
    $n = $this->nsi->load($nid);
    if (!$n) return new Response('', Response::HTTP_NOT_FOUND);
    if (!$n->access('view')) return new Response('', Response::HTTP_FORBIDDEN);
    $def = $this->field_service->get_fields_def($n);
    return new JsonResponse($def);
  }

  /**
   * Répond à la route /c-act/api/ntk/{tk}/fields/{field}
   * 
   * @param string $tk
   * @param string $field
   */
  public function getByToken($tk, $field)
  {
    $this->ks->trigger();
    // Vérifie la présence du header X-API-ACCESS: capytale
    $r = $this->rs->getCurrentRequest();
    if (self::API_ACCESS !== $r->headers->get(self::API_ACCESS_HDR)) {
      return new Response('', Response::HTTP_BAD_REQUEST);
    }
    $node = url_token::getNode($this->nsi, $tk);
    if (!$node) return new Response('', Response::HTTP_NOT_FOUND);
    $field = $this->field_service->get($node, $field);
    if (false === $field) return new Response('', Response::HTTP_NOT_FOUND);
    $file = $field->file();
    $hdr = self::RESP_HEADER;
    $hdr['Content-Type'] = $field->contentType();
    if ($file->is_null())  return new Response('', Response::HTTP_NO_CONTENT);
    BinaryFileResponse::trustXSendfileTypeHeader();
    return new BinaryFileResponse($field->file()->file_path(), 200, $hdr, false);
  }
}
