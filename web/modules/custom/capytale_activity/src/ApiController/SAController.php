<?php

namespace Drupal\capytale_activity\ApiController;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use Drupal\capytale_activity\Exception\CapytaleActivityApiException as CAAE;

/**
 * Fournit la liste des assignments d'une activité
 */
class SAController implements ContainerInjectionInterface
{
  const RESP_HEADER = [
    'Cache-Control' => 'no-store'
  ];
  protected $ks;
  /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $etm */
  protected $etm;
  /** @var \Drupal\Core\Session\AccountInterface $cu */
  protected $cu;
  /** @var \Drupal\Core\Database\Connection $db */
  protected $db;
  /** @var \Drupal\capytale_activity\Activity\ActivityManager  $cam */
  protected $cam;

  public function __construct(
    $ks,
    $etm,
    $cu,
    $db,
    $cam
  ) {
    $this->ks = $ks;
    $this->etm = $etm;
    $this->cu = $cu;
    $this->db = $db;
    $this->cam = $cam;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $c)
  {
    return new static(
      $c->get('page_cache_kill_switch'),
      $c->get('entity_type.manager'),
      $c->get('current_user'),
      $c->get('database'),
      $c->get('capytale_activity.manager')
    );
  }

  private function _checkIsAuthenticated()
  {
      if (!$this->cu->isAuthenticated()) throw new CAAE(CAAE::ACCESS_DENIED);
  }

  public function list_depre($nid, Request $request) {
    $this->ks->trigger();
    return $this->_list_depre($nid, $request);
  }

  /**
   * Sert la route capytale_api.assignments : '/c-act/api/n/{nid}/assignments'
   */
  public function list($nid, Request $r) {
    $this->ks->trigger();
    try {
      if ((!\ctype_digit($nid)) || ($nid[0] === '0')) throw new CAAE(CAAE::NOT_FOUND);
      $this->_checkIsAuthenticated();
      $method = $r->getMethod();
      switch ($method) {
        case 'GET':
          return $this->_list($nid);
        default:
          throw new CAAE(CAAE::BAD_REQUEST);
      }
    } catch (CAAE $e) {
      return new JsonResponse(['status' => 'error', 'message' => $e->getMessage()], $e->getHttpStatus());
    } catch (\Exception $e) {
      return new JsonResponse(['status' => 'error', 'message' => $e->getMessage()], 400);
    }
  }

  /**
   * 
   * @deprecated
   * @param string|int $nid
   * @param \Symfony\Component\HttpFoundation\Request $request
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function _list_depre($nid, $request)
  {
    if ((!ctype_digit($nid)) || ($nid[0] === '0')) return new Response('', Response::HTTP_NOT_FOUND);
    if ($request->query->has('all')) {
      $mode = 3;
    } else if ($request->query->has('hidden')) {
      $mode = 2;
    } else {
      $mode = 1;
    }
    /** @var \Drupal\node\NodeStorageInterface $nodeStorage */
    $nodeStorage = $this->etm->getStorage('node');
    /** @var \Drupal\node\NodeInterface $n */
    $n = $nodeStorage->load($nid);
    if (!$n) return new Response('nonid', Response::HTTP_NOT_FOUND);
    if (!$n->access('c-sa:review')) return new Response('', Response::HTTP_FORBIDDEN);

    $query = $this->db->select('node__field_activity', 'fa');
    $query->condition('field_activity_target_id', $nid);
    // $query->condition('nfd.uid', $this->cu->id(), '<>');
    $query->leftJoin('node_field_data', 'nfd', 'nfd.nid = fa.entity_id');
    $query->addField('nfd', 'nid', 'nid');
    $query->addField('nfd', 'uid', 'uid');
    $query->leftJoin('user__field_nom', 'ufn', 'ufn.entity_id = nfd.uid');
    $query->addField('ufn', 'field_nom_value', 'nom');
    $query->leftJoin('user__field_prenom', 'ufp', 'ufp.entity_id = nfd.uid');
    $query->addField('ufp', 'field_prenom_value', 'prenom');
    $query->leftJoin('user__field_classe', 'ufc', 'ufc.entity_id = nfd.uid');
    $query->addField('ufc', 'field_classe_value', 'classe');
    $query->leftJoin('node__field_workflow', 'nfw', 'nfw.entity_id = nfd.nid');
    $query->addField('nfw', 'field_workflow_value', 'workflow');
    if ($mode < 3) {
      // Il y a un filtre sur la visibilité.
      $uid = $this->cu->id();
      $subquery = $this->db->select('node__field_etiquettes', 'nfe');
      $subquery->join('taxonomy_term_data', 'ttd', 'ttd.vid = \'private_tags\' AND ttd.tid = nfe.field_etiquettes_target_id');
      $subquery->join('user_term', 'ut', 'ut.tid = ttd.tid');
      $subquery->join('taxonomy_term_field_data', 'ttfd', 'ttfd.name = \'corbeille\' AND ttfd.tid = ttd.tid');
      $subquery->addExpression('1', 'one');
      $subquery->where('[nfe].[entity_id] = [nfd].[nid] AND [ut].[uid] = :uid', [':uid' => $uid]);

      if ($mode === 1) {
        $query->notExists($subquery);
      } else {
        $query->exists($subquery);
      }
    }
    $query->orderBy('nom');
    $res = $query->execute();
    $list = [];
    while ($row = $res->fetchObject()) {
      $list[] = [
        'nid' => (int)$row->nid,
        'uid' => (int)$row->uid,
        'nom' => $row->nom,
        'prenom' => $row->prenom,
        'classe' => $row->classe,
        'workflow' => (int)$row->workflow,
      ];
    }

    return new JsonResponse($list, 200, self::RESP_HEADER);
  }

  /**
   * @param string|int $nid
   * @return \Symfony\Component\HttpFoundation\Response
   */
  private function _list($nid)
  {
    /** @var \Drupal\node\NodeStorageInterface $nodeStorage */
    $nodeStorage = $this->etm->getStorage('node');
    /** @var \Drupal\node\NodeInterface $n */
    $n = $nodeStorage->load($nid);
    if (!$n) throw new CAAE(CAAE::NOT_FOUND);
    $ab = $this->cam->getActivityBunch($n);
    if (!$ab) throw new CAAE(CAAE::NOT_FOUND);
    if ($ab->isSa()) throw new CAAE(CAAE::NOT_FOUND);
    if (!$n->access('c-sa:review')) throw new CAAE(CAAE::ACCESS_DENIED);

    $sql = "WITH TRASH AS (
SELECT ttd.tid AS tid
FROM {taxonomy_term_data} ttd
JOIN {user_term} ut ON ut.tid = ttd.tid
JOIN {taxonomy_term_field_data} ttfd ON ttfd.tid = ttd.tid
WHERE ut.uid = :uid AND ttd.vid = 'private_tags' AND ttfd.name = 'corbeille'
)
SELECT
nfd.nid as nid,
nfd.uid as uid,
ufn.field_nom_value AS nom,
ufp.field_prenom_value AS prenom,
ufc.field_classe_value AS classe,
nfw.field_workflow_value AS workflow,
EXISTS(SELECT 1 FROM {node__field_etiquettes} nfe WHERE NOT nfe.deleted AND nfe.field_etiquettes_target_id IN (SELECT tid FROM TRASH) AND nfe.entity_id = nfa.entity_id) AS hidden
FROM {node__field_activity} nfa
JOIN {node_field_data} nfd ON nfd.nid = nfa.entity_id
LEFT JOIN {user__field_nom} ufn ON ufn.entity_id = nfd.uid AND NOT ufn.deleted
LEFT JOIN {user__field_prenom} ufp ON ufp.entity_id = nfd.uid AND NOT ufp.deleted
LEFT JOIN {user__field_classe} ufc ON ufc.entity_id = nfd.uid AND NOT ufc.deleted
LEFT JOIN {node__field_workflow} nfw ON nfw.entity_id = nfd.nid AND NOT nfw.deleted
WHERE nfa.field_activity_target_id = :nid AND NOT nfa.deleted
ORDER BY nom";

    $res = $this->db->query($sql, [':nid' => $nid, ':uid' => $this->cu->id()]);

    $list = [];
    while ($row = $res->fetchObject()) {
      $list[] = [
        'nid' => (int)$row->nid,
        'uid' => (int)$row->uid,
        'lastname' => $row->nom,
        'firstname' => $row->prenom,
        'classe' => $row->classe,
        'workflow' => (int)$row->workflow,
        'hidden' => !!$row->hidden,
      ];
    }

    return new JsonResponse($list, 200, self::RESP_HEADER);
  }
}
