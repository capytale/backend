<?php

namespace Drupal\tags_tree\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use \RecursiveIteratorIterator;

/**
 * Provides a 'EtiquettesTreeBlock' block.
 *
 * @Block(
 *  id = "etiquettes_tree_block",
 *  admin_label = @Translation("Mes étiquettes"),
 * )
 */
class EtiquettesTreeBlock extends BlockBase {


  /**
   * {@inheritdoc}
   */
  public function build() {

    $user = \Drupal::currentUser();
    $roles = $user->getRoles();
    $terms = array();
    if ( !in_array('administrator', $roles)) {
      $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('private_tags');
    }

    $renderable = [
      '#theme' => 'etiquettes_tree_template',
      '#terms' => $terms,
      '#colors' => getColors($terms),
    ];
    // todo : max-age 0
    return $renderable;

  }

}

function getColors($array) {

  //Base case: an empty array produces no list
  if (empty($array)) return array();
  $colors = array();

  foreach ($array as $el) {
    if (is_object($el)) {
      if (property_exists($el, 'tid')) {
        $database = \Drupal::database();
        $query = $database->select('taxonomy_term__field_color', 'fc');
        $query->condition('fc.entity_id', $el->tid);
        $query->fields('fc', ['field_color_color']);
        $result = $query->execute();
        $color = "";
        foreach ($result as $record) {
          $color.= $record->field_color_color;
        }
        $colors[$el->tid] = $color;
      }
    }
  }

  return $colors;
}
