<?php

namespace Drupal\tags_tree\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use \RecursiveIteratorIterator;

/**
 * Provides a 'EtiquetageTreeBlock' block.
 *
 * @Block(
 *  id = "etiquetage_tree_block",
 *  admin_label = @Translation("Étiqueter"),
 * )
 */
class EtiquetageTreeBlock extends BlockBase {


  /**
   * {@inheritdoc}
   */
  public function build() {

    $user = \Drupal::currentUser();
    $roles = $user->getRoles();
    $terms = array();
    if ( !in_array('administrator', $roles)) {
      $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('private_tags');
    }

    $renderable = [
      '#theme' => 'etiquetage_tree_template',
      '#terms' => $terms,
      '#colors' => getTheColors($terms),
    ];
    return $renderable;

  }

}

function getTheColors($array) {

  //Base case: an empty array produces no list
  if (empty($array)) return array();
  $colors = array();

  //Recursive Step: make a list with child lists
  foreach ($array as $subArray) {
    if (is_object($subArray)) {
      if (property_exists($subArray, 'tid')) {
        $database = \Drupal::database();
        $query = $database->select('taxonomy_term__field_color', 'fc');
        $query->condition('fc.entity_id', $subArray->tid);
        $query->fields('fc', ['field_color_color']);
        $result = $query->execute();
        $color = "";
        foreach ($result as $record) {
          $color.= $record->field_color_color;
        }
        $colors[$subArray->tid] = $color;
        $colors = $colors + getTheColors($subArray);
      }
    }
  }

  return $colors;
}
