<?php

namespace Drupal\tags_tree\Commands;

use Drush\Commands\DrushCommands;
use Drupal\Core\Database\Connection;


/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 */
class CreateRecycleTagCommands extends DrushCommands
{
  const RECYCLE_LABEL = 'Corbeille';
  const TAGS_VID = 'private_tags';
  /** @var \Drupal\taxonomy\VocabularyStorage $vocab_storage */
  protected $vocab_storage;

  /** @var \Drupal\taxonomy\TermStorage $term_storage */
  protected $term_storage;

  /** @var \Drupal\Core\Database\Connection $db */
  protected $db;


  /**
   * @param Drupal\Core\Entity\ContentEntityStorageInterface $entityTypeManager
   */
  public function __construct($entityTypeManager, $db)
  {
    parent::__construct();
    $this->vocab_storage = $entityTypeManager->getStorage('taxonomy_vocabulary');
    $this->term_storage = $entityTypeManager->getStorage('taxonomy_term');
    $this->db = $db;
  }

  /**
   * @command capytale:CreateRecycleTag
   * @usage capytale:CreateRecycleTag
   *   Crée les corbeilles des utilisateurs.
   */
  public function CreateRecycleTag() {
    $vid = $this->get_vocab_id();
    if (false === $vid) {
      $this->output->writeln('Vocabulaire private_tags introuvable.');
    }
    $uid = 8698;

    $subquery = $this->db->select('user__roles', 'ur');
    $subquery->addExpression('1', 'one');
    $subquery->where('[ur].[entity_id] = [u].[uid]');
    $subquery->condition('ur.roles_target_id', 'administrator');

    $query = $this->db->select('users', 'u');
    $query->fields('u', ['uid']);
    $query->notExists($subquery);
    $query->condition('u.uid', 0, '>');

    $uids = $query->execute()->fetchCol();

    $count = 0;
    $already = 0;
    $created = 0;
    foreach ($uids as $uid) {
      $count++;
      list($found, $maxweight) = $this->has_recycle($vid, $uid);
      if ($found) {
        $already++;
      } else {
        $this->add_recycle($vid, $uid, $maxweight + 1);
        $created++;
      }
      if ($count % 100 == 0) {
        $this->output->writeln('Traités : ' . $count . ', créés : ' . $created . ', existants : ' . $already);
      }
    }
    $this->output->writeln('Traités : ' . $count . ', créés : ' . $created . ', existants : ' . $already);
  }

  protected function has_recycle($vid, $uid) {
    $query = $this->db->select('user_term', 'ut');
    $query->condition('ut.uid', $uid);
    $query->addField('ut', 'tid');
    $tids = $query->execute()->fetchCol();
    $terms = $this->term_storage->loadMultiple($tids);
    $found = false;
    $maxweight = 0;
    foreach ($terms as $term) {
      /** @var \Drupal\taxonomy\TermInterface $term */
      $iw = $term->getWeight();
      if ($iw > $maxweight) $maxweight = $iw;
      if (false !== \stripos(self::RECYCLE_LABEL, $term->name->value)) $found = true;
    }
    return [$found, $maxweight];
  }

  protected function add_recycle($vid, $uid, $weight) {
    $term = $this->term_storage->create([
      'name' => self::RECYCLE_LABEL,
      'vid' => $vid,
      'weight' => $weight
    ]);
    $term->save();
    $result = $this->db->update('user_term')
      ->fields(['uid' => $uid])
      ->condition('tid', $term->id())
      ->execute();
  }

  protected function get_vocab_id() {
    $vocabs = $this->vocab_storage->loadByProperties(['vid' => self::TAGS_VID]);
    if (\count($vocabs) !== 1) {
      return false;
    }
    /** @var \Drupal\taxonomy\Entity\Vocabulary $tags_vocab */
    $tags_vocab = \array_pop($vocabs);
    return $tags_vocab->id();
  }


}
