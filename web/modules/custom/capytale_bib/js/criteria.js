(function ($, Drupal) {
  Drupal.behaviors.criteria = {
    attach: function (context) {

      $('[data-toggle="tooltip"]').tooltip()

      $("#total").html("1 pt");
      $("#critlabel").html('<i class="far fa-check-circle"></i> A sa place dans la bibliothèque');



      $("#clear", context).on("click", function () {
        console.log("clear", $(this));
        $(".rating__star")
          .each(function () {
            $(this).removeClass("active");
            if ($(this).children().hasClass("fa-check")) {
              $(this).addClass("default");
            }

          });

        $("*[data-drupal-selector='edit-field-criteria-0']")
          .find("select")
          .val(1)
          .change();
        $("#total").html("1 pt");
        $("#critlabel").html('<i class="far fa-check-circle"></i> A sa place dans la bibliothèque');
      });

      selector('div[id^="rating"]');

      function selector(id) {
        $(id)
          .find(".rating__star")
          .on("click", function () {
            var $this = $(this);
            console.log(
              "select",
              $("*[data-drupal-selector='" + $this.data("target") + "']")
            );
            console.log("val", $this.data("val"));
            $this.parent().children().removeClass("default");
            $this.addClass("active");
            $this.siblings().removeClass("active");
            $("*[data-drupal-selector='" + $this.data("target") + "']")
              .val($this.data("val"))
              .change();
            let s = sumup();
            let pt = (s>-2 && s<2)? " pt" : " pts";
            $("#total").html(s + pt);
            if (s < 0) {
              var icon =
                '<i class="fas fa-ban"></i> À dépublier de la bibliothèque';
            } else if (s > 10) {
              var icon =
                '<i class="fas fa-medal"></i> Mérite le Label de Grande Qualité';
            } else {
              var icon =
                '<i class="far fa-check-circle"></i> A sa place dans la bibliothèque';
            }
            $("#critlabel").html(icon);
          });
      }

      function sumup() {
        var sum = 0;
        $("*[data-drupal-selector='edit-field-criteria-0']")
          .find("select")
          .each(function () {
            sum = sum + (parseInt($(this).val()) || 0);
          });
        return sum;
      }
    },
  };
})(jQuery, Drupal);
