jQuery(document).ready(function($) {

  var table = $('#mytable').DataTable({
    //"ajax": 'export',
    "ajax": {
      "url": "/web/export",
      "dataSrc": ""
    },
    "deferRender": true,
    "order": [[7, "desc"]],
    "fnDrawCallback": function(oSettings) {
      $('[data-bs-toggle="tooltip"]').tooltip();
    },
    "columns": [
      // { "data": "a" },
      {
        "data": null,
        "width": "8em",
        render: function(data, type, row) {
          return '<span class="hidden">'
          + data.type
          + '</span>'
          + '<img src="' + data.icon + '">'
        },
      },
      {
        "data": null,
        "width": "8em",
        render: function(data, type, row) {
          return '<a href="' + data.player_url + '">' + data.title + '</a>'
        },
      },
      {
        data: null,
        "orderable": false,
        render: function(data, type, row) {
          return '<div href="' + data.b_url + '" '
            + 'style="cursor:pointer"'
            + 'data-toshake="icon' + data.nid + '" '
            + 'data-bs-toggle="tooltip" data-bs-placement="top" '
            + 'title="Copier le lien de partage direct entre enseignants"'
            + '          id="cpcode' + data.nid + '" data-url="' + data.b_url + '" data-toshake="bloc' + data.nid + '" '
            + '          onclick="copyToMyClipboard(\'cpcode' + data.nid + '\', 1.9)">'
            + '<i id="icon'+data.nid+'" class="fas fa-link" style="color: #003CC5;"></i>'
            + '</div>'
        },
      },
      {
        "data": "c",
        "width": "85em",
      },
      { "data": "d" },
      {
        data: null,
        render: function(data, type, row) {
          return '<span class="hidden">'
            + '0'.repeat(5 - data.nb_star.length) + data.nb_star
            + '</span>'
            + data.nb_star +
            ' <a href="' + data.url_star + '"><i class="' + data.star_status + ' fa-star"></i></a>'
        },
      },
      {
        data: null,
        // type: "natural", // cf https://datatables.net/plug-ins/sorting/natural
        render: function(data, type, row) {
          return '<span class="hidden">'
            + '0'.repeat(5 - data.nb_clone.length) + data.nb_clone
            + '</span>'
            + data.nb_clone
            + ' <span class="mytooltip"><a href="'
            + data.url_clone +
            '"><i class="far fa-clone"></i></a><span class="mytooltiptext">Créer une copie dans mes activités</span></span>'
        },
      },
      {
        data: {
          _: "date.display",
          sort: "date.timestamp",
          "orderSequence": ["desc", "asc"],
        }
      },
      { "data": "g" }
    ],
    "columnDefs": [
      {
        // The `data` parameter refers to the data for the cell (defined by the
        // `data` option, which defaults to the column being worked with, in
        // this case `data: 0`.
        "render": function(data, type, row) {
          return '<img src="' + data + '">';
        },
        "targets": 0
      },
    ],
    "language": {
      search: "Rechercher ",
      "info": "lignes _START_ à _END_ sur _TOTAL_",
      "lengthMenu": "Afficher _MENU_ lignes",
      "loadingRecords": '<i class="fa fa-spinner fa-spin" style="font-size:48px;"></i>',
      "paginate": {
        "first": "Première page",
        "last": "Dernière page",
        "next": "Page suivante",
        "previous": "Page précédente"
      },
    },
  });

});

function copyToMyClipboard(id, factor) {
  var el = document.getElementById(id);
  copyTextToClipboard(el.dataset.url);

  var elem = document.getElementById(el.dataset.toshake);
  console.log(factor);
  elem.animate([
    {
      transform: 'scale(1,1)'
    },
    {
      transform: "scale(" + factor + ", " + factor + ")"
    },
    {
      transform: 'scale(1,1)'
    },
    {
      transform: "scale(" + factor + ", " + factor + ")"
    },
    {
      transform: 'scale(1,1)'
    }],
    {
      duration: 400
    });
}
function fallbackCopyTextToClipboard(text) {
  var textArea = document.createElement("textarea");
  textArea.value = text;

  // Avoid scrolling to bottom
  textArea.style.top = "0";
  textArea.style.left = "0";
  textArea.style.position = "fixed";

  document.body.appendChild(textArea);
  textArea.focus();
  textArea.select();

  try {
    var successful = document.execCommand('copy');
    var msg = successful ? 'successful' : 'unsuccessful';
    console.log('Fallback: Copying text command was ' + msg);
  } catch (err) {
    console.error('Fallback: Oops, unable to copy', err);
  }

  document.body.removeChild(textArea);
}
function copyTextToClipboard(text) {
  if (!navigator.clipboard) {
    fallbackCopyTextToClipboard(text);
    return;
  }
  navigator.clipboard.writeText(text).then(function() {
    console.log('Async: Copying to clipboard was successful!');
  }, function(err) {
    console.error('Async: Could not copy text: ', err);
  });
}
