(function ($, Drupal, drupalSettings) {
  $(document).ready(function ($) {

    $('[data-toggle="tooltip"]').tooltip();

    $('.comment-clickable').on('click', function() {
      const nid = $(this).data("nid");
      console.log('Opening comment modal for: ' + nid);
      const ajaxSettings = {
        url: 'c-act/'+nid+'/bibcomments',
        dialogType: 'modal',
        dialog: { width: 800, title: 'Commentaires' },
      }
      const ajaxObject = Drupal.ajax(ajaxSettings);
      ajaxObject.execute();
    });

    $('.mysharecode').on('click', function() {
      copyToMyClipboard("cpcode"+$(this).data('nid'), 2.5);
    });

    Drupal.AjaxCommands.prototype.ChangeNbComm = function (ajax, response, status) {
      try {
        id = "#count-" + response.nid;
        $(id).parent().addClass('color-red');
        setTimeout(function() { $(id).parent().removeClass('color-red'); }, 3000);

        $(id).fadeOut(1000, function() {
          $(this).text(response.nbComm).fadeIn(2500);
        });

      } catch { }
    };

    });
  })(jQuery, Drupal, drupalSettings);



function copyToMyClipboard(id, factor) {
  var el = document.getElementById(id);
  copyTextToClipboard( window.location.origin+el.dataset.url);

  var elem = document.getElementById(el.dataset.toshake);
  console.log(el.dataset.toshake)
  console.log(factor);
  elem.animate([
    {
      transform: 'scale(1,1)'
    },
    {
      transform: "scale(" + factor + ", " + factor + ")"
    },
    {
      transform: 'scale(1,1)'
    },
    {
      transform: "scale(" + factor + ", " + factor + ")"
    },
    {
      transform: 'scale(1,1)'
    }],
    {
      duration: 400
    });
}
function fallbackCopyTextToClipboard(text) {
  var textArea = document.createElement("textarea");
  textArea.value = text;

  // Avoid scrolling to bottom
  textArea.style.top = "0";
  textArea.style.left = "0";
  textArea.style.position = "fixed";

  document.body.appendChild(textArea);
  textArea.focus();
  textArea.select();

  try {
    var successful = document.execCommand('copy');
    var msg = successful ? 'successful' : 'unsuccessful';
    console.log('Fallback: Copying text command was ' + msg);
  } catch (err) {
    console.error('Fallback: Oops, unable to copy', err);
  }

  document.body.removeChild(textArea);
}
function copyTextToClipboard(text) {
  if (!navigator.clipboard) {
    fallbackCopyTextToClipboard(text);
    return;
  }
  navigator.clipboard.writeText(text).then(function () {
    console.log('Async: Copying to clipboard was successful!');
  }, function (err) {
    console.error('Async: Could not copy text: ', err);
  });
}
