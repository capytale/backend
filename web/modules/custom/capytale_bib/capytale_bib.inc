<?php

/**
 * @file
 * Contains utility functions.
 */

/**
 * Returns the array of criteria.
 */
function capytale_bib_get_criteria()
{
  $criteria = [
    'eth' => [
      "title" => "Éthique",
      "weight" => 1,
      "icon" => "<i class=\"fas fa-balance-scale\"></i>",
      "values" => array(-1000 => -1000, 1 => 1),
      'default_value' => 1,
      "icons" => [
        -1000 => "<i class=\"rating__star fas fa-times\"></i>",
        1  => "<i class=\"rating__star fas fa-check\"></i>",
      ],
    ],
    'clr' => [
      "title" => "Clarté",
      "weight" => 1,
      "icon" => "<i class=\"fas fa-glasses\"></i>",
      "values" => array(-2 => -2, 0 => 0, 1 => 1, 2 => 2),
      'default_value' => 0,
      "icons" => [
        -2 => "<i class=\"rating__star fas fa-times\"></i>",
        0  => "<i class=\"rating__star fas fa-minus fa-xs\"></i>",
        1  => "<i class=\"rating__star fas fa-check\"></i>",
        2  => "<i class=\"rating__star far fa-heart\"></i>",
      ],
    ],
    'fon' => [
      "title" => "Fond",
      "weight" => 1,
      "icon" => "<i class=\"far fa-lightbulb\"></i>",
      "values" => array(-2 => -2, 0 => 0, 1 => 1, 2 => 2),
      'default_value' => 0,
      "icons" => [
        -2 => "<i class=\"rating__star fas fa-times\"></i>",
        0  => "<i class=\"rating__star fas fa-minus fa-xs\"></i>",
        1  => "<i class=\"rating__star fas fa-check\"></i>",
        2  => "<i class=\"rating__star far fa-heart\"></i>",
      ],
    ],
    'uti' => [
      "title" => "Utilisabilité",
      "weight" => 1,
      "icon" => "<i class=\"fas fa-recycle\"></i>",
      "values" => array(-2 => -2, 0 => 0, 1 => 1, 2 => 2),
      'default_value' => 0,
      "icons" => [
        -2 => "<i class=\"rating__star fas fa-times\"></i>",
        0  => "<i class=\"rating__star fas fa-minus fa-xs\"></i>",
        1  => "<i class=\"rating__star fas fa-check\"></i>",
        2  => "<i class=\"rating__star far fa-heart\"></i>",
      ],
    ],
    'per' => [
      "title" => "Pertinence",
      "weight" => 1,
      "icon" => "<i class=\"fas fa-bullseye\"></i>",
      'default_value' => 0,
      "values" => array(-2 => -2, 0 => 0, 1 => 1, 2 => 2),
      "icons" => [
        -2 => "<i class=\"rating__star fas fa-times\"></i>",
        0  => "<i class=\"rating__star fas fa-minus fa-xs\"></i>",
        1  => "<i class=\"rating__star fas fa-check\"></i>",
        2  => "<i class=\"rating__star far fa-heart\"></i>",
      ],
    ],
    'app' => [
      "title" => "Apparence",
      "weight" => 1,
      "icon" => "<i class=\"fas fa-spell-check\"></i>",
      "values" => array(-2 => -2, 0 => 0, 1 => 1, 2 => 2),
      'default_value' => 0,
      "icons" => [
        -2 => "<i class=\"rating__star fas fa-times\"></i>",
        0  => "<i class=\"rating__star fas fa-minus fa-xs\"></i>",
        1  => "<i class=\"rating__star fas fa-check\"></i>",
        2  => "<i class=\"rating__star far fa-heart\"></i>",
      ],
    ],
  ];
  return $criteria;
}
