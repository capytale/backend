<?php

/**
 * @file
 * The module file.
 */

/**
 * Implements hook_views_data().
 */
function capytale_bib_views_data()
{
  $data['views']['table']['group'] = 'content';
  $data['views']['table']['join'] = [
    // #global is a special flag which allows a table to appear all the time.
    '#global' => [],
  ];

  $data['views']['the_type'] = [
    'title' => 'Le type d\'activité',
    'help' => 'Renvoie le type d\'activité indépendemment de tout',
    'field' => [
      'id' => 'the_type_views_field', // cf Plugin/views/field/TheTypeViewsField
    ],
    // 'filter' => [
    //   'title' => 'Type d activité - Custom Filter',
    //   'help' => 'Fournit un filtre sur les types d activité (type, activity_type ou kernel)',
    //   'field' => 'id',
    //   //cf Plugin/views/field/theTypeViewsFilter
    //   'id' => 'the_type_views_filter',
    // ],
  ];
  $data['views']['player_url'] = [
    'title' => 'Url du player',
    'help' => 'Renvoie l\'URL du player pour l\'activité',
    'field' => [
      'id' => 'player_url_views_field', // cf Plugin/views/field/PlayerUrlViewsField
    ],
  ];
  $data['views']['clone_url'] = [
    'title' => 'Url de clonage',
    'help' => 'Renvoie l\'URL de clonage de l\'activité',
    'field' => [
      'id' => 'clone_url_views_field', // cf Plugin/views/field/PlayerUrlViewsField
    ],
  ];
  return $data;
}
