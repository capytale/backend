<?php
namespace Drupal\capytale_bib\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\capytale_bib\ApiController\BibAjax;
use Drupal\capytale_activity\url_token;

class Permalink extends ControllerBase
{

  public function permalink($mode, $nidOrTk) {
    if ($mode === 'id') return $this->permalink_id($nidOrTk);
    if ($mode === 'tk') return $this->permalink_tk($nidOrTk);
    return $this->_error();
  }


  protected function permalink_tk($tk) {
    $decodeTk = url_token::decode($tk);
    $nid = $decodeTk->id();
    if (!$nid) return $this->_error();
    $node = \Drupal\node\Entity\Node::load($nid);
    if (!$node) return $this->_error();
    if (! $decodeTk->validate($node->uuid())) return $this->_error();
    
    return $this->_permalink($node);
  }

  protected function permalink_id($nid) {
    $node = \Drupal\node\Entity\Node::load($nid);
    if (!$node)  return $this->_error();
    $shared = $node->get('field_status_shared')->value;
    $web = $node->get('field_status_web')->value;
    $uid = \Drupal::currentUser()->id();

    $test1 = $uid && $shared;
    $test2 = $shared && $web;

    if ($test1 || $test2) return $this->_permalink($node);
    return $this->_error();
  }

  protected function _permalink($node) {
    $owner = $node->getOwner();

    $term_objects = $node->get('field_theme')->referencedEntities();
    $themes = [];
    foreach ($term_objects as $term_object) {
      $themes[] = $term_object->label();
    }

    $term_objects = $node->get('field_modules')->referencedEntities();
    $modules = [];
    foreach ($term_objects as $term_object) {
      $modules[] = $term_object->label();
    }

    $dsc = strip_tags($node->get('field_description')->value);
    $abs = strip_tags($node->get('field_abstract')->value);
    $abstract = (strlen($abs)>1)? $abs : $dsc;

    $c_act_manager = \Drupal::service('capytale_activity.manager');
    $type_ori = $node->getType();
    $type = $node->get('field_activity_type')->value;
    $icon = $c_act_manager->buildIconUrl($type);

    return [
      '#theme' => 'permalink',
      '#vars' => [
        'nid' => $node->id(),
        'title' => $node->get('title')->value,
        'type' => $type,
        'changed' => $node->get('changed')->value,
        'type_ori' => $type_ori,
        'type' => $type,
        'enseignement' => $node->get('field_enseignement')->getString(),
        'theme' => implode(', ', $themes),
        'modules' => implode(', ', $modules),
        'niveau' => $node->get('field_niveau')->getString(),
        'abstract' => $abstract,
        'nom' => $owner->get('field_nom')->value,
        'prenom' => $owner->get('field_prenom')->value,
        'icon' => $icon,
      ],
    ];
}


  protected function _error() {
    return [
      '#theme' => 'permalink',
      '#vars' => [
        'nid' => 0,
      ],
    ];
  }

}

