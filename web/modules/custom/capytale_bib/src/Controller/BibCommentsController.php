<?php
namespace Drupal\capytale_bib\Controller;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CommandInterface;
use Drupal\Core\Ajax\OpenModalDialogCommand;

class BibCOmmentsController implements ContainerInjectionInterface {

  /** @var \Drupal\Core\Form\FormBuilderInterface $efb */
  protected $efb;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container)
  {
    return new static($container->get('entity.form_builder'));
  }

  /**
   * @param \Drupal\Core\Form\FormBuilderInterface $efb
   */
  public function __construct($efb)
  {
    $this->efb = $efb;
  }

  /**
   * @param \Drupal\node\NodeInterface $node
   */
  public function form($node) {

    $view_builder = \Drupal::entityTypeManager()->getViewBuilder('node');
    $field = $node->get('comment');
    $view = $view_builder->viewField($field);

    return $view;
  }
}
