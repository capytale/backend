<?php
namespace Drupal\capytale_bib\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\user\Entity\User;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

class BibStar extends ControllerBase {

  public function setstar($nid) {
    $userid = \Drupal::currentUser()->id();
    $user = User::load($userid);
    $roles = $user->getRoles();
    $key = array_search('authenticated', $roles);
    unset($roles[$key]);
    $referer = \Drupal::request()->server->get('HTTP_REFERER');
    if ( !in_array('teacher',$roles)) {
      return new TrustedRedirectResponse($referer);
    }

    $sa_list = \Drupal::entityTypeManager()
      ->getStorage('bibstar_entity')
      ->loadByProperties([
        'name' => 'bibstar',
        'nid' => $nid,
        'uid' => $userid,
      ]);
    $count = 0;
    foreach ( $sa_list as $sa ) { $count++; }

    \Drupal::messenger()->addStatus("$count - $nid,$userid");
    if ($count == 0) {
      // Add bibstar
      $entity = \Drupal::entityTypeManager()
        ->getStorage('bibstar_entity')
        ->create([
          'type' => 'bibstar',
          'uid' => $userid,
          'nid' => $nid,
          'name' => 'bibstar',
        ]);
      $entity->save();
    }
    else {
      // Remove bibstar
      $query = \Drupal::entityQuery('bibstar_entity')
        ->condition('nid', $nid)
        ->condition('uid', $userid);
      $ids = $query->execute();

      // Load these entities ($uids) in our case using storage controller.
      // We call loadMultiple method and give $uids array as argument.
      $itemsToDelete = \Drupal::entityTypeManager()->getStorage('bibstar_entity')
                                                   ->loadMultiple($ids);

      // Loop through our entities and deleting them by calling by delete method.
      foreach ($itemsToDelete as $item) {
        $item->delete();
      }
    }


    return new RedirectResponse(Url::fromRoute('bibliotheque.bibajax')->toString());
  }

}
