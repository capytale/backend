<?php

namespace Drupal\capytale_bib\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

class Bib extends ControllerBase
{

  public function bibajax()
  {
    $roles = \Drupal::currentUser()->getRoles();
    return [
      '#theme' => 'bibajax',
      '#vars' => [
        'isTeacher' => in_array('teacher', $roles),
      ],
    ];
  }

  public function bibview()
  {
    $roles = \Drupal::currentUser()->getRoles();
    if (in_array('teacher', $roles)) {
      $url = Url::fromRoute('view.bibliotheque.page_2', [], ['query' => ['field_status_web_value' => 1]]);
    } else {
      $url = Url::fromRoute('view.bibliotheque.page_1');
    }

    return new RedirectResponse($url->toString());


  }
}
