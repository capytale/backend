<?php

namespace Drupal\capytale_bib;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormAjaxException;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Ajax\CommandInterface;
use Drupal\Core\Ajax\MessageCommand;
use Drupal\Core\Messenger\MessengerInterface;


class FackeMessenger implements MessengerInterface
{
  public function addMessage($message, $type = self::TYPE_STATUS, $repeat = FALSE)
  {
  }
  public function addStatus($message, $repeat = FALSE)
  {
  }
  public function addError($message, $repeat = FALSE)
  {
  }
  public function addWarning($message, $repeat = FALSE)
  {
  }
  public function all()
  {
  }
  public function messagesByType($type)
  {
  }
  public function deleteAll()
  {
  }
  public function deleteByType($type)
  {
  }
}

/**
 * Class FormHooks.
 *
 * Classe statique qui gère les hooks de formulaire.
 */
class FormHooks
{

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param string $form_id
   */
  public static function handle_form_alter(&$form, FormStateInterface $form_state, $form_id)
  {
    /** @var \Drupal\node\NodeForm $formObj */
    $formObj = $form_state->getFormObject();
    $formObj->setMessenger(new FackeMessenger());
    /** @var \Drupal\node\NodeInterface $node */
    $node = $form_state->getFormObject()->getEntity();
    $form['#title'] = $node->getTitle();
    $form['revision_information']['#access'] = false;
    $form['#theme'] = ['bibliotheque_metadata_form'];

    if (\array_key_exists('field_status_web', $form)) {
      $form['field_status_web']['#states'] = [
        'enabled' => [':input[data-drupal-selector=edit-field-status-shared-value]' => ['checked' => true]]
      ];

      $form['display'] = array(
        '#type' => 'container',
        '#states' => array(
          'visible' => array(
            ':input[data-drupal-selector=edit-field-status-shared-value]' => array('checked' => TRUE),
          ),
        ),
      );
      $form['display']['bib_sharing_url'] = [
        '#type' => 'markup',
        '#markup' => "Lien vers l'activité dans la bibliothèque : http://capytale2.ac-paris.fr/web/b/" . $node->id(),
      ];
    }


    $form['actions']['submit_cancel'] = [
      '#type' => 'submit',
      '#weight' => 999,
      '#value' => \t('Cancel'),
      '#limit_validation_errors' => [],
      '#ajax' => [
        'callback' => [__CLASS__, 'handleCancel'],
        'disable-refocus' => true
      ]
    ];
    $form['#validate'][] = [__CLASS__, 'formValidate'];
    $form['actions']['submit']['#ajax'] = [
      'disable-refocus' => true,
      'callback' => [__CLASS__, 'handleSubmit'],
    ];
    $form['actions']['delete']['#access'] = false;
  }

  public static function handle_comment_form_alter(&$form, FormStateInterface $form_state, $form_id)
  {
    $form['#attached']['library'][] = 'capytale_bib/criteria';
    $form['actions']['submit']['#ajax'] = [
      'disable-refocus' => true,
      'callback' => [__CLASS__, 'handleCommentSubmit'],
    ];
    $form['field_criteria']['#attributes']['class'][] = 'hiddddddddden';
    #$form['field_criteria']['widget'][0]['criteria']['clr']['#access'] = FALSE;
    #kint($form);
  }


  public static function handleCancel(&$form, $form_state)
  {
    \Drupal::messenger()->deleteAll();
    $response = new AjaxResponse();
    $response->addCommand(new CloseModalDialogCommand());
    return $response;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public static function formValidate(&$form, $form_state)
  {
    if (!$form_state->getValue('field_status_shared')['value']) return;
    if (!self::abstractValidate($form_state)) {
      $form_state->setErrorByName('field_abstract', 'Merci de saisir un résumé de 50 caractères au minimum');
    }
    if (!self::enseignementValidate($form_state)) {
      $form_state->setErrorByName('field_enseignement', 'Merci d\'indiquer au moins un enseignement');
    }
    if (!self::niveauValidate($form_state)) {
      $form_state->setErrorByName('field_niveau', 'Merci d\'indiquer au moins un niveau');
    }
  }

  protected static function abstractValidate($form_state)
  {
    if (\count($form_state->getValue('field_abstract')) < 1) return false;
    if (\strlen($form_state->getValue('field_abstract')[0]['value']) < 50) return false;
    return true;
  }
  protected static function enseignementValidate($form_state)
  {
    if (\count($form_state->getValue('field_enseignement')) < 1) return false;
    if (\count($form_state->getValue('field_enseignement')[0]) < 1) return false;
    return true;
  }
  protected static function niveauValidate($form_state)
  {
    if (\count($form_state->getValue('field_niveau')) < 1) return false;
    if (\count($form_state->getValue('field_niveau')[0]) < 1) return false;
    return true;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public static function handleSubmit(&$form, $form_state)
  {
    \Drupal::messenger()->deleteAll();
    $response = new AjaxResponse();
    if ($form_state->hasAnyErrors()) {
      $done = false;
      $errors = $form_state->getErrors();
      foreach ($errors as $item => $error) {
        $response->addCommand(new MessageCommand($error, '#bibmetadataformmessage', ['type' => 'error'], !$done));
        $done = true;
      }
      return $response;
    }
    $n = $form_state->getFormObject()->getEntity();
    if ($n->bundle() == 'notebook') {
      if ($n->field_teacheraccess->value) {
        $state = '1';
      } else {
        $state = '0';
      }
    } else if ($n->field_status_shared->value) {
      if ($n->field_status_web->value) $state = '2';
      else $state = '1';
    } else {
      $state = '0';
    }
    $response->addCommand(new CloseModalDialogCommand());
    $response->addCommand(new ChangeBibStateCommand($n->id(), $state));
    return $response;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public static function handleCommentSubmit(&$form, $form_state)
  {
    \Drupal::messenger()->deleteAll();
    $response = new AjaxResponse();
    if ($form_state->hasAnyErrors()) {
      $done = false;
      $errors = $form_state->getErrors();
      foreach ($errors as $item => $error) {
        $response->addCommand(new MessageCommand($error, '#bibmetadataformmessage', ['type' => 'error'], !$done));
        $done = true;
      }
      return $response;
    }

    $nid = $form_state->getFormObject()->getEntity()->getCommentedEntityId();
    $nbComm = \Drupal::entityQuery('comment')
    ->condition('entity_id', $nid)
    ->count()->execute();
    $response->addCommand(new CloseModalDialogCommand());
    $response->addCommand(new ChangeCommentCountCommand($nid, $nbComm));
    return $response;
  }
}


class ChangeBibStateCommand implements CommandInterface
{
  protected $nid;
  protected $state;
  public function __construct($nid, $state)
  {
    $this->state = $state;
    $this->nid = $nid;
  }

  public function render()
  {
    return [
      'command' => 'ChangeBibState',
      'nid' => $this->nid,
      'state' => $this->state,
    ];
  }
}

class ChangeCommentCountCommand implements CommandInterface
{
  protected $nid;
  protected $nbComm;
  public function __construct($nid, $nbComm)
  {
    $this->nbComm = $nbComm;
    $this->nid = $nid;
  }

  public function render()
  {
    return [
      'command' => 'ChangeNbComm',
      'nid' => $this->nid,
      'nbComm' => $this->nbComm,
    ];
  }
}
