<?php

namespace Drupal\capytale_bib\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining BibStar entity entities.
 *
 * @ingroup bibstar
 */
interface BibStarEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the BibStar entity name.
   *
   * @return string
   *   Name of the BibStar entity.
   */
  public function getName();

  /**
   * Sets the BibStar entity name.
   *
   * @param string $name
   *   The BibStar entity name.
   *
   * @return \Drupal\capytale_bib\Entity\BibStarEntityInterface
   *   The called BibStar entity entity.
   */
  public function setName($name);

  /**
   * Gets the BibStar entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the BibStar entity.
   */
  public function getCreatedTime();

  /**
   * Sets the BibStar entity creation timestamp.
   *
   * @param int $timestamp
   *   The BibStar entity creation timestamp.
   *
   * @return \Drupal\capytale_bib\Entity\BibStarEntityInterface
   *   The called BibStar entity entity.
   */
  public function setCreatedTime($timestamp);

}
