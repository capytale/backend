<?php

namespace Drupal\capytale_bib\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for BibStar entity entities.
 */
class BibStarEntityViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
