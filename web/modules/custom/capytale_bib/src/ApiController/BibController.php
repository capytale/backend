<?php

namespace Drupal\capytale_bib\ApiController;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

use Drupal\capytale_activity\url_token;

/**
 * Private Download Controller
 */
class BibController implements ContainerInjectionInterface
{
  const LIST_LIMIT = 100; // Nombre maximal d'activités renvoyées
  /** @var Drupal\Core\PageCache\ResponsePolicy\KillSwitch $ks */
  protected $ks;
  /** @var \Drupal\Core\Database\Connection $db */
  protected $db;

  public function __construct(
    $ks,
    $db
  ) {
    $this->ks = $ks;
    $this->db = $db;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $c)
  {
    return new static(
      $c->get('page_cache_kill_switch'),
      $c->get('database')
    );
  }


  /**
   * Répond à la route /c-bib/api/list/tk
   * TEMPORAIRE donne la liste des notebook python ordonnée par nbClones desc
   * 
   * @param \Symfony\Component\HttpFoundation\Request $req
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function list(Request $req)
  {
    $this->ks->trigger();
    $sql = 
"SELECT
 node.nid as nid,
 node.uuid as uuid
FROM node
JOIN node__field_activity_type nfat on nfat.entity_id = node.nid
JOIN node__field_status_shared nfss on nfss.entity_id = node.nid
JOIN node__field_nb_clone nfnc on nfnc.entity_id = node.nid
WHERE
 nfat.field_activity_type_value in ('notebook.python3', 'notebook.python3-old')
 AND nfss.field_status_shared_value
 AND NOT nfss.deleted
ORDER BY nfnc.field_nb_clone_value DESC
LIMIT " . self::LIST_LIMIT;
    $list = $this->db->query($sql)->fetchAll();
    $ret = [];

    foreach ($list as $item) {
      $ret[] = url_token::get_token($item->nid, $item->uuid);
    }

    return new JsonResponse($ret);
  }

}
