<?php

namespace Drupal\capytale_bib\ApiController;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\capytale_activity\Activity\ActivityManager;
use Drupal\Component\Utility\Xss;


/**
 * Controller for export json.
 */
class BibAjax extends ControllerBase
{

  /**
   * {@inheritdoc}
   */
  public function data()
  {

    $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
    if (in_array("teacher", $user->getRoles())) {
      $list = $this->getElements(
        'node__field_status_shared',
        'field_status_shared_value'
      );
    } else {
      $list = $this->getElements(
        'node__field_status_web',
        'field_status_web_value'
      );
    }

    $json_array = [];
    /** @var \Drupal\capytale_activity\Activity\ActivityManager $c_act_manager */
    $c_act_manager = \Drupal::service('capytale_activity.manager');
    foreach ($list as $id => $item) {
      /** @var \Drupal\node\NodeInterface $item */
      $dsc = strip_tags($item->descr, '<p><br>');
      $abs = strip_tags($item->abstract, '<p><br>');
      $abstract = (strlen($abs) > 1) ? $abs : $dsc;
      $abstract = (strlen($abstract) > 300) ? substr($abstract, 0, 300) . " [...]" : $abstract;
      $icon = $c_act_manager->buildIconUrl($item->at_type);
      $cloning_url = Url::fromRoute('capytale_ui.clone', ['nid' => $id]);
      $b_url = Url::fromRoute('capytale_auth.bibredirect', ['nid' => $id])->setOption('absolute', true)->toString();
      $token = \Drupal::csrfToken()->get($cloning_url->getInternalPath());
      $cloning_url->setOptions(['absolute' => TRUE, 'query' => ['token' => $token]]);
      $playerurl = Url::fromRoute('c-act.play', ['nid' => $id, 'mode' => 'view'])->setOption('absolute', true)->toString();

      $url = Url::fromRoute('star.setstar', ['nid' => $id]);
      $token = \Drupal::csrfToken()->get($url->getInternalPath());
      $url->setOptions(['absolute' => TRUE, 'query' => ['token' => $token]]);
      $url_star = $url->toString();
      $nom = Xss::filter($item->nom);
      $prenom = Xss::filter($item->prenom);


      //$tk = \Drupal\capytale_activity\url_token::get_token($id, $item->uuid);
      array_push($json_array, array(
        'type' => $item->at_type,
        'icon' => $icon,
        'player_url' => $playerurl,
        'title' => htmlspecialchars($item->title),
        'c' => mb_convert_encoding(htmlspecialchars($abstract), 'UTF-8', 'UTF-8'),
        'd' =>  $item->enseignement,
        'nb_star' => ($item->nbstar) ? $item->nbstar : 0,
        'url_star' => $url_star,
        'star_status' => ($item->mestared == 1) ? "fas" : "far",
        'nid' => $id,
        'b_url' => $b_url,
        'nb_clone' => ($item->nb_clone) ? $item->nb_clone : 0,
        'url_clone' => $cloning_url->toString(),
        'date' => array(
          'display' =>  date('d/m/Y', $item->changed),
          'timestamp' => $item->changed,
        ),
        'g' =>  "$prenom $nom",
      ));
    }
    return new JsonResponse(array_values($json_array));
  }

  private function getElements($table, $condition)
  {
    $uid = \Drupal::currentUser()->id();

    $database = \Drupal::database();
    $query = $database->select($table, 'fss');
    $query->join('node_field_data', 'nfd', 'nfd.nid = fss.entity_id');
    $query->fields('fss', ['entity_id'])
      ->fields('nfd', ['title', 'type', 'changed'])
      ->condition($condition, 1);

    $query->leftJoin('node', 'node', 'node.nid = nfd.nid');
    $query->addField('node', 'uuid', 'uuid');

    $query->leftJoin('user__field_nom', 'ufn', 'ufn.entity_id = nfd.uid');
    $query->addField('ufn', 'field_nom_value', 'nom');

    $query->leftJoin('user__field_prenom', 'ufp', 'ufp.entity_id = nfd.uid');
    $query->addField('ufp', 'field_prenom_value', 'prenom');
    $query->leftJoin('node__field_description', 'descr', 'descr.entity_id = fss.entity_id');
    $query->addField('descr', 'field_description_value', 'descr');
    $query->leftJoin('node__field_abstract', 'abstract', 'abstract.entity_id = fss.entity_id');
    $query->addField('abstract', 'field_abstract_value', 'abstract');
    $query->leftJoin('node__field_enseignement', 'ens', 'ens.entity_id = fss.entity_id');
    $query->addField('ens', 'field_enseignement_value', 'enseignement');
    $query->leftJoin('node__field_nb_clone', 'nbcl', 'nbcl.entity_id = fss.entity_id');
    $query->addField('nbcl', 'field_nb_clone_value', 'nb_clone');
    $query->leftJoin('user__field_etab', 'ufe', 'ufe.entity_id = nfd.uid');
    $query->addField('ufe', 'field_etab_target_id', 'etab_tgt');
    $query->leftJoin('node__field_uai', 'nfu', 'nfu.entity_id = ufe.field_etab_target_id');
    $query->addField('nfu', 'field_uai_value', 'uai');
    $query->leftJoin('node__field_activity_type', 'nfat', 'nfat.entity_id = fss.entity_id');
    $query->addField('nfat', 'field_activity_type_value', 'at_type');

    $query->addExpression('exists (select 1 from `bibstar_entity` as se3 where se3.uid = :uid and se3.nid = fss.entity_id )', 'mestared', [':uid' => $uid]);

    $sq = $database->select('bibstar_entity', 'se1');
    $sq->fields('se1', ['nid']);
    $sq->addExpression('count(1)', 'nb');
    $sq->groupBy('nid');

    $query->leftJoin($sq, 'star', 'star.nid = fss.entity_id');
    $query->addField('star', 'nb', 'nbstar');

    return $query->execute()->fetchAllAssoc("entity_id");
  }
}
