<?php

namespace Drupal\capytale_bib\ApiController;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

use Drupal\capytale_activity\url_token;

/**
 * Private Download Controller
 */
class DetailController implements ContainerInjectionInterface
{
  /** @var Drupal\Core\PageCache\ResponsePolicy\KillSwitch $ks */
  protected $ks;
  /** @var Drupal\Core\Entity\ContentEntityStorageInterface $etm */
  protected $etm;
  /** @var \Drupal\Core\Database\Connection $db */
  protected $db;

  public function __construct(
    $ks,
    $etm,
    $db
  ) {
    $this->ks = $ks;
    $this->etm = $etm;
    $this->db = $db;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $c)
  {
    return new static(
      $c->get('page_cache_kill_switch'),
      $c->get('entity_type.manager'),
      $c->get('database')
    );
  }


  /**
   * Répond à la route /c-act/api/ntk/{tk}/details
   * 
   * @param string $tk
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function getByToken($tk)
  {
    $this->ks->trigger();
    $node = url_token::getNode($this->etm->getStorage('node'), $tk);
    if (!$node) return new Response('', Response::HTTP_NOT_FOUND);
    $bundle = $node->bundle();
    if (($bundle !== 'activity') && ($bundle !== 'notebook_activity') && ($bundle != 'console_activity')) return new Response('', Response::HTTP_NOT_FOUND);
    return new JsonResponse($this->getDetails($node));
  }

  /**
   * Construit le tableau de détails
   * 
   * @param \Drupal\node\NodeInterface $node
   * @return array
   */
  private function getDetails($node)
  {
    /** @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $term_field */
    $term_field = $node->get('field_theme');
    $term_objects = $term_field->referencedEntities();
    $themes = [];
    foreach ($term_objects as $term_object) {
      $themes[] = $term_object->label();
    }

    /** @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $term_field */
    $term_field = $node->get('field_modules');
    $term_objects = $term_field->referencedEntities();
    $modules = [];
    foreach ($term_objects as $term_object) {
      $modules[] = $term_object->label();
    }

    /** @var \Drupal\Core\Field\FieldItemListInterface $term_field */
    $term_field = $node->get('field_enseignement');
    $ens = [];
    foreach ($term_field as $term_object) {
      $ens[] = $term_object->value;
    }

    /** @var \Drupal\Core\Field\FieldItemListInterface $term_field */
    $term_field = $node->get('field_niveau');
    $level = [];
    foreach ($term_field as $term_object) {
      $level[] = $term_object->value;
    }

    $abstract = \strip_tags($node->get('field_abstract')->value);
    $abstract = (\strlen($abstract)>1)? $abstract : \strip_tags($node->get('field_description')->value);

    $sql = "SELECT count(1) as cnt FROM bibstar_entity WHERE nid = :nid";
    $stars = $this->db->query($sql, [':nid' => $node->id()])->fetchField();
    $stars = empty($stars) ? 0 : (int)$stars;

    $clones = $node->get('field_nb_clone')->value;
    $clones = empty($clones) ? 0 : (int)$clones;

    return [
      'title' => $node->get('title')->value,
      'type' => $node->get('field_activity_type')->value,
      'enseignements' => $ens,
      'themes' => $themes,
      'modules' => $modules,
      'niveaux' => $level,
      'abstract' => $abstract,
      'clones' => $clones,
      'stars' => $stars,
    ];
  }
}
