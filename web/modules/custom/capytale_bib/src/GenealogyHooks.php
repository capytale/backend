<?php

namespace Drupal\capytale_bib;

class GenealogyHooks
{
    /**
     * Ajoute la filiation lorsqu'une activité est clonée
     *
     * @param \Drupal\node\NodeInterface $node
     */
    public static function filiationAdd($entity, $duplicate)
    {
        $db = \Drupal::database();
        $sql = "INSERT INTO {genealogy} (nid, parentNid, unixTimeStamp) VALUES (:nid, :parentNid, :unixTimeStamp)";
        $q = $db->query($sql, [':nid' => $duplicate->id(), ':parentNid' => $entity->id(), ':unixTimeStamp' => time()]);
    }
    /**
     * Supprime la filiation de clonage éventuelle lorsqu'une activité est supprimée
     *
     * @param \Drupal\node\NodeInterface $node
     */
    public static function filiationDelete($node)
    {
        $db = \Drupal::database();
        $sql = "DELETE FROM {genealogy} WHERE nid = :nid";
        $q = $db->query($sql, [':nid' => $node->id()]);
    }

}
