<?php

namespace Drupal\capytale_bib\Plugin\views\filter;

use Drupal\views\Plugin\views\filter\InOperator;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\ViewExecutable;
use Drupal\views\Views;

/**
 * Filter by Type.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("the_type_views_filter")
 */
// class TheTypeViewsFilter extends StringFilter
class TheTypeViewsFilter extends InOperator
{

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL)
  {
    parent::init($view, $display, $options);
    $this->valueTitle = 'Types';
    $this->definition['options callback'] = array($this, 'generateOptions');
  }

  /**
   * {@inheritdoc}
   */
  public function operators()
  {
    return [
      '=' => [
        'title' => $this->t('Is equal to'),
        'short' => $this->t('='),
        'method' => 'opFilterType',
        'values' => 1,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function query()
  {
    // Override the query
    // So that no filtering takes place if the user doesn't select any options.
    if (!empty($this->value)) {
      parent::query();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validate()
  {
    // Skip validation
    // If no options have been chosen so we can use it as a non-filter.
    if (!empty($this->value)) {
      parent::validate();
    }
  }

  /**
   * Helper function that generates the options.
   * @return array
   */
  public function generateOptions()
  {
    // TODO : à réécrire avec une SQL query
    return [
      'Pixel' => 'Pixel',
      'codabloc' => 'Codabloc',
      'html' => 'HTML',
      'console_activity' => 'Console',
      'notebook_activity' => 'Notebook',
      'sql' => 'SQL',
      'ocaml' => 'OCaml',
    ];
  }

  /**
   * {@inheritdoc}
   */
  // public function query()
  protected function opFilterType()
  {
    $this->ensureMyTable();

    /** @var \Drupal\views\Plugin\views\query\Sql $query */
    $query = $this->query;
    $operator = $this->operator;
    $value = $this->value[0];

    // // Join Kernel table
    // $nfk = [
    //   'table' => 'node__field_kernel',
    //   'field' => 'entity_id',
    //   'left_table' => 'node_field_data',
    //   'left_field' => 'nid',
    //   'operator' => '=',
    // ];
    // $join = Views::pluginManager('join')->createInstance('standard', $nfk);
    // $query->addRelationship('node__field_kernel', $join, 'node_field_data');

    // Join activity_type table
    $nfat = [
      'table' => 'node__field_activity_type',
      'field' => 'entity_id',
      'left_table' => 'node_field_data',
      'left_field' => 'nid',
      'operator' => '=',
    ];
    $join = Views::pluginManager('join')->createInstance('standard', $nfat);
    $query->addRelationship('node__field_activity_type', $join, 'node_field_data');

    // Build Condition
    $query->addWhereExpression(
      'AND',
      // "node__field_kernel.field_kernel_value $operator :type 
      //   OR type $operator :type 
      //   OR node__field_activity_type.field_activity_type_value $operator :type",
      "node__field_activity_type.field_activity_type_value $operator :type",
        [':type' => $value]
    );
  }
}
