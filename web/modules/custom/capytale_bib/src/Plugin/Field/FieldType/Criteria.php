<?php

namespace Drupal\capytale_bib\Plugin\Field\FieldType;

use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;

/**
 * Field type "criteria".
 *
 * @FieldType(
 *   id = "criteria",
 *   label = @Translation("criteria"),
 *   description = @Translation("criteria field."),
 *   category = @Translation("Bibliotheque"),
 *   default_widget = "criteria_default",
 *   default_formatter = "criteria_default",
 * )
 */
class Criteria extends FieldItemBase implements FieldItemInterface
{

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition)
  {

    $module_handler = \Drupal::service('module_handler');
    $path = $module_handler->getModule('capytale_bib')->getPath();
    include_once DRUPAL_ROOT . "/" . $path . "/capytale_bib.inc";
    $criteria = capytale_bib_get_criteria();

    $output = array();

    // Make a column for every criterion.
    foreach ($criteria as $criterion => $description) {
      $output['columns'][$criterion] = array(
        'description' => $description['title'],
        'type' => 'int',
        'unsigned' => FALSE,
        'not null' => TRUE,
      );
    }

    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition)
  {

    $module_handler = \Drupal::service('module_handler');
    $path = $module_handler->getModule('capytale_bib')->getPath();
    include_once DRUPAL_ROOT . "/" . $path . "/capytale_bib.inc";
    $criteria = capytale_bib_get_criteria();

    foreach ($criteria as $criterion => $description) {
      $properties[$criterion] = DataDefinition::create('integer')
        ->setLabel($description['title']);
    }

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty()
  {

    $item = $this->getValue();

    $module_handler = \Drupal::service('module_handler');
    $path = $module_handler->getModule('capytale_bib')->getPath();
    include_once DRUPAL_ROOT . "/" . $path . "/capytale_bib.inc";
    $criteria = capytale_bib_get_criteria();

    foreach ($criteria as $criterion => $description) {
      if (isset($item[$criterion]) && $item[$criterion] != 0) {
        return FALSE;
      }
    }

    return TRUE;
  }

  /**
   * Returns the array of criteria.
   */
  public function getCriteriaNames()
  {

    $module_handler = \Drupal::service('module_handler');
    $path = $module_handler->getModule('capytale_bib')->getPath();
    include_once DRUPAL_ROOT . "/" . $path . "/capytale_bib.inc";
    $criteria = capytale_bib_get_criteria();

    $output = array();

    foreach ($criteria as $criterion => $description) {

      if ($this->$criterion) {
        $output[$criterion] = $description['title'];
      }
    }

    return $output;
  }

}
