<?php

namespace Drupal\capytale_bib\Plugin\Field\FieldWidget;

use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Field\WidgetInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Field widget "criteria_default".
 *
 * @FieldWidget(
 *   id = "criteria_default",
 *   label = @Translation("Criteria default"),
 *   field_types = {
 *     "criteria",
 *   }
 * )
 */
class CriteriaDefaultWidget extends WidgetBase implements WidgetInterface
{

    /**
     * {@inheritdoc}
     */
    public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state)
    {

        $module_handler = \Drupal::service('module_handler');
        $path = $module_handler->getModule('capytale_bib')->getPath();
        include_once DRUPAL_ROOT . "/" . $path . "/capytale_bib.inc";
        $criteria = capytale_bib_get_criteria();

        // $item is where the current saved values are stored.
        $item = &$items[$delta];

        // $element is already populated with #title, #description, #delta,
        // #required, #field_parents, etc.
        $element += array(
            '#type' => 'fieldset',
        );

        $element['criteria'] = array(
            '#type' => 'fieldset',
            '#process' => array(__CLASS__ . '::processCriteriaFieldset'),
        );
        $element['#attached']['library'][] = 'capytale_bib/criteria';

        foreach ($criteria as $criterion => $description) {
            $debug = isset($item->$criterion) ? " (valeur enregistrée: {$item->$criterion})" : "";
            $element['criteria'][$criterion] = array(
                '#type' => 'select',
                '#options' => $description['values'],
                '#default_value' => isset($item->$criterion) ? $item->$criterion : $description['default_value'],
                '#attributes' => array('class' => array('hidden')),
            );
        }

        return $element;
    }

    /**
     * Form widget process callback.
     */
    public static function processCriteriaFieldset($element, FormStateInterface $form_state, array $form)
    {

        $elem_key = array_pop($element['#parents']);

        return $element;
    }
}
