<?php

namespace Drupal\capytale_bib\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Field formatter "criteria_default".
 *
 * @FieldFormatter(
 *   id = "criteria_default",
 *   label = @Translation("Criteria default"),
 *   field_types = {
 *     "criteria",
 *   }
 * )
 */
class CriteriaDefaultFormatter extends FormatterBase
{

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings()
  {
    return array(
      'criteria' => 'csv',
    ) + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state)
  {

    $output['criteria'] = array(
      '#title' => t('Criteria'),
      '#type' => 'select',
      '#options' => array(
        'csv' => t('Comma separated values'),
        'list' => t('Unordered list'),
        'icons' => t('Icônes FontaAwesome'),
      ),
      '#default_value' => $this->getSetting('criteria'),
    );

    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary()
  {

    $summary = array();

    // Determine ingredients summary.
    $criteria_summary = FALSE;
    switch ($this->getSetting('criteria')) {

      case 'csv':
        $criteria_summary = 'Comma separated values';
        break;

      case 'list':
        $criteria_summary = 'Unordered list';
        break;

      case 'icons':
        $criteria_summary = 'Unordered list';
        break;
    }

    // Display criteria summary.
    if ($criteria_summary) {
      $summary[] = t('Criteria display: @format', array(
        '@format' => t($criteria_summary),
      ));
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode)
  {

    $output = array();

    // Iterate over every field item and build a renderable array
    // (I call them rarray for short) for each item.
    foreach ($items as $delta => $item) {

      $build = array();

      // Render Criteria.
      // Here as well, we follow the same format as above.
      // We build a container, within which, we render the property
      // label (Criteria) and the actual values for the criteria
      // as per configuration.
      $criteria_format = $this->getSetting('criteria');
      $build['criteria'] = array(
        '#type' => 'container',
        '#attributes' => array(
          'class' => array('criteria'),
        ),
        'value' => array(
          '#type' => 'container',
          '#attributes' => array(
            'class' => array('field__item'),
          ),
          // The buildCriteria method takes responsibility of generating
          // markup for criteria as per the format set in field
          // configuration. We use $this->getSetting('criteria') above to
          // read the configuration.
          'text' => $this->buildCriteria($criteria_format, $item),
        ),
      );

      $output[$delta] = $build;
    }

    return $output;
  }

  /**
   * Builds a renderable array or string of criteria.
   *
   * @param string $format
   *   The format in which the criteria are to be displayed.
   *
   * @return array
   *   A renderable array of criteria.
   */
  public function buildCriteria($format, FieldItemInterface $item)
  {
    // Instead of having a switch-case we build a dynamic method name
    // as per a pre-determined format. In this way, if we will to add
    // a new format in the future, all we will have to do is create a
    // new method named "buildCriteriaFormatName()".
    $callback = 'buildCriteria' . ucfirst($format);
    return $this->$callback($item);
  }

  /**
   * Format criteria as Icons.
   */
  public function buildCriteriaIcons(FieldItemInterface $item)
  {
    $module_handler = \Drupal::service('module_handler');
    $path = $module_handler->getModule('capytale_bib')->getPath();
    include_once DRUPAL_ROOT . "/" . $path . "/capytale_bib.inc";
    $criteria = capytale_bib_get_criteria();


    $titles[] = "<div id=\"titles\">";
    $values[] = "<div id=\"ratings\">";

    $tab = [];
    $sum = 0;
    foreach ($criteria as $criterion => $description) {
      $titles[] = "<div class=\"small-rating\">{$criteria[$criterion]['icon']}</div>";
      $values[] = "<div class=\"small-rating\">{$criteria[$criterion]['icons'][$item->$criterion]}</div>";
      $sum += $item->$criterion;
    };

    if ($sum < 0) {
      $icon =
        "<i class=\"fas fa-ban\" title=\"{$sum} pts - À dépublier de la bibliothèque\"></i>";
    } else if ($sum > 10) {
      $icon =
        "<i class=\"fas fa-medal\" title=\"{$sum} pts - Mérite le Label de Grande Qualité\"></i>";
    } else {
      $icon =
        "<i class=\"far fa-check-circle\" title=\"{$sum} pts - A sa place dans la bibliothèque\"></i>";
    }


    $titles[] = "</div>";
    $values[] = "<span class=\"criteria\">$icon</span>";
    $values[] = "</div>";

    return array(
      '#markup' => implode(' ', $titles) . implode(' ', $values),
    );
  }

  /**
   * Format criteria as CSV.
   */
  public function buildCriteriaCsv(FieldItemInterface $item)
  {
    $criteria = $item->getCriteriaNames();
    #kint($item);

    $tab = [];
    foreach ($criteria as $criterion => $description) {
      $tab[] = isset($item->$criterion) ? "{$criteria[$criterion]}: {$item->$criterion}" : "";
    };

    return array(
      '#markup' => implode(', ', $tab),
    );
  }

  /**
   * Format criteria as an unordered list.
   */
  public function buildCriteriaList(FieldItemInterface $item)
  {
    // "item_list" is a very handy render type.
    return array(
      '#theme' => 'item_list',
      '#items' => $item->getCriteriaNames(),
    );
  }
}
