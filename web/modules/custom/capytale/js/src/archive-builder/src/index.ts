import "@capytale/activity.js/activity/activityBunch/all";
import Archiver from "@capytale/activity.js/activity/archiver/all";

import http from "@capytale/activity.js/backend/capytale/http";
import load from "@capytale/activity.js/backend/capytale/activityBunch";
import type ActivityBunch from "@capytale/activity.js/activity/activityBunch/base";
import typeLoad from "@capytale/activity.js/backend/capytale/activityType";
import type { ActivityType } from "@capytale/activity.js/backend/capytale/activityType";
import type { ArchiveFileData } from "@capytale/activity.js/activity/archiver/base";

import iter from "@capytale/activity.js/activity/iterator";
import * as zip from "@zip.js/zip.js";

(function (api: any) {
    // Initialise le contexte
    let context: any = {};
    type saInfo = { nid: string, nom: string, prenom: string, classe: string, workflow: string };
    if (api.mode == 'my') {
        // Récupère les types d'activités
        context.typesPromise = typeLoad.getList().then(types => {
            const typesMap: { [key: string]: ActivityType } = {};
            for (const t of types) {
                typesMap[t.id] = t;
            }
            return typesMap;
        });
        const activityList: { [nid: string]: any } = {};
        for (const a of api.activityList as { nid: string | number }[]) {
            activityList[a.nid] = a;
        }
        api.activityList = activityList;
        api.isArchivable = function (nid: string | number): boolean {
            const type = activityList[nid]?.type;
            if (type == null) return false;
            return Archiver.isArchivable(type);
        }
    } else if (api.mode == 'assignment') {
        load.doCacheActivityNode = true;
        context.activityBunchPromise = load(api.actid);
        context.saListPromise = http.getJsonAsync<saInfo[]>({ name: 'sa.list', id: api.actid, filter: 'all' })
            .then(list => {
                const saList: { [nid: string]: saInfo } = {};
                for (const sa of list) {
                    saList[sa.nid] = sa;
                }
                return saList;
            });
    }

    // Gestion de l'unicité des noms de dossiers
    let usedFolder: string[] = [];
    function checkFolder(n: string, ext?: string): string {
        let to = ext ? n + '.' + ext : n;
        if (usedFolder.includes(to)) {
            const name = n + '(';
            ext = ext ? ').' + ext : ')';
            let i = 0;
            do { to = name + ++i + ext } while (usedFolder.includes(to));
        }
        usedFolder.push(to);
        return to;
    }

    // Création du zip dans le cas 'my'
    const createMyZip = async function (ids: number[], cb: (msg: string) => void): Promise<Blob | null> {
        const typesMap = await context.typesPromise;
        if (Array.isArray(ids) && ids.length > 0) {
            const len = ids.length;
            const zipWriter = new zip.ZipWriter(new zip.BlobWriter('application/zip'));
            usedFolder = [];
            let i = 0;
            const d = new Date();
            let report = 'Archive créée le ' + d.toLocaleString() + "\r\n";
            for (const ap of iter(load, ids)) {
                let msg = ++i + "/" + len;
                report += "\r\n" + msg;
                try {
                    cb(msg);
                    const a = await ap;
                    const friendlyType = typesMap[a.activityType] == null ? a.activityType : typesMap[a.activityType].name;
                    const desc = " (" + friendlyType + ") " + a.title.value;
                    report += desc;
                    cb(msg = msg + desc);
                    const archiver = Archiver.getArchiverFor(a);
                    if (archiver != null) {
                        const folder = checkFolder(archiver.mainFileName)
                        for (const f of archiver.files) {
                            const path = folder + '/' + f.path;
                            const data = await f.dataPromise;
                            await zipWriter.add(path, getReader(data));
                        }
                    }
                } catch (error) {
                    cb(msg + ' Erreur !');
                    report += ' (Erreur)';
                }
            }
            const fileName = checkFolder('rapport', 'txt');
            await zipWriter.add(fileName, new zip.TextReader(report));
            return zipWriter.close() as Promise<Blob>;
        }
        return null;
    };

    const wfMap: { [key: string]: string } = {
        'current': 'en cours',
        'finished': 'rendu',
        'corrected': 'corrigé',
        'unknown': '???',
    };

    const getReader = function (data: ArchiveFileData): zip.Reader<any> {
        switch (data.type) {
            case 'url': return new zip.HttpReader(data.data);
            case 'text': return new zip.TextReader(data.data);
            case 'uint8array': return new zip.Uint8ArrayReader(data.data);
        }
        return new zip.TextReader('Erreur');
    }

    // Création du zip dans le cas 'assignment'
    const createAssigmentZip = async function (ids: number[], cb: (msg: string) => void): Promise<Blob | null> {
        if (Array.isArray(ids) && ids.length > 0) {
            const len = ids.length;
            const activityBunch: ActivityBunch = await context.activityBunchPromise;
            if (!Archiver.isArchivable(activityBunch.activityType)) return null;
            const archiver = Archiver.getArchiverFor(activityBunch);
            if (archiver == null) return null;
            const isFolderModel = archiver.assignmentModel == 'folder';
            const zipWriter = new zip.ZipWriter(new zip.BlobWriter('application/zip'));
            const d = new Date();
            let report = 'Archive créée le ' + d.toLocaleString() + "\r\n";
            usedFolder = [];
            if (!isFolderModel) {
                try {
                    for (const f of archiver.activityFiles) {
                        const path = f.path;
                        const data = await f.dataPromise;
                        await zipWriter.add(path, getReader(data));
                        usedFolder.push(path);
                    }
                } catch (error) {
                    cb('Erreur !');
                }
            }
            let i = 0;
            const saList: { [nid: string]: saInfo } = await context.saListPromise;
            const dParser = new DOMParser();
            for (const ap of iter(load, ids)) {
                let msg = ++i + "/" + len;
                report += "\r\n" + msg;
                try {
                    cb(msg);
                    const a = await ap;
                    const student = saList[a.mainNode.nid];
                    const wf = a.assignmentNode?.workflow ?? 'unknown';
                    let state = wfMap[wf];
                    if (wf === 'corrected') {
                        let grade = a.assignmentNode?.grading?.value;
                        if (grade != null) {
                            const doc = dParser.parseFromString(grade, 'text/html');
                            state += ' (' + doc.documentElement.textContent + ')';
                        }
                    } 
                    const desc = " : " + student.nom + " " + student.prenom + " (" + student.classe + ") : " + state;
                    report += desc;
                    cb(msg = msg + desc);
                    const archiver = Archiver.getArchiverFor(a);
                    if (archiver != null) {
                        let fileName = student.nom + "_" + student.prenom;
                        fileName = Archiver.slugify(fileName);
                        if (isFolderModel) {
                            const folder = checkFolder(fileName)
                            for (const f of archiver.files) {
                                const path = folder + '/' + f.path;
                                const data = await f.dataPromise;
                                await zipWriter.add(path, getReader(data));
                            }
                        } else {
                            const file = archiver.mainFile;
                            fileName = checkFolder(fileName, file.ext);
                            const data = await file.dataPromise;
                            await zipWriter.add(fileName, getReader(data));
                        }
                    }
                } catch (error) {
                    cb(msg + ' Erreur !');
                    report += ' (Erreur)';
                }
            }
            const fileName = checkFolder('rapport', 'txt');
            await zipWriter.add(fileName, new zip.TextReader(report));
            return zipWriter.close() as Promise<Blob>;
        }
        return null;
    };

    if (api.mode == 'my') api.createZip = createMyZip;
    else if (api.mode == 'assignment') api.createZip = createAssigmentZip;
    else api.createZip = function () { return null; };

    api.download = function (blob: Blob) {
        const a = document.createElement('a');
        a.className = "hidden";
        a.href = URL.createObjectURL(blob);
        a.download = "archive.zip";
        a.click();
        a.remove();
    };
})((window as any).archiveBuilder || ((window as any).archiveBuilder = {}));
