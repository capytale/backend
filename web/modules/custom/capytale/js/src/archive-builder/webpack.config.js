const path = require('path');

module.exports = {
  mode: 'production',
  entry: './build/index.js',
  output: {
    filename: 'archive-builder.js',
    path: path.resolve(__dirname, '../..'),
  },
};