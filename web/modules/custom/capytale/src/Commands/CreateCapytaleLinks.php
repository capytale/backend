<?php

namespace Drupal\capytale\Commands;

use Drush\Commands\DrushCommands;
use Drupal\menu_link_content\Entity\MenuLinkContent;


/**
 * Drush command file.
 */
class CreateCapytaleLinks extends DrushCommands
{

  /**
   * Add Capytale essential links.
   *
   * @command capytale:create-capytale-links
   * @aliases cccl
   */
  public function CreateLinks()
  {
    MenuLinkContent::create([
      'title' => 'Mes activités',
      'link' => ['uri' => 'internal:/my'],
      'menu_name' => 'main',
      'weight' => -110,
    ])->save();
    MenuLinkContent::create([
      'title' => 'La Bibliothèque',
      'link' => ['uri' => 'internal:/bibliotheque'],
      'menu_name' => 'main',
      'weight' => -100,
    ])->save();
  }
}
