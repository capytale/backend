<?php

namespace Drupal\capytale\Commands;

use Drush\Commands\DrushCommands;


/**
 * Drush command file.
 */
class RemoveOldRoles extends DrushCommands
{

  /**
   * Replace a given role with a user.data entry for *ALL* users.
   *
   * @command capytale:remove-old-roles
   * @param $role the role to remove for *ALL* users
   * @param $userParam the user.data parameter replacing the removed role
   * @aliases cror
   */
  public function RemoveRole($role, $userParam = FALSE)
  {
    if ($userParam == FALSE) {
      $userParam = $role;
    }
    $query = \Drupal::entityQuery('user');
    $num_rows = $query->count()->execute();
    $query = \Drupal::entityQuery('user');
    $uids = $query->execute();
    $n = 0;
    $userData = \Drupal::service('user.data');
    // Boucle sur l'ensemble des utilisateurs
    foreach ($uids as $uid) {
      $n++;
      $user = \Drupal\user\Entity\User::load($uid);
      $roles = $user->getRoles();

      if ($n % 500 == 0) {
        $this->output()->writeln("$n/$num_rows");
      }
      if (in_array($role, $roles)) {
        // supprime le rôle et ajoute une entrée user.data
        $this->output()->writeln("{$n}/{$num_rows} - User n°{$uid} : Role {$role} supprimé -> Paramètre {$userParam} ajouté");
        $user->removeRole($role);
        $user->save();
        $userData->set('capytale', $uid, $userParam, True);
      }
    }
  }
}
