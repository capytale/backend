<?php

namespace Drupal\capytale\Commands;

use Drush\Commands\DrushCommands;

// Exemple : Convert
// console_activity (node bundle) 
// to
// activity (node bundle) 
//
// execute this command :
// $ drush capytale:bundle-transition node console_activity activity


/**
 * Drush command file.
 */
class BundleTransition extends DrushCommands
{

  /**
   * Transition from $from_bundle to $to_bundle for all entities of a given $entity_type
   *
   * @command capytale:bundle-transition
   * @param $entity_type the entity_type of the bundles
   * @param $from_bundle the bundle to be changed
   * @param $to_bundle   the destination bundle
   * @aliases cbt
   */
  public function ChangeType($entity_type, $from_bundle, $to_bundle)
  {

    $storage = \Drupal::service('entity_type.manager')->getStorage($entity_type);

    // Get the names of the base tables.
    $base_table_names = [];
    $base_table_names[] = $storage->getBaseTable();
    $base_table_names[] = $storage->getDataTable();

    $fields = array_filter(
      \Drupal::service('entity_field.manager')->getFieldDefinitions($entity_type, $from_bundle),
      function ($fieldDefinition) {
        return $fieldDefinition instanceof \Drupal\field\FieldConfigInterface;
      }
    );

    // Get the names of the field and field_revision tables.
    $table_mapping = $storage->getTableMapping();
    $field_table_names = [];
    foreach ($fields as $field) {
      $field_table = $table_mapping->getFieldTableName($field->getName());
      $field_table_names[] = $field_table;

      $field_storage_definition = $field->getFieldStorageDefinition();
      $field_revision_table = $table_mapping
        ->getDedicatedRevisionTableName($field_storage_definition);
      $field_table_names[] = $field_revision_table;
    }

    // Get the nids to update.
    // $query = \Drupal::service('entity.query')->get('node');
    $query = \Drupal::entityQuery('node');
    $query->condition('type', $from_bundle);
    $nids = $query->execute();

    // Update Field tables 'bundle' column.
    foreach ($field_table_names as $table_name) {
      $query = \Drupal\Core\Database\Database::getConnection('default')
        ->update($table_name)
        ->fields(['bundle' => $to_bundle])
        ->condition('bundle', $from_bundle);
      $this->output()->writeln($query->__toString());
      // $query->execute();
    }
    // Update Base tables 'type' column.
    foreach ($base_table_names as $table_name) {
      $query = \Drupal\Core\Database\Database::getConnection('default')
        ->update($table_name)
        ->fields(['type' => $to_bundle])
        ->condition('type', $from_bundle);
      $this->output()->writeln($query->__toString());
      // $query->execute();
    }
  }
}
