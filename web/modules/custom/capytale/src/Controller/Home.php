<?php

namespace Drupal\capytale\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Site\Settings;
// use Drupal\capytale\Controller\CapytaleCrudController;

/**
 * Provides route responses for the Example module.
 */
class Home extends ControllerBase
{
  const HOME_SETTING = 'home_page_node_id';

  /**
   * Return the home page.
   * - if anonymous : connexion
   * - if role teacher : page d'accueil
   * - if role !teacher : page my
   */
  public function home()
  {
    $user = $this->currentUser();

    if ( !$user->isAuthenticated()) {
      // utilisateur non authentifié
      return $this->redirect('capytale_auth.idp_list');
    } else {
      $user_roles = $this->currentUser->getRoles();
      if (in_array('teacher', $user_roles)) {
        $nid = Settings::get(self::HOME_SETTING);
        $node_exists = false;
        if (is_int($nid)) {
          $values = \Drupal::entityQuery('node')->condition('nid', $nid)->execute();
          $node_exists = !empty($values);
        }
        if ($node_exists) {
          return $this->redirect('entity.node.canonical', ['node' => $nid]);
        } else {
          return $this->redirect('capytale.default_home_page');
        }
      } else {
        // role !prof :
        return $this->redirect('capytale_ui.my');
      }
    }
  }
  public function default_home_page()
  {
    $output = array();

    $output['default'] = array(
      '#markup' => $this->t('Page d\'accueil par défaut.<br/>
      Renseignez dans settings.php l\'id de la page à afficher ici.'),
    );
    return $output;
  }
}
