<?php

namespace Drupal\capytale\Controller;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ArchiveController implements ContainerInjectionInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container)
  {
    return new static();
  }

  public function __construct()
  {
  }

  public function buildArchive() {
    return [
        '#theme' => 'capytale_archive_builder',
    ];
  
  }

}