<?php

namespace Drupal\capytale\Exception;

use Symfony\Component\HttpFoundation\Response as R;

/**
 * Class CapytaleApiException.
 * 
 * Erreurs renvoyées par les API Capytale.
 */
class CapytaleApiException extends \Exception
{
  private $httpStatus;
  /**
   * Access denied.
   */
  const E_ACCESS_DENIED = 1;
  /**
   * Bad request.
   */
  const E_BAD_REQUEST = 2;
  /**
   * Not found.
   */
  const E_NOT_FOUND = 3;

  const MSG_UNKNOWN = 'Erreur inconnue';

  /**
   * CapytaleApiException constructor.
   * 
   * @param int $code Code d'erreur.
   * @param string $msg Message d'erreur optionnel.
   */
  public function __construct($code, $msg = "")
  {
    list($status, $msg) = $this->_format($code, $msg);
    $this->httpStatus = $status;
    parent::__construct($msg, $code);
  }

  /**
   * Formatte le message d'erreur.
   * Cette fonction peut être surchargée pour personnaliser le format.
   * 
   * @param int $code Code d'erreur.
   * @param string $msg Message d'erreur optionnel.
   * 
   * @return array|null Tableau contenant le code HTTP et le message d'erreur. Renvoyer null pour utiliser le format par défaut.
   */
  protected function format($code, $msg)
  {
    return null;
  } 

  private function _format($code, $msg)
  {
    $res = $this->format($code, $msg);
    if (!empty($res)) return $res;
    switch ($code) {
      case self::E_ACCESS_DENIED:
        $msg = empty($msg) ? 'Accès refusé' : $msg;
        return [R::HTTP_FORBIDDEN, $msg];
      case self::E_BAD_REQUEST:
        $msg = empty($msg) ? 'Requête invalide' : $msg;
        return [R::HTTP_BAD_REQUEST, $msg];
      case self::E_NOT_FOUND:
        $msg = empty($msg) ? 'Ressource non trouvée' : $msg;
        return [R::HTTP_NOT_FOUND, $msg];
    }
    if (empty($msg)) $msg = self::MSG_UNKNOWN;
    return [R::HTTP_BAD_REQUEST, $msg];
  }

  public function getHttpStatus()
  {
    return $this->httpStatus;
  }
}
