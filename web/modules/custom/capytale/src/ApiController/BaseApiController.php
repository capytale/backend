<?php

namespace Drupal\capytale\ApiController;

use Symfony\Component\HttpFoundation\Response as R;
use Symfony\Component\HttpFoundation\JsonResponse as JR;

use Drupal\capytale\Exception\CapytaleApiException as CAE;

abstract class BaseApiController
{
    const NO_STORE_HEADER = [
        'Cache-Control' => 'no-store'
    ];

    const SAFE_METHODS = ['GET', 'HEAD', 'OPTIONS'];

    /**
     * Retourne l'utilisateur Capytale courant.
     * 
     * @return \Drupal\capytale_auth\Users\CurrentUserInterface
     */
    protected function _currentCapytaleUser()
    {
        return \Drupal::service('c_auth.current_user');
    }

    /**
     * Teste si l'utilisateur est authentifié.
     * 
     * @return void
     * @throws \Drupal\capytale\Exception\CapytaleApiException
     */
    protected function _checkIsAuthenticated()
    {
        if (!$this->_currentCapytaleUser()->isAuthenticated()) throw new CAE(CAE::E_ACCESS_DENIED);
    }

    /**
     * Teste si le jeton CSRF est valide.
     * 
     * @return void
     * @throws \Drupal\capytale\Exception\CapytaleApiException
     */
    protected function _checkCsrf($request)
    {
        /** @var \Drupal\Core\Access\CsrfRequestHeaderAccessCheck $csfrChecker */
        $csfrChecker = \Drupal::service('access_check.header.csrf');
        /** @var \Drupal\Core\Access\AccessResultForbidden $access */
        $access = $csfrChecker->access($request, \Drupal::currentUser());
        if (!$access->isAllowed()) throw new CAE(CAE::E_ACCESS_DENIED, $access->getReason());
    }

    /**
     * Teste si la chaîne passée est un entier.
     * 
     * @return void
     * @throws \Drupal\capytale\Exception\CapytaleApiException
     */
    protected function _checkIsInt($s){
        if ((!\ctype_digit($s)) || ($s[0] === '0')) throw new CAE(CAE::E_NOT_FOUND);
    }

    /**
     * Triggers page cache kill switch.
     * 
     * @return void
     */ 
    protected function _killPageCache()
    {
        \Drupal::service('page_cache_kill_switch')->trigger();
    }    

    /**
     * Génère une réponse.
     * 
     * @param mixed $data Données à renvoyer. Si null ou omis, renvoie une réponse vide avec statut 204.
     */
    protected function _response($data = null)
    {
        if ($data === null) return new R('', R::HTTP_NO_CONTENT, self::NO_STORE_HEADER);
        return new JR($data, R::HTTP_OK, self::NO_STORE_HEADER);
    }

    /**
     * Génère une réponse d'erreur.
     * 
     * @param \Exception $e
     */
    protected function _errorResponse(\Exception $e)
    {
        if ($e instanceof CAE) return new JR(['status' => 'error', 'message' => $e->getMessage()], $e->getHttpStatus());
        // Todo: renvoyer le message d'erreur selon la configuration debug.
        return new JR(['status' => 'error', 'message' => CAE::MSG_UNKNOWN], R::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * Traite une requête.
     * 
     * Attention, les méthodes GET, HEAD et OPTIONS sont traitées sans vérification d'authentification ni de jeton CSRF.
     * 
     * @param array $handlers Tableau associatif contenant les méthodes de traitement des requêtes.
     *                        Les clés sont les méthodes HTTP, les valeurs sont les noms des méthodes de traitement.
     * @param \Symfony\Component\HttpFoundation\Request $r
     * @param mixed ...$args Arguments à passer à la méthode de traitement.
     * 
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function _handleRequest($handlers, $r, ...$args)
    {
        try {
            $method = $r->getMethod();
            if (!\array_key_exists($method, $handlers)) throw new CAE(CAE::E_BAD_REQUEST, 'Méthode non implémentée');
            if (!\in_array($method, self::SAFE_METHODS)) {
                $this->_checkIsAuthenticated();
                $this->_checkCsrf($r);
            }
            $handler = $handlers[$method];
            return $this->$handler($r, ...$args);
        } catch (\Exception $e) {
            return $this->_errorResponse($e);
        }
    }
}
