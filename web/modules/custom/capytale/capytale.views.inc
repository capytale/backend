<?php

/**
 * @file
 * The module file.
 */

/**
 * Implements hook_views_data().
 */
function capytale_views_data() {
  $data['views']['table']['group'] = t('Custom Global');
  $data['views']['table']['join'] = [
    // #global is a special flag which allows a table to appear all the time.
    '#global' => [],
  ];

  $data['views']['current_uid_views_field'] = [
    'title' => t('Current user ID'),
    'help' => t('The ID of current logged ID.'),
    'field' => [
      'id' => 'current_uid_views_field',
    ],
  ];
  $data['views']['notebook_html_views_field'] = [
    'title' => t('Version Html du notebook'),
    'help' => t('URL du fichier .ipynb.html ou ERROR si non présent'),
    'field' => [
      'id' => 'notebook_html_views_field',
    ],
  ];
  $data['views']['notebook_html_file_views_field'] = [
    'title' => t('Fichier Html du notebook'),
    'help' => t('URL du fichier .ipynb.html ou ERROR si non présent'),
    'field' => [
      'id' => 'notebook_html_file_views_field',
    ],
  ];
  $data['views']['nb_vues_views_field'] = [
    'title' => t('Nombre de vues élève pour l\'activité'),
    'help' => t('Renvoie le nb de vues ou le nb de copies'),
    'field' => [
      'id' => 'nb_vues_views_field',
    ],
  ];
  $data['views']['clone_link_views_field'] = [
    'title' => t('Lien de clonage de l\'activité'),
    'help' => t('Renvoie le lien de clonage'),
    'field' => [
      'id' => 'clone_link_views_field',
    ],
  ];
  $data['views']['the_kernel_views_field'] = [
    'title' => t('le kernel de l\'activité'),
    'help' => t('Renvoie le kernel de l\'activité'),
    'field' => [
      'id' => 'the_kernel_views_field',
    ],
  ];
  // $data['views']['nb_star_views_field'] = [
    // 'title' => t('Nombre d etoiles'),
    // 'help' => t('Renvoie le nb stars'),
    // 'field' => [
      // 'id' => 'nb_star_views_field',
    // ],
  // ];
  $data['views']['last_access_views_field'] = [
    'title' => t('Dernier accès élève pour l\'activité'),
    'help' => t('Renvoie la date du dernier accès à une copie'),
    'field' => [
      'id' => 'last_access_views_field',
    ],
  ];
  return $data;
}
