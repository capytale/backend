(function ($, Drupal, drupalSettings, archiveBuilder) {
  $(document).ready(function ($) {
    if (localStorage.getItem('tagBarStatus') === null || localStorage.getItem('tagBarStatus') == 'closed') {
      document.getElementById("colA").style.display = "none";
      document.getElementById("colB").classList.add("colB-deploy");
      document.getElementById("colB").classList.remove("colB-reduce");
    } else {
      document.getElementById("colA").style.display = "block";
      document.getElementById("colB").classList.remove("colB-deploy");
      document.getElementById("colB").classList.add("colB-reduce");
    };

    // Archivage ZIP
    archiveBuilder.mode = "my";
    const span = document.getElementById('col-download').getElementsByTagName('button')[0];

    archiveBuilder.doOne = function (id) {
      archiveBuilder.doMultiple([id]);
    }

    archiveBuilder.doMultiple = function (ids) {
      archiveBuilder.toArchive = ids;
      const ajaxSettings = {
        url: 'build-archive',
        dialogType: 'modal',
        dialog: { width: 400, title: 'Création de l\'archive zip.' }
      }
      const ajaxObject = Drupal.ajax(ajaxSettings);
      ajaxObject.execute();
    }

    span.addEventListener('click', () => {
      const form = document.getElementById('form');
      const data = new FormData(form);
      archiveBuilder.doMultiple(data.getAll('nids[]'));
    });

    $("#my-content").hide();

    // DataTable
    fetch(Drupal.url('my_json_data'))
      .then(r => r.json())
      .then(activityList => {// début
        $("#my-content").show();
        $("#my-content-spinner").hide();

        archiveBuilder.activityList = activityList;
        const rootUrl = window.location.protocol + '//' + window.location.host + Drupal.url('');
        var table = $('#mytable').DataTable({
          data: activityList,
          "lengthMenu": [
            [10, 50, -1],
            ['10 lignes', '50 lignes', 'Tout'],
          ],
          "pageLength": 50,
          "fnDrawCallback": function (oSettings) {
            $('[data-bs-toggle="tooltip"]').tooltip();
            Drupal.attachBehaviors();
            (function ($) {
              $(document).ready(function () {
                $('#select-all').click(function (event) {
                  if (this.checked) {
                    // Iterate each checkbox
                    $(':checkbox').each(function () {
                      this.checked = true;
                    });
                  } else {
                    $(':checkbox').each(function () {
                      this.checked = false;
                    });
                  }
                });

                $(':checkbox').change(function () {
                  // do stuff here. It will fire on any checkbox change
                  var x = $(".z:checked").length;
                  var elsno = document.getElementsByClassName("noCheck");
                  var elsone = document.getElementsByClassName("oneCheck");
                  var els = document.getElementsByClassName("check");

                  Array.prototype.forEach.call(elsno, function (el) {
                    if (x == 0) {
                      el.style.display = "inline";
                    } else {
                      el.style.display = "none";
                    }
                  });
                  Array.prototype.forEach.call(elsone, function (el) {
                    if (x == 1) {
                      el.style.display = "inline";
                    } else {
                      el.style.display = "none";
                    }
                  });
                  Array.prototype.forEach.call(els, function (el) {
                    if (x > 0) {
                      el.style.display = "inline";
                    } else {
                      el.style.display = "none";
                    }
                  });
                });
              });
            })(jQuery);
          },
          "deferRender": true,
          "order": [[4, "desc"]],
          "rowId": "row_id",
          "columns": [
            { // ------------------- Checkboxes --------------
              // ---------------------------------------------
              "data": null,
              "orderable": false,
              render: function (data, type, row) {
                return '<input type="checkbox" name="nids[]" class="z" value="' + data.nid + '">';
              },
            },
            { // ------------------- ICONE -----------------
              // -------------------------------------------
              "data": null,
              "width": "32px",
              render: function (data, type, row) {
                return '<span class="hidden">' + data.type + '</span>'
                  + '<span class="hidden">Iam-' + data.whoami + '</span>'
                  + '<img src="' + data.icon + '" width="40px"'
                  + 'data-bs-toggle="tooltip" data-bs-placement="top" title="Activité '
                  + data.type
                  + '">'
              },
            },
            { // ------------------- TITRE -----------------
              // -------------------------------------------
              "data": null,
              "width": "85em",
              render: function (data, type, row) {
                var ret = '<div class="parent">'
                  + '<div class="myDIV childA">'
                  + '<a href="' + data.player + '" data-capytale="title">' + data.title + '</a>'
                  + '</div>'

                if (data.whoami != "ap") {
                  ret = ret + '&nbsp;'
                    + '<div class="childB hideinfo">'
                    + '<a href="node/' + data.nid + '/edit/" '
                    + 'data-bs-toggle="tooltip" data-bs-placement="right" title="Modifier les paramètres">'
                    + '<i class="fas fa-cog fa-lg"></i>'
                    + '</a></div>'
                }
                ret = ret + '</div >'
                return ret
              },
            },
            { // ------------------- EVAL / APPR ---------------
              // -----------------------------------------------
              "data": null,
              "width": "8em",
              render: function (data, type, row) {
                return data.evaluation + '<hr/>' + data.appreciation;
              },
            },
            { // ------------------- DERNIER ACCÈS ---------------
              // -------------------------------------------------
              "data": null,
              render: function (data, type, row) {
                var d = new Date(data.last_access * 1000);
                var dformat = [
                  ("0" + d.getDate()).slice(-2),
                  ("0" + (d.getMonth() + 1)).slice(-2),
                  d.getFullYear().toString().slice(-2),
                ].join('/') + " à " + [
                  ("0" + d.getHours()).slice(-2),
                  ("0" + d.getMinutes()).slice(-2),
                ].join(':');
                return '<span class="hidden">' + data.last_access + '</span>'
                  + '<span id="aa' + data.nid + '" data-url="url-' + data.nid + '" data-toshake="aa' + data.nid + '" onclick="copyToMyClipboard(\'aa' + data.nid + '\', 2)">' + dformat + '</span>';
              },
            },
            { // ------------------- PARTAGE -----------------
              // ---------------------------------------------
              "data": null,
              "className": "dt-center",
              render: function (data, type, row) {
                if (data.mode.includes('O')) {
                  classe = "green";
                } else {
                  classe = "red";
                }
                var tt = ' data-bs-toggle="tooltip" data-bs-placement="right" data-bs-html="true" class="' + classe + '"';
                var ttl = ' data-bs-toggle="tooltip" data-bs-placement="left" data-bs-html="true" class="' + classe + '"';
                var beg = (data.tr_beg ? (new Date(data.tr_beg + "Z")).toLocaleString() : false);
                var end = (data.tr_end ? (new Date(data.tr_end + "Z")).toLocaleString() : false);
                if (beg) beg = beg.replace(' ', ' à ');
                if (end) end = end.replace(' ', ' à ');
                var modes = {
                  // N:None C:Complete L: Lock --- O:Ouvert X:Fermé
                  N_O: '<span' + tt + 'title="Libre d\'accès"> <i class="fas fa-lock-open"></i> </span>',
                  N_X: '<span' + tt + 'title="Accès interdit"> <i class="fas fa-lock red"></i> </span>',
                  C_O: '<span' + tt + 'title="À réaliser avant le ' + end + '"> <i class="fas fa-clock orange"></i></i></span>',
                  C_X: '<span' + tt + 'title="Date limite du ' + end + ' dépassée"> <i class="fas fa-clock red"></i></i></span>',
                  L_O: '<span' + tt + 'title="À réaliser avant le ' + end + '"> <i class="fas fa-clock orange"></i></i></span>',
                  L_X: '<span' + tt + 'title="Date limite du ' + end + ' dépassée"> <i class="fas fa-clock red"></i></i></span>',
                };

                var info = "";
                if (data.mode.includes('O')) {
                  if (data.workflow == 100) {
                    info = '<span' + ttl + 'title="En cours"><i class="fas fa-pen blue"></i></span>' + modes[data.mode]
                  } else if (data.workflow == 200) {
                    info = '<span' + ttl + 'title="Rendu"><i class="far fa-envelope orange"></i></span>';
                  } else if (data.workflow == 300) {
                    info = '<span' + ttl + 'title="Corrigé"><i class="fas fa-tasks green"></i></span>';
                  }
                }
                if (data.mode.includes('X')) {
                  if (data.workflow == 100) {
                    info = info + modes[data.mode];
                  } else if (data.workflow == 200) {
                    info = '<span' + ttl + 'title="Rendu"><i class="far fa-envelope orange"></i></span>';
                  } else if (data.workflow == 300) {
                    info = '<span' + ttl + 'title="Corrigé"><i class="fas fa-tasks green"></i></span>';
                  }
                }

                if (data.whoami == "cr") return "Perso";
                if (data.whoami == "ap") return info + "Apprenant de " + data.boss;
                if (!data.code) return "Associé par " + data.boss;
                if (data.whoami == "as") return "Associé par " + data.boss;
              },
            },
            { // ------------------- TAGS --------------------
              // ---------------------------------------------
              "data": null,
              render: function (data, type, row) {

                if (Object.keys(data.tags).length == 0) return ("");
                var str = "";
                var tids = data.tags.tids.split(',')
                var names = data.tags.names.split(',')
                var colors = data.tags.colors.split(',')
                tids.forEach(function callback(tid, idx) {
                  name = names[idx];
                  color = colors[idx];
                  if (name.match("Corbeille")) {
                    var icon = "fas fa-trash-alt tagicon";
                    var color = "";
                  } else {
                    var icon = "fas fa-tag";
                    var color = color;
                  }
                  str = str + '<span class="hidden">tag_' + tid + '</span>'
                    + '<div class="tag"><span class="tagicon"'
                    + 'style="color:' + color + '"><i class="' + icon + '"></i></span>'
                    + '<button type="submit" class="deltag naked_button" name="action-del-etiquette"'
                    + 'value="' + tid + '-' + data.nid + '" data-bs-toggle="tooltip" data-bs-placement="left" title="Supprimer">'
                    + '<i class="far fa-times-circle small red"></i>'
                    + '</button>'
                    + '&nbsp;<span class="naked_button">' + name + '</span>'
                    + '</div>'
                });
                return (str);
              },
            },
            { // ------------------- MORE --------------------
              // ---------------------------------------------
              "data": null,
              "orderable": false,
              render: function (data, type, row) {
                var str = ''
                  + '<a id="bloc' + data.nid + '" class="nav-link dropdown-toggle gray" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">'
                  + '<i class="far fa-plus-square"></i></a>'
                  + '<ul class="dropdown-menu" aria-labelledby="navbarDropdown">'
                if (data.whoami != "ap") {
                  str = str
                    + '<li><button class="action-indiv" type="submit" name="action-param" value="' + data.nid + '">'
                    + '          <i class="fas fa-cog"></i> Paramètres'
                    + '</button></li>'
                    + '<li><button class="action-indiv" type="submit" name="action-clone" value="' + data.nid + '">'
                    + '         <i class="far fa-clone"></i> Cloner'
                    + '</button></li>'
                  // + '<li><a class="dropdown-item" href="#">Copier l\'URL de publication privée (lecture seule)</a></li>'
                }
                str = str
                  // + '<div class="btn-group dropstart">'
                  + '<li><button class="action-indiv" type="button" onclick="window.archiveBuilder.doOne(\'' + data.nid + '\')">'
                  + '         <i class="fas fa-download"></i> Télécharger'
                  + '</button></li>'
                  + '<div class="dropdown-divider"></div>'
                  + '<li><button class="action-indiv action-indiv-danger" type="submit" name="action-delete" value="' + data.nid + '">'
                  + '         <i class="fas fa-window-close red"></i> Supprimer'
                  + '</button></li>'
                  + '</ul>'
                // + '</div>'
                return (str);
              },
            },
          ],
          "language": {
            search: "Rechercher ",
            "info": "lignes _START_ à _END_ sur _TOTAL_",
            "lengthMenu": "Afficher _MENU_ lignes",
            "loadingRecords": '<i class="fa fa-spinner fa-spin" style="font-size:48px;"></i>',
            "emptyTable": 'Aucune activité, entrez un code ci-dessus ou cliquez sur <i class="fas fa-plus-circle"></i> pour commencer.',
            "paginate": {
              "first": "Première page",
              "last": "Dernière page",
              "next": "Page suivante",
              "previous": "Page précédente"
            },
          },
        });

        $(".whereIam_search").on('click', function () {
          var wasDown = $(this).hasClass("whereIam_down");
          $(".whereIam_search").each(function () {
            $(this).removeClass("whereIam_down");
          });

          if (wasDown) {
            table.column(1)
              .search("")
              .draw();
          }
          else {
            $(this).addClass("whereIam_down");
            table.column(1)
              .search($(this).val())
              .draw();
          }
        });



        // TAGS
        // ---------------------------------------------
        $(".tag_search").on('click', function () {
          // Highlight tag term in list 
          $(".tag_search").each(function () {
            $(this).removeClass("roundedSquare");
          });
          $(this).toggleClass("roundedSquare");
          // Tag Search
          table.column(6)
            .search($(this).data("tag"), true, true, true)
            .draw();
        });
        // hide Corbeille tagged by default
        table.column(6).search('^((?!Corbeille).)*$', true, true, true).draw();
      });// fin
  });
})(jQuery, Drupal, drupalSettings, window.archiveBuilder || (window.archiveBuilder = {}));

function copyToMyClipboard(id, factor) {
  var el = document.getElementById(id);
  copyTextToClipboard(el.dataset.url);

  var elem = document.getElementById(el.dataset.toshake);
  elem.animate([
    {
      transform: 'scale(1,1)'
    },
    {
      transform: "scale(" + factor + ", " + factor + ")"
    },
    {
      transform: 'scale(1,1)'
    },
    {
      transform: "scale(" + factor + ", " + factor + ")"
    },
    {
      transform: 'scale(1,1)'
    }],
    {
      duration: 400
    });
}
function fallbackCopyTextToClipboard(text) {
  var textArea = document.createElement("textarea");
  textArea.value = text;

  // Avoid scrolling to bottom
  textArea.style.top = "0";
  textArea.style.left = "0";
  textArea.style.position = "fixed";

  document.body.appendChild(textArea);
  textArea.focus();
  textArea.select();

  try {
    var successful = document.execCommand('copy');
    var msg = successful ? 'successful' : 'unsuccessful';
  } catch (err) {
    console.error('Fallback: Oops, unable to copy', err);
  }

  document.body.removeChild(textArea);
}
function copyTextToClipboard(text) {
  if (!navigator.clipboard) {
    fallbackCopyTextToClipboard(text);
    return;
  }
  navigator.clipboard.writeText(text).catch(function (err) {
    console.error('Async: Could not copy text: ', err);
  });
}


function toggleNav() {
  var x = document.getElementById("colA");
  if (x.style.display === "none") {
    // Ouvre le panneau
    document.getElementById("colB").classList.remove("colB-deploy");
    document.getElementById("colB").classList.add("colB-reduce");
    setTimeout(() => { x.style.display = "block"; }, 500);
    localStorage.setItem('tagBarStatus', 'open');
    document.getElementById('hamburger').classList.remove("pulse");
    document.getElementById('hamburgernotif').display = "none";
  } else {
    // Ferme le panneau
    x.style.display = "none";
    document.getElementById("colB").classList.add("colB-deploy");
    document.getElementById("colB").classList.remove("colB-reduce");
    localStorage.setItem('tagBarStatus', 'closed');
    var tagtitle = localStorage.getItem('tagTitleSearch');
    if (tagtitle != null && tagtitle != "displaywithnofilter") {
      document.getElementById('hamburger').classList.add("pulse");
      document.getElementById('hamburgernotif').display = "inline";
    }
  }
}
function toggleEtiquettes() {
  var x = document.getElementById("etiqueter");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
