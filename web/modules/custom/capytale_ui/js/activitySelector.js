//https://www.w3schools.com/bootstrap/bootstrap_filters.asp
//https://www.w3schools.com/bootstrap/tryit.asp?filename=trybs_filters_anything&stacked=h

const capytale = {
  //https://www.w3schools.com/xml/tryit.asp?filename=tryajax_first
  star_activity(act) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {
        var e = document.getElementById(act + "_star");
        if (this.responseText == 0) {
          e.classList.add("far");
          e.classList.remove("fas");
          e.classList.remove("yellow");
        } else {
          e.classList.remove("far");
          e.classList.add("fas");
          e.classList.add("yellow");
        }
      }
    };
    xhttp.open("GET", "star_activity?name=" + act, true);
    xhttp.send();
  },
  init() {
    (function ($) {
      $(document).ready(function () {
        $("#myInput").on("keyup", function () {
          var value = $(this).val().toLowerCase();
          $(".col").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
          });
        });
      });
    })(jQuery);

    (function ($) {
      $(document).ready(function () {
        $("#star").on("click", function () {
          $(".col").filter(function () {
            $(this).toggle($(this).hasClass("star"));
          });
        });
      });
    })(jQuery);

    (function ($) {
      $(document).ready(function () {
        $("#all").on("click", function () {
          $(".col").filter(function () {
            $(this).toggle($(this).hasClass("col"));
          });
        });
      });
    })(jQuery);

    (function ($) {
      $(document).ready(function () {
        $("#college").on("click", function () {
          $(".col").filter(function () {
            $(this).toggle($(this).hasClass("college"));
          });
        });
      });
    })(jQuery);

    (function ($) {
      $(document).ready(function () {
        $("#lycee").on("click", function () {
          $(".col").filter(function () {
            $(this).toggle($(this).hasClass("lycee"));
          });
        });
      });
    })(jQuery);

    (function ($) {
      $(document).ready(function () {
        $("#prepa").on("click", function () {
          $(".col").filter(function () {
            $(this).toggle($(this).hasClass("prepa"));
          });
        });
      });
    })(jQuery);

    (function ($) {
      $(document).ready(function () {
        $("#vittascience").on("click", function () {
          $(".col").filter(function () {
            $(this).toggle($(this).hasClass("vittascience"));
          });
        });
      });
    })(jQuery);
  },
};
