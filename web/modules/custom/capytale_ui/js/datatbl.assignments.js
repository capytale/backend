(($, Drupal, drupalSettings, archiveBuilder) => {
  $(document).ready(function($) {
    let cbs;
    const cbChange = function() {
      const checked = cbs.filter(':checked');
      $('#select-all').prop('checked', checked.length === cbs.length);
      if (checked.length > 0) {
        $('.ifCheck').show();
      } else {
        $('.ifCheck').hide();
      }
    };

    $('#select-all').click(function(event) {
      if (this.checked) {
        // Iterate each checkbox
        $('#mytable tbody :checkbox').each(function() {
          this.checked = true;
        });
      } else {
        $('#mytable tbody :checkbox').each(function() {
          this.checked = false;
        });
      }
      cbChange();
    });

    function richContentToPlainText(src) {
      if (src.startsWith("<")) {
        src = sanitize(src)
        src = src
          .replace(/<(?:br|\/div|\/p)>/g, "\n")
          .replace(/<.*?>/g, "");
        const textArea = document.createElement("textarea");
        textArea.innerHTML = src;
        src = textArea.value;
      }
      return src;
    }
    $('.ap_ch').each(function(){
      $(this).val(richContentToPlainText($(this).val()));
      autosize($(this));
    });
    $('.ev_ch').each(function(){
      $(this).val(richContentToPlainText($(this).val()));
      autosize($(this));
    });

    let exportBtn = {
      text: '<i class="fas fa-download"></i> Télécharger les copies',
      className: 'ifCheck',
      attr: {
        style: "pointer-events: auto;",
      },
    };
    if (drupalSettings.c_act.exportable) {
      archiveBuilder.actid = drupalSettings.c_act.nid;
      archiveBuilder.mode = "assignment";
      const exporterUrl = Drupal.url('build-archive');
      exportBtn.action = () => {
        const form = document.getElementById('form');
        const data = new FormData(form);
        archiveBuilder.toArchive = data.getAll('nids[]');
        const ajaxSettings = {
          url: exporterUrl,
          dialogType: 'modal',
          dialog: { width: 400, title: 'Création de l\'archive zip.' }
        }
        const ajaxObject = Drupal.ajax(ajaxSettings);
        ajaxObject.execute();
      };
      exportBtn.titleAttr = "Télécharger les copies sélectionnées";
    } else {
      exportBtn.titleAttr = "Cette activité n'est pas encore exportable.";
      exportBtn.enabled = false;
      exportBtn.attr = {
        style: "pointer-events: default;",
      };
    }

    const buttons = ['csvHtml5', exportBtn];

    if (drupalSettings.c_act.detailedEvaluation) {
      let detailedBtn = {
        text: '<i class="fas fa-list"></i> Voir les détails',
        attr: {
          style: "pointer-events: auto;",
        },
      };
      detailedBtn.action = () => {
        const form = document.getElementById('form');
        const data = new FormData(form);
        const nid = drupalSettings.c_act.nid;
        const detailedUrl = Drupal.url('detailed_evaluation/' + nid);
        const ajaxSettings = {
          url: detailedUrl,
          dialogType: 'modal',
          dialog: { width: 800, height: 700, title: 'Résultats détaillés' }
        }
        const ajaxObject = Drupal.ajax(ajaxSettings);
        ajaxObject.execute();
      };
      buttons.push(detailedBtn);
    }

    var table = $('#mytable').DataTable({
      "paging": false,
      "stateSave": true,
      "order": [[3, "asc"]],
      "columns": [
        { "className": "dt-center", "orderable": false },
        null,
        { "className": "dt-center", "orderable": false },
        null,
        null,
        { "className": "dt-center" },
        null,
        null,
      ],
      "dom": 'Bfrtip',
      "buttons": buttons,
      "language": {
        search: "Rechercher ",
        "infoFiltered": "(filtrés depuis un total de _MAX_ éléments)",
        "info": "lignes _START_ à _END_ sur _TOTAL_",
        "lengthMenu": "Afficher _MENU_ lignes",
        "paginate": {
          "first": "Première page",
          "last": "Dernière page",
          "next": "Page suivante",
          "previous": "Page précédente"
        },
      },
      fnDrawCallback: function(oSettings) {
        cbs = $('#mytable tbody :checkbox');
        cbs.off('change');
        cbs.on('change', cbChange);
        cbChange();
      }
    });

    $(".tag_search").on('click', function() {
      // Tag highlight
      $(".tag_search").each(function() {
        $(this).removeClass("roundedSquare");
      });
      $(this).toggleClass("roundedSquare");
      // Tag Search
      table.column(2).search($(this).data("tag"), true, true, true).draw();
      window.location.href = window.location.pathname + "?" + $.param({ 'search': $(this).data("tag") })
    });

    const params = new Proxy(new URLSearchParams(window.location.search), {
      get: (searchParams, prop) => searchParams.get(prop),
    });
    let value = (params.search ? params.search : 'tag-visible'); // "some_value"
    table.column(2).search(value, true, true, true).draw();

    if (params.search == "tag-hidden") {
      var d = document.getElementById("tagHidden");
      d.classList.add("roundedSquare");
    } else if (params.search == "tag") {
      var d = document.getElementById("tagAll");
      d.classList.add("roundedSquare");
    } else {
      var d = document.getElementById("tagVisible");
      d.classList.add("roundedSquare");
    }

    let csrfPrm = null;
    function getCsrf() {
      if (csrfPrm != null) return csrfPrm;
      return csrfPrm = fetch(Drupal.url('session/token')).then((r) => { return r.text() });
    }

    $(".ap_ch")
      .on('blur', async function() {
        $(this).addClass("pulse")
        const nid = $(this).data("nid");
        const csrf = await getCsrf();
        body = JSON.stringify({ nid, task: "ap_ch", value: $(this).val() })
        fetch(Drupal.url("assignments/edit_ap_ev"), { method: "POST", headers: { ['X-CSRF-Token']: csrf }, body })
          .then(response => {
            if (!response.ok) {
              console.log("Error: ", response.status);
              let element = $(this);
              let addIt = function() { element.addClass("redborder"); };
              let delIt = function() { element.removeClass("pulse"); };
              setTimeout(function() { setTimeout(addIt, 3000); setTimeout(delIt, 3000); }, 0);
              throw new Error('Fetch error', response);
            }
            return response;
          })
          .then(data => {
            $(this).removeClass("pulse");
            let element = $(this);
            let addIt = function() { element.addClass("okidok"); };
            let delIt = function() { element.removeClass("okidok"); };
            setTimeout(function() { addIt(); setTimeout(delIt, 500); }, 0);
          })
          .catch(error => console.log(error));
      })
      .on('focus', function() {
        $(this).tooltip('show');
      });

    $(".ev_ch")
      .on('blur', async function() {
        $(this).addClass("pulse")
        const nid = $(this).data("nid");
        const csrf = await getCsrf();
        body = JSON.stringify({ nid, task: "ev_ch", value: $(this).val() })
        fetch(Drupal.url("assignments/edit_ap_ev"), { method: "POST", headers: { ['X-CSRF-Token']: csrf }, body })
          .then(response => {
            if (!response.ok) {
              console.log("Error: ", response.status);
              let element = $(this);
              let addIt = function() { element.addClass("redborder"); };
              let delIt = function() { element.removeClass("pulse"); };
              setTimeout(function() { setTimeout(addIt, 3000); setTimeout(delIt, 3000); }, 0);
              throw new Error('Fetch error', response);
            }
            return response;
          })
          .then(data => {
            $(this).removeClass("pulse");
            let element = $(this);
            let addIt = function() { element.addClass("okidok"); };
            let delIt = function() { element.removeClass("okidok"); };
            setTimeout(function() { addIt(); setTimeout(delIt, 500); }, 0);
          })
          .catch(error => console.log(error));
      })
      .on('focus', function() {
        $(this).tooltip('show');
      });;

  });
})(jQuery, Drupal, drupalSettings, window.archiveBuilder || (window.archiveBuilder = {}));
