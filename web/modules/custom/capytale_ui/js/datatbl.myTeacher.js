(function ($, Drupal, drupalSettings, archiveBuilder) {
  $(document).ready(function ($) {
    if (localStorage.getItem('tagBarStatus') === null || localStorage.getItem('tagBarStatus') == 'closed') {
      document.getElementById("colA").style.display = "none";
      document.getElementById("colB").classList.add("colB-deploy");
      document.getElementById("colB").classList.remove("colB-reduce");
    } else {
      document.getElementById("colA").style.display = "block";
      document.getElementById("colB").classList.remove("colB-deploy");
      document.getElementById("colB").classList.add("colB-reduce");
    };

    // Archivage ZIP
    archiveBuilder.mode = "my";
    const span = document.getElementById('col-download').getElementsByTagName('button')[0];

    archiveBuilder.doOne = function (id) {
      archiveBuilder.doMultiple([id]);
    }

    archiveBuilder.doMultiple = function (ids) {
      archiveBuilder.toArchive = ids;
      const ajaxSettings = {
        url: 'build-archive',
        dialogType: 'modal',
        dialog: { width: 400, title: 'Création de l\'archive zip.' }
      }
      const ajaxObject = Drupal.ajax(ajaxSettings);
      ajaxObject.execute();
    }

    span.addEventListener('click', () => {
      const form = document.getElementById('form');
      const data = new FormData(form);
      archiveBuilder.doMultiple(data.getAll('nids[]'));
    });

    $("#my-content").hide();

    // DataTable
    fetch(Drupal.url('my_json_data'))
      .then(r => r.json())
      .then(activityList => {// début
        $("#my-content").show();
        $("#my-content-spinner").hide();

        archiveBuilder.activityList = activityList;
        const rootUrl = window.location.protocol + '//' + window.location.host + Drupal.url('');
        var table = $('#mytable').DataTable({
          data: activityList,
          "lengthMenu": [
            [10, 50, -1],
            ['10 lignes', '50 lignes', 'Tout'],
          ],
          "pageLength": 50,
          "fnDrawCallback": function (oSettings) {
            $('[data-bs-toggle="tooltip"]').tooltip();
            Drupal.attachBehaviors();
            (function ($) {
              $(document).ready(function () {
                $('#select-all').click(function (event) {
                  if (this.checked) {
                    // Iterate each checkbox
                    $(':checkbox').each(function () {
                      this.checked = true;
                    });
                  } else {
                    $(':checkbox').each(function () {
                      this.checked = false;
                    });
                  }
                });

                $(':checkbox').change(function () {
                  // do stuff here. It will fire on any checkbox change
                  var x = $(".z:checked").length;
                  var elsno = document.getElementsByClassName("noCheck");
                  var elsone = document.getElementsByClassName("oneCheck");
                  var els = document.getElementsByClassName("check");

                  Array.prototype.forEach.call(elsno, function (el) {
                    if (x == 0) {
                      el.style.display = "inline";
                    } else {
                      el.style.display = "none";
                    }
                  });
                  Array.prototype.forEach.call(elsone, function (el) {
                    if (x == 1) {
                      el.style.display = "inline";
                    } else {
                      el.style.display = "none";
                    }
                  });
                  Array.prototype.forEach.call(els, function (el) {
                    if (x > 0) {
                      el.style.display = "inline";
                    } else {
                      el.style.display = "none";
                    }
                  });
                });
              });
            })(jQuery);
          },
          "deferRender": true,
          "order": [[4, "desc"]],
          "rowId": "row_id",
          "columns": [
            { // ------------------- Checkboxes --------------
              // ---------------------------------------------
              "data": null,
              "orderable": false,
              render: function (data, type, row) {
                return '<input type="checkbox" name="nids[]" class="z" value="' + data.nid + '">';
              },
            },
            { // ------------------- ICONE -----------------
              // -------------------------------------------
              "data": null,
              "width": "32px",
              render: function (data, type, row) {
                return '<span class="hidden">' + data.type + '</span>'
                  + '<span class="hidden">Iam-' + data.whoami + '</span>'
                  + '<img src="' + data.icon + '" width="40px"'
                  + 'data-bs-toggle="tooltip" data-bs-placement="top" title="Activité '
                  + data.type
                  + '">'
              },
            },
            { // ------------------- TITRE -----------------
              // -------------------------------------------
              "data": null,
              "width": "85em",
              render: function (data, type, row) {
                var ret = '<div class="parent">'
                  + '<div class="myDIV childA">'
                  + '<a href="' + data.player + '" data-capytale="title">' + data.title + '</a>'
                  + '</div>'

                if (data.whoami != "ap") {
                  ret = ret + '&nbsp;'
                    + '<div class="childB hideinfo">'
                    + '<a href="node/' + data.nid + '/edit/" '
                    + 'data-bs-toggle="tooltip" data-bs-placement="right" title="Modifier les paramètres">'
                    + '<i class="fas fa-cog fa-lg"></i>'
                    + '</a></div>'
                }
                ret = ret + '</div >'
                return ret
              },
            },
            { // ------------------- NB VUES ---------------
              // -------------------------------------------
              "data": null,
              "width": "8em",
              render: function (data, type, row) {
                if (data.whoami == "ap") return "";
                let nb_visibles = data.views_total - data.views_hidden;
                let text_v = nb_visibles + (nb_visibles > 1 ? " vues" : " vue");
                let text_c = "Voir les travaux :<br/>"
                  + nb_visibles + (nb_visibles > 1 ? " visibles" : " visible")
                  + "<br/>"
                  + data.views_hidden + (data.views_hidden > 1 ? " masqués" : " masqué");
                if (data.views_hidden == 0) {
                  text_c = "Voir les travaux";
                } else {
                  text_v = text_v.replace(" ", '<sup class="gray">+</sup> ');
                }
                let yena = '<a href="assignments/' + data.nid + '" class="views_total" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-html="true" title="' + text_c + '" >' + text_v + "</a>";
                let yenapas = "0 vue";
                let sort = ("0000" + nb_visibles * 100 + data.views_hidden + "0").slice(-8, -1);
                sort = '<span class="hidden">' + sort + '</span>';
                return sort + (data.views_total > 0 ? yena : yenapas)
              },
            },
            { // ------------------- DERNIER ACCÈS ---------------
              // -------------------------------------------------
              "data": null,
              render: function (data, type, row) {
                var d = new Date(data.last_access * 1000);
                var dformat = [
                  ("0" + d.getDate()).slice(-2),
                  ("0" + (d.getMonth() + 1)).slice(-2),
                  d.getFullYear().toString().slice(-2),
                ].join('/') + " à " + [
                  ("0" + d.getHours()).slice(-2),
                  ("0" + d.getMinutes()).slice(-2),
                ].join(':');
                return '<span class="hidden">' + data.last_access + '</span><span>' + dformat + '</span>';
              },
            },
            { // ------------------- PARTAGE -----------------
              // ---------------------------------------------
              "data": null,
              "className": "dt-center",
              render: function (data, type, row) {
                if (data.mode.includes('O')) {
                  classe = "share-on-old";
                  classe2 = "dropdown-capytale-on";
                } else {
                  classe = "share-off-old";
                  classe2 = "dropdown-capytale-off";
                }
                tt = ' data-bs-toggle="tooltip" data-bs-placement="right" data-bs-html="true" ';
                var beg = (data.tr_beg ? (new Date(data.tr_beg + "Z")).toLocaleString() : false);
                var end = (data.tr_end ? (new Date(data.tr_end + "Z")).toLocaleString() : false);
                if (beg) beg = beg.replace(' ', ' à ');
                if (end) end = end.replace(' ', ' à ');
                var modes = {
                  // N:None C:Complete L: Lock --- O:Ouvert X:Fermé
                  N_O: '',
                  N_X: '<span' + tt + 'title="Non partagé avec la classe."> <i class="fas fa-lock red"></i> </span>',
                  C_O: '<span' + tt + 'title="Libre pour les élèves sur la période<br/>du ' + beg + '<br/>au ' + end + '<br/>et en lecture seule en dehors."> <span class="fa-layers fa-fw"> <i class="fas fa-clock"></i> <i class="fas fa-envelope" data-fa-transform="shrink-4 down-4.2 right-10"></i> </span> </span>',
                  C_X: '<span' + tt + 'title="Libre pour les élèves sur la période<br/>du ' + beg + '<br/>au ' + end + '<br/>et en lecture seule en dehors."> <span class="fa-layers fa-fw"> <i class="fas fa-clock orange"></i> <i class="fas fa-envelope orange" data-fa-transform="shrink-4 down-4.2 right-10"></i> </span> </span>',
                  L_O: '<span' + tt + 'title="Libre pour les élèves sur la période<br/>du ' + beg + '<br/>au ' + end + '<br/>et non accessible en dehors."> <span class="fa-layers fa-fw"> <i class="fas fa-clock"></i> <i class="fas fa-lock" data-fa-transform="shrink-4 down-4.2 right-10"></i> </span> </span>',
                  L_X: '<span' + tt + 'title="Libre pour les élèves sur la période<br/>du ' + beg + '<br/>au ' + end + '<br/>et non accessible en dehors."> <span class="fa-layers fa-fw"> <i class="fas fa-clock red"></i> <i class="fas fa-lock red" data-fa-transform="shrink-4 down-4.2 right-10"></i> </span> </span>',
                };
                var bloc = '<a id="bloc' + data.nid + '" class="nav-link dropdown-toggle ' + classe2 + '" '
                  + 'href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">'
                  + '<span class="' + classe + '" '
                  // + 'data-bs-toggle="tooltip" data-bs-placement="left" '
                  + 'title="Déroulez pour choisir le mode de partage avec la classe" '
                  + '>' + data.code + ' ' + modes[data.mode] + '</span></a>'
                  + '<ul class="dropdown-menu" aria-labelledby="navbarDropdown">'
                  + '<li><span class="dropdown-item" style="cursor:pointer"'
                  + '          id="cpcode' + data.nid + '" data-url="' + data.code + '" data-toshake="bloc' + data.nid + '" '
                  + '          onclick="copyToMyClipboard(\'cpcode' + data.nid + '\', 1.2)">'
                  + '          Copier le code de partage avec la classe'
                  + '</span></li>'
                  + '<li><span class="dropdown-item" style="cursor:pointer"'
                  + '          id="cpurl' + data.nid + '" data-url="' + rootUrl + 'c/' + data.code + '" data-toshake="bloc' + data.nid + '" '
                  + '          onclick="copyToMyClipboard(\'cpurl' + data.nid + '\', 1.2)">'
                  + '          Copier l\'URL de partage avec la classe'
                  + '</span></li>'
                  + '<li><a class="dropdown-item use-ajax" style="text-decoration:none;"'
                  + 'data-dialog-options="{&quot;width&quot;:520, &quot;title&quot;:&quot;' + rootUrl + 'c/' + data.code + '&quot;}"'
                  + 'data-dialog-type="modal" href="my_qrcode/' + data.code + '">'
                  + 'Afficher le QRcode de partage'
                  + '</a>'
                  // + '<li><a class="dropdown-item" href="#">Copier l\'URL de publication privée (lecture seule)</a></li>'
                  + '</ul>';
                if (data.whoami == "cr") return bloc;
                if (data.whoami == "ap") return "Apprenant de " + data.boss;
                if (data.boss_role == "student") return "Associé par élève " + data.boss;
                if (data.whoami == "as") return bloc + "Associé par " + data.boss;
              },
            },
            { // ------------------- BIB --------------------
              // --------------------------------------------
              "data": null,
              "name": "bib",
              "className": "dt-center",
              render: function (data, type, row) {
                let mydata = data.bib;
                const parts = mydata.split(':');
                if (parts.length !== 2) return '';
                const id = parts[0];
                const state = parts[1];
                let classe, tip, color1, color2;
                const bibFormUrl = Drupal.url('c-act/' + id + '/bibmetadata');
                if (state == '1') {
                  // classe = 'fa-book';
                  classe = 'fas fa-users';
                  tip = 'Publié dans la bibliothèque entre enseignants';
                  color1 = '#2471a3';
                  color2 = 'green';
                } else if (state == '2') {
                  classe = 'fa fa-globe';
                  tip = 'Publié dans la bibliothèque publique';
                  color1 = '#2471a3';
                  color2 = '#3498db';
                } else {
                  tip = 'Publier';
                  color1 = 'lightgray';
                  color2 = 'gray';
                  return (
                    '<a href="' + bibFormUrl + '" class="use-ajax" data-bs-toggle="tooltip" data-bs-placement="right" title="' + tip + '">'
                    + '<i class="fa fa-share-alt" style="color: ' + color1 + ';"></i>'
                    + '</a>'
                  );
                }
                return (
                  '<a href="' + bibFormUrl + '" class="use-ajax">'
                  + '<span class="fa-layers fa-fw"'
                  + 'data-bs-toggle="tooltip" data-bs-placement="right" title="' + tip + '">'
                  + '<i data-fa-transform="shrink-2" class="fa fa-share-alt fa-lg" style="color: ' + color1 + ';"></i><i style="color: ' + color2 + ';" class="' + classe + ' fa-lg" data-fa-transform="shrink-6 up-8 left-9"></i>' +
                  '</span></a>'
                );
              }
            },
            { // ------------------- TAGS --------------------
              // ---------------------------------------------
              "data": null,
              render: function (data, type, row) {

                if (Object.keys(data.tags).length == 0) return ("");
                var str = "";
                var tids = data.tags.tids.split(',')
                var names = data.tags.names.split(',')
                var colors = data.tags.colors.split(',')
                tids.forEach(function callback(tid, idx) {
                  name = names[idx];
                  color = colors[idx];
                  if (name.match("Corbeille")) {
                    var icon = "fas fa-trash-alt tagicon";
                    var color = "";
                  } else {
                    var icon = "fas fa-tag";
                    var color = color;
                  }
                  str = str + '<span class="hidden">tag_' + tid + '</span>'
                    + '<div class="tag"><span class="tagicon"'
                    + 'style="color:' + color + '"><i class="' + icon + '"></i></span>'
                    + '<button type="submit" class="deltag naked_button" name="action-del-etiquette"'
                    + 'value="' + tid + '-' + data.nid + '" data-bs-toggle="tooltip" data-bs-placement="left" title="Supprimer">'
                    + '<i class="far fa-times-circle small red"></i>'
                    + '</button>'
                    + '&nbsp;<span class="naked_button">' + name + '</span>'
                    + '</div>'
                });
                return (str);
              },
            },
            { // ------------------- MORE --------------------
              // ---------------------------------------------
              "data": null,
              "orderable": false,
              render: function (data, type, row) {
                var str = ''
                  + '<a id="bloc' + data.nid + '" class="nav-link dropdown-toggle gray" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">'
                  + '<i class="far fa-plus-square"></i></a>'
                  + '<ul class="dropdown-menu" aria-labelledby="navbarDropdown">'
                if (data.whoami != "ap") {
                  str = str
                    + '<li><button class="action-indiv" type="submit" name="action-param" value="' + data.nid + '">'
                    + '          <i class="fas fa-cog"></i> Paramètres'
                    + '</button></li>'
                    + '<li><button class="action-indiv" type="submit" name="action-clone" value="' + data.nid + '">'
                    + '         <i class="far fa-clone"></i> Cloner'
                    + '</button></li>'
                    + '<li><button class="action-indiv" type="button" id="cpmoo' + data.nid + '" data-url="' + rootUrl + 'c/' + data.code + '/' + data.provider + '" data-toshake="bloc' + data.nid + '" '
                    + '          onclick="copyToMyClipboard(\'cpmoo' + data.nid + '\', 1.2)">'
                    + '          <i class="fas fa-code"></i> Copier l\'URL d\'intégration dans Moodle'
                    + ' <a href="https://capytale2.ac-paris.fr/wiki/doku.php?id=moodle_iframe" data-bs-toggle="tooltip" data-bs-placement="top" title="Plus d\'infos..."><i class="fas fa-info-circle"></i></a>'
                    + '</button></li>'
                  if (data.mode == "N_X") {
                    str = str + '<li><button class="action-indiv" type="submit" name="action-clonable" value="' + data.nid + '">'
                      + '         <i class="fas fa-lock-open green"></i> Débloquer la distribution'
                      + '</button></li>'
                  } else {
                    str = str + '<li><button class="action-indiv" type="submit" name="action-unclonable" value="' + data.nid + '">'
                      + '         <i class="fas fa-lock red"></i> Bloquer la distribution'
                      + '</button></li>'
                  }
                  // + '<li><a class="dropdown-item" href="#">Copier l\'URL de publication privée (lecture seule)</a></li>'
                }
                str = str
                  // + '<div class="btn-group dropstart">'
                  + '<li><button class="action-indiv" type="button" onclick="window.archiveBuilder.doOne(\'' + data.nid + '\')">'
                  + '         <i class="fas fa-download"></i> Télécharger'
                  + '</button></li>'
                  + '<div class="dropdown-divider"></div>'
                  + '<li><button class="action-indiv action-indiv-danger" type="submit" name="action-delete" value="' + data.nid + '" onclick="return confirm(\'Vous vous apprêtez à supprimer DÉFINITIVEMENT le(s) élément(s) sélectionné(s). Confirmez-vous avec certitude ?\')">'
                  + '         <i class="fas fa-window-close red"></i> Supprimer'
                  + '</button></li>'
                  + '</ul>'
                // + '</div>'
                return (str);
              },
            },
          ],
          "language": {
            search: "Rechercher ",
            "info": "lignes _START_ à _END_ sur _TOTAL_",
            "lengthMenu": "Afficher _MENU_",
            "loadingRecords": '<i class="fa fa-spinner fa-spin" style="font-size:48px;"></i>',
            "emptyTable": 'Aucune activité, cliquez sur <i class="fas fa-plus-circle"></i> pour commencer.',
            "paginate": {
              "first": "Première page",
              "last": "Dernière page",
              "next": "Page suivante",
              "previous": "Page précédente"
            },
          },
        });

        $(".whereIam_search").on('click', function () {
          var wasDown = $(this).hasClass("whereIam_down");
          $(".whereIam_search").each(function () {
            $(this).removeClass("whereIam_down");
          });

          if (wasDown) {
            table.column(1)
              .search("")
              .draw();
          }
          else {
            $(this).addClass("whereIam_down");
            table.column(1)
              .search($(this).val())
              .draw();
          }
        });



        // TAGS
        // ---------------------------------------------
        $(".tag_search").on('click', function () {
          // Highlight tag term in list
          $(".tag_search").each(function () {
            $(this).removeClass("roundedSquare");
          });
          var tag = String($(this).data("tag"));
          var tagtitle = String($(this).data("tagtitle"));
          $(this).toggleClass("roundedSquare");
          if (tagtitle == "displaywithnofilter") {
            $("#active-filter").text("");
            $("#active-filter").removeClass("active-filter-on");
            $("#hamburgernotif").hide();
          } else {
            $("#active-filter").text("Filtre actif : " + tagtitle);
            $("#active-filter").addClass("active-filter-on");
            $("#hamburgernotif").show();
          }

          // Tag Search
          table.column(7)
            .search($(this).data("tag"), true, true, true)
            .draw();
          localStorage.setItem('tagSearch', $(this).data("tag"));
          localStorage.setItem('tagTitleSearch', tagtitle);
        });


        if (localStorage.getItem('tagSearch') === null) {
          // hide Corbeille tagged by default
          table.column(7).search('^((?!Corbeille).)*$', true, true, true).draw();
        } else {
          // Highlight tag term in list
          $(".tag_search").each(function () {
            $(this).removeClass("roundedSquare");
          });
          var tagtitle = localStorage.getItem('tagTitleSearch');
          if (tagtitle == "displaywithnofilter") {
            $("#active-filter").text("");
            $("#active-filter").removeClass("active-filter-on");
            $(".openbtn").removeClass("pulse");
            $("#hamburgernotif").hide();
          } else {
            $("#active-filter").text("Filtre actif : " + tagtitle);
            $("#active-filter").addClass("active-filter-on");
            $("#hamburgernotif").show();
            if (localStorage.getItem('tagBarStatus') == 'closed') {
              $(".openbtn").addClass("pulse");
            } else {
              $(".openbtn").removeClass("pulse");
            }
          }
          document.querySelector("[data-tag='" + localStorage.getItem('tagSearch') + "']").classList.add("roundedSquare");
          // Tag Search
          table.column(7)
            .search(localStorage.getItem('tagSearch'), true, true, true)
            .draw();
        }

        Drupal.AjaxCommands.prototype.ChangeBibState = function (ajax, response, status) {
          try {
            table.data().each((r) => {
              if (r.nid == response.nid) {
                r.bib = response.nid + ':' + response.state;
                if (response.state == '1') {
                  r.status_shared = '1';
                  r.status_web = '0';
                } else if (response.state == '2') {
                  r.status_shared = '1';
                  r.status_web = '1';
                } else {
                  r.status_shared = '0';
                  r.status_web = '0';
                }
              }
            });
            table.rows().invalidate().draw();
          } catch { }
        };
      });//fin
  });
})(jQuery, Drupal, drupalSettings, window.archiveBuilder || (window.archiveBuilder = {}));

function copyToMyClipboard(id, factor) {
  var el = document.getElementById(id);
  copyTextToClipboard(el.dataset.url);

  var elem = document.getElementById(el.dataset.toshake);
  elem.animate([
    {
      transform: 'scale(1,1)'
    },
    {
      transform: "scale(" + factor + ", " + factor + ")"
    },
    {
      transform: 'scale(1,1)'
    },
    {
      transform: "scale(" + factor + ", " + factor + ")"
    },
    {
      transform: 'scale(1,1)'
    }],
    {
      duration: 400
    });
}
function fallbackCopyTextToClipboard(text) {
  var textArea = document.createElement("textarea");
  textArea.value = text;

  // Avoid scrolling to bottom
  textArea.style.top = "0";
  textArea.style.left = "0";
  textArea.style.position = "fixed";

  document.body.appendChild(textArea);
  textArea.focus();
  textArea.select();

  try {
    var successful = document.execCommand('copy');
    var msg = successful ? 'successful' : 'unsuccessful';
  } catch (err) {
    console.error('Fallback: Oops, unable to copy', err);
  }

  document.body.removeChild(textArea);
}
function copyTextToClipboard(text) {
  if (!navigator.clipboard) {
    fallbackCopyTextToClipboard(text);
    return;
  }
  navigator.clipboard.writeText(text).catch(function (err) {
    console.error('Async: Could not copy text: ', err);
  });
}


function toggleNav() {
  var x = document.getElementById("colA");
  if (x.style.display === "none") {
    // Ouvre le panneau
    document.getElementById("colB").classList.remove("colB-deploy");
    document.getElementById("colB").classList.add("colB-reduce");
    setTimeout(() => { x.style.display = "block"; }, 500);
    localStorage.setItem('tagBarStatus', 'open');
    document.getElementById('hamburger').classList.remove("pulse");
    document.getElementById('hamburgernotif').display = "none";
  } else {
    // Ferme le panneau
    x.style.display = "none";
    document.getElementById("colB").classList.add("colB-deploy");
    document.getElementById("colB").classList.remove("colB-reduce");
    localStorage.setItem('tagBarStatus', 'closed');
    var tagtitle = localStorage.getItem('tagTitleSearch');
    if (tagtitle != null && tagtitle != "displaywithnofilter") {
      document.getElementById('hamburger').classList.add("pulse");
      document.getElementById('hamburgernotif').display = "inline";
    }
  }
}
function toggleEtiquettes() {
  var x = document.getElementById("etiqueter");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
