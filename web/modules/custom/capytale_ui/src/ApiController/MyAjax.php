<?php

namespace Drupal\capytale_ui\ApiController;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Url;

/**
 * Controller for export json.
 */
class MyAjax extends ControllerBase
{

  public function data()
  {
    // No cache
    \Drupal::service('page_cache_kill_switch')->trigger();

    // get user infos
    $user = \Drupal::currentUser();
    $uid = $user->id();
    $roles = $user->getRoles(true);

    $is_student = \in_array("student", $roles);
    if ($is_student) {
      $is_teacher = false;
    } else {
      $is_teacher = \in_array("teacher", $roles);
      if (!$is_teacher) return $this->redirect('capytale_auth.idp_list');
    }

    $bigList = $this->bigQuery($uid);
    if (\count($bigList) === 0) return new JsonResponse([]);

    $manager = \Drupal::service('capytale_activity.manager');
    $session = \Drupal::request()->getSession();
    $provider = $session->get('c_a_provider');

    $all_nids = [];
    $all_owners_keyed = [];
    $all_aids = [];

    foreach ($bigList as $item) {
      $all_nids[] = $item->nid;
      if ($item->owner_id !== null) $all_owners_keyed[$item->owner_id] = null;
      if ($is_teacher && ($item->own_aid !== null)) $all_aids[] = $item->own_aid;
    }

    if (\count($all_nids) > 0) {
      $all_nids = \implode(',', $all_nids);
      $tids_keyed = $this->bigTagsQuery($uid, $all_nids);
    } else {
      $tids_keyed = [];
    }
    if (\count($all_owners_keyed) > 0) {
      $all_owners = \implode(',', \array_keys($all_owners_keyed));
      $all_owners_keyed = $this->bigOwnersQuery($all_owners);
    } else {
      $all_owners_keyed = [];
    }
    if ($is_teacher && \count($all_aids)) {
      $all_aids = \implode(',', $all_aids);
      $views_keyed = $this->bigViewsQuery($uid, $all_aids);
    } else {
      $views_keyed = [];
    }

    $now = time();
    foreach ($bigList as $item) {
      if ($is_teacher && ($item->own_aid !== null)) {
        if (\array_key_exists($item->own_aid, $views_keyed)) {
          $views = $views_keyed[$item->own_aid];
          $item->views_total = \intval($views->total);
          $item->views_hidden = \intval($views->hidden);
          $item->changed = \max($item->changed, $views->changed);
        } else {
          $item->views_total = 0;
          $item->views_hidden = 0;
        }
      }
      if (\array_key_exists($item->nid, $tids_keyed)) {
        $item->tags = $tids_keyed[$item->nid];
      } else {
        $item->tags = [];
      }
      if ($item->owner_id !== null) {
        if (\array_key_exists($item->owner_id, $all_owners_keyed)) {
          $boss = $all_owners_keyed[$item->owner_id];
          $boss_role = $boss->role;
          $boss = empty($boss->lastname) ? Xss::filter($boss->username) : Xss::filter($boss->lastname);
        } else {
          $boss = null;
          $boss_role = null;
        }
        if (empty($boss)) $boss = 'Inconnu';
        $item->boss = $boss;
        $item->boss_role = $boss_role;
      }
      $item->row_id = "row-" . $item->nid;
      $item->mode = $this->getMode($item, $now);
      $item->icon = $manager->buildIconUrl($item->type);
      // $item->title = Xss::filter($item->title);
      $item->title = htmlspecialchars($item->title);
      $item->evaluation = Xss::filter($item->evaluation);
      $item->appreciation = Xss::filter($item->appreciation);
      $item->provider = $provider;
      $item->last_access = $item->changed;
      $item->player = $this->getPlayer($item);
    }
    return new JsonResponse($bigList);
  }

  /**
   * Récupère plein d'infos sur les activités de l'utilisateur
   * 
   * @param int|string $uid
   */
  private function bigQuery($uid)
  {
    $sql =
      "WITH
MY AS
(
SELECT
  IF(nfd.type = 'student_assignment', nfd.nid, NULL) as said,
  nfd.nid as nid,
  (nfd.uid = :uid) as own,
  nfd.uid as uid,
  nfd.type as type,
  nfd.changed as changed,
  nfd.title as title
FROM node_field_data nfd
WHERE
  (nfd.uid = :uid
  OR
  nfd.nid in (SELECT entity_id FROM node__field_associates nfa where nfa.field_associates_target_id = :uid AND NOT nfa.deleted))
  AND
  nfd.type in ('activity', 'notebook_activity', 'console_activity', 'student_assignment')
),
REF AS
(
SELECT
  MY.nid as nid,
  MY.changed as changed,
  MY.said as said,
  (MY.said IS NOT NULL) as sa,
  MY.own as own,
  IF(MY.said IS NULL, MY.type, nfd.type) as actnodetype,
  IF(MY.said IS NULL, MY.title, nfd.title) as title,
  IF(MY.said IS NULL, MY.nid, nfa.field_activity_target_id) as aid,
  IF(MY.said IS NULL, IF(MY.own, NULL, MY.uid), nfd.uid) as owner_id
FROM MY
LEFT JOIN node__field_activity nfa ON nfa.entity_id = MY.said AND NOT nfa.deleted
LEFT JOIN node_field_data nfd on nfd.nid = nfa.field_activity_target_id
),
ALL_1 AS
(
SELECT
REF.*,
IF(REF.sa, 'ap', IF(REF.own, 'cr', 'as')) as whoami,
IF(REF.sa, NULL, REF.nid) as own_aid,
IF(nfw.field_workflow_value IS NULL, 0, nfw.field_workflow_value) as workflow,
nfc.field_code_value as code,
IF(nfss.field_status_shared_value IS NULL, 0, nfss.field_status_shared_value) as status_shared,
IF(nfsw.field_status_web_value IS NULL, 0, nfsw.field_status_web_value) as status_web,
IF(nfsc.field_status_clonable_value IS NULL, 0, nfsc.field_status_clonable_value) as status_clonable,
IF(nfatm.field_access_tr_mode_value IS NULL, 'none', nfatm.field_access_tr_mode_value) as access_tr_mode,
nfatr.field_access_timerange_value as tr_beg,
nfatr.field_access_timerange_end_value as tr_end,
IF(nfev.field_evaluation_value IS NULL, '', nfev.field_evaluation_value) as evaluation,
IF(nfap.field_appreciation_value IS NULL, '', nfap.field_appreciation_value) as appreciation,
nfat.field_activity_type_value as type
FROM REF
LEFT JOIN node__field_workflow nfw ON nfw.entity_id = REF.said AND NOT nfw.deleted
LEFT JOIN node__field_code nfc ON nfc.entity_id = REF.aid AND NOT nfc.deleted
LEFT JOIN node__field_status_shared nfss ON nfss.entity_id = REF.aid AND NOT nfss.deleted
LEFT JOIN node__field_status_web nfsw ON nfsw.entity_id = REF.aid AND NOT nfsw.deleted
LEFT JOIN node__field_status_clonable nfsc ON nfsc.entity_id = REF.aid AND NOT nfsc.deleted
LEFT JOIN node__field_access_tr_mode nfatm ON nfatm.entity_id = REF.aid AND NOT nfatm.deleted
LEFT JOIN node__field_access_timerange nfatr ON nfatr.entity_id = REF.aid AND NOT nfatr.deleted
LEFT JOIN node__field_evaluation nfev ON nfev.entity_id = REF.said AND NOT nfev.deleted
LEFT JOIN node__field_appreciation nfap ON nfap.entity_id = REF.said AND NOT nfap.deleted
LEFT JOIN node__field_activity_type nfat ON nfat.entity_id = REF.aid AND NOT nfat.deleted
)
SELECT
  ALL_1.*,
  IF(ALL_1.sa, 0, CONCAT(ALL_1.nid, ':', IF(ALL_1.status_shared, IF(ALL_1.status_web, 2, 1), 0))) as bib
FROM ALL_1";
    $db = \Drupal::database();
    return $db->query($sql, [':uid' => $uid])->fetchAll();
  }

  /**
   * Récupère les info sur les propriétaires
   * 
   * @param string $all_owners une chaine composée des uid séparés par ','
   */
  private function bigOwnersQuery($all_owners)
  {
    if (\preg_match('/[^0-9,]/', $all_owners)) throw new \Exception();
    $sql =
      "WITH
USERS AS
(
SELECT
  ufd.uid as uid,
  ufd.name as username,
  BIT_OR(CASE ur.roles_target_id WHEN 'teacher' THEN 2 WHEN 'student' THEN 1 ELSE NULL END) as profile
FROM users_field_data ufd
LEFT JOIN user__roles ur ON ur.entity_id = ufd.uid AND NOT ur.deleted
WHERE ufd.uid IN ({$all_owners})
GROUP BY ufd.uid
)
SELECT
 USERS.uid as uid,
 USERS.username as username,
 ufn.field_nom_value as lastname,
 ufp.field_prenom_value as firstname,
 CASE WHEN USERS.profile & 1 THEN 'student' WHEN USERS.profile & 2 THEN 'teacher' ELSE NULL END as role
FROM USERS
LEFT JOIN user__field_nom ufn ON ufn.entity_id = USERS.uid AND NOT ufn.deleted
LEFT JOIN user__field_prenom ufp ON ufp.entity_id = USERS.uid AND NOT ufp.deleted";
    $db = \Drupal::database();
    return $db->query($sql)->fetchAllAssoc('uid');
  }

  /**
   * Compte les nombres de vues
   * 
   * @param int|string $uid le userid de l'utilisateur courant
   * @param string $all_aids une chaine composée des nid séparés par ','
   */
  private function bigViewsQuery($uid, $all_aids)
  {
    if (\preg_match('/[^0-9,]/', $all_aids)) throw new \Exception();
    $sql =
      "WITH
TRASH_TID AS
(
SELECT
  ttd.tid as tid
FROM taxonomy_term_data ttd
JOIN user_term ut ON ut.tid = ttd.tid
JOIN taxonomy_term_field_data ttfd ON ttfd.tid = ttd.tid
WHERE
  ttd.vid = 'private_tags'
  AND ut.uid = :uid
  AND ttfd.name = 'corbeille'
),
VIEWS AS
(
SELECT
  nfa.field_activity_target_id as nid,
  nfd.changed as changed,
  1 as cnt,
  IF(EXISTS(SELECT 1 FROM node__field_etiquettes nfe WHERE NOT nfe.deleted AND nfe.field_etiquettes_target_id IN (SELECT tid FROM TRASH_TID) AND nfe.entity_id = nfa.entity_id), 1, NULL) as hidden
FROM node__field_activity nfa
JOIN node_field_data nfd ON nfd.nid = nfa.entity_id
WHERE NOT nfa.deleted
AND
nfa.field_activity_target_id IN ({$all_aids})
)
SELECT
  VIEWS.nid as nid,
  COUNT(VIEWS.cnt) as total,
  COUNT(VIEWS.hidden) as hidden,
  MAX(VIEWS.changed) as changed
FROM VIEWS
GROUP BY VIEWS.nid";
    $db = \Drupal::database();
    return $db->query($sql, [':uid' => $uid])->fetchAllAssoc('nid');
  }


  /**
   * Récupère les tid (tag id) des activités de l'utilisateur
   * 
   * @param string|int $uid le userid
   * @param string $all_nids une chaine composée des nid séparés par ','
   */
  private function bigTagsQuery($uid, $all_nids)
  {
    if (\preg_match('/[^0-9,]/', $all_nids)) throw new \Exception();
    $sql =
      "SELECT
  nfe.entity_id as nid,
  GROUP_CONCAT(nfe.field_etiquettes_target_id ORDER BY nfe.field_etiquettes_target_id SEPARATOR ',') as tids,
  GROUP_CONCAT(ttfd.name ORDER BY nfe.field_etiquettes_target_id SEPARATOR ',') as names,
  GROUP_CONCAT(ttfc.field_color_color ORDER BY nfe.field_etiquettes_target_id SEPARATOR ',') as colors
FROM node__field_etiquettes nfe
JOIN taxonomy_term_data ttd ON ttd.tid = nfe.field_etiquettes_target_id
JOIN taxonomy_term_field_data ttfd ON ttfd.tid = ttd.tid
JOIN taxonomy_term__field_color ttfc ON ttfc.entity_id = ttd.tid
JOIN user_term ut ON ut.tid = nfe.field_etiquettes_target_id
WHERE
  NOT nfe.deleted
  AND ttd.vid = 'private_tags'
  AND ut.uid = :uid
  AND nfe.entity_id IN ({$all_nids})
GROUP BY nfe.entity_id";
    $db = \Drupal::database();
    return $db->query($sql, [':uid' => $uid])->fetchAllAssoc('nid');
  }


  private function getMode($item, $now)
  {
    if (!$item->status_clonable) return "N_X"; // None : sans TimeRange
    if (!$item->access_tr_mode || $item->access_tr_mode == "none") return "N_O";
    if ($item->access_tr_mode == "complete") $value = "C_"; // Complete : rendu auto
    if ($item->access_tr_mode == "lock") $value = "L_"; // Lock : Verrouillage auto

    $atr_b = strtotime($item->tr_beg . 'Z');
    $atr_e = strtotime($item->tr_end . 'Z');
    $value .= (($atr_b < $now) && ($now < $atr_e)) ? "O" : "X";
    return $value;
  }

  private function getPlayer($item)
  {
    if ($item->sa) {
      $mode = 'assignment';
    } else {
      $mode = 'create';
    }
    return Url::fromRoute('c-act.play', ['nid' => $item->nid, 'mode' => $mode])->toString();
  }
}
