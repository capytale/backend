<?php

namespace Drupal\capytale_ui\Plugin\Filter;

use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * Provides a filter to caption elements.
 *
 * When used in combination with the filter_align filter, this must run last.
 *
 * @Filter(
 *   id = "filter_void",
 *   title = @Translation("Void Filter"),
 *   description = @Translation("Produit systématiquement une chaine vide."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE
 * )
 */
class FilterVoid extends FilterBase
{

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode)
  {
    return new FilterProcessResult('');
  }
}
