<?php
/**
 * @file
 * Contains \Drupal\capytale_ui\Plugin\Block\CodeBlock.
 */

namespace Drupal\capytale_ui\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'activity access code' block.
 *
 * @Block(
 *   id = "code_block",
 *   admin_label = @Translation("Code block"),
 *   category = @Translation("Custom code block")
 * )
 */
class CodeBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    $form = \Drupal::formBuilder()->getForm('Drupal\capytale_ui\Form\CodeForm');

    return $form;
   }
}
