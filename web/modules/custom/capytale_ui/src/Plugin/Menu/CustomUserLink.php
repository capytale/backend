<?php

namespace Drupal\capytale_ui\Plugin\Menu;

use Drupal\Core\Menu\MenuLinkDefault;

class CustomUserLink extends MenuLinkDefault
{

  public function getCacheContexts()
  {
    return ['user'];
  }

  public function getTitle()
  {
    if (\Drupal::currentUser()->isAnonymous()) {
      return "INVISIBLE";
    }
    $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
    return "{$user->field_prenom->value} {$user->field_nom->value}";
  }

  public function isEnabled()
  {
    return \Drupal::currentUser()->isAuthenticated();
  }

  public function getDescription()
  {
    return null;
  }

  public function getRouteName()
  {
    return 'c-auth.account';
  }
}
