<?php

namespace Drupal\capytale_ui\Plugin\Menu;

use Drupal\Core\Menu\MenuLinkDefault;

use Drupal\capytale_auth\Users\CurrentUser as CU;

class CustomSesameLink extends MenuLinkDefault
{
  public function getCacheContexts()
  {
    return ['user'];
  }

  public function getTitle()
  {
    return 'Comptes élèves Sésame';
  }

  public function getRouteName()
  {
    return 'c-auth.sesame-manage';
  }

  public function isEnabled()
  {
    $cu = CU::get();
    return $cu->isTeacher() &&  $cu->getProvider() === 'mail';
  }

  public function getDescription()
  {
    return "Gestion des comptes élève sans ENT";
  }
}
