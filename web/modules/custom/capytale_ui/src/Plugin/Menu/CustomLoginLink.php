<?php

namespace Drupal\capytale_ui\Plugin\Menu;

use Drupal\Core\Menu\MenuLinkDefault;

use Drupal\capytale_auth\Users\CurrentUser as CU;

class CustomLoginLink extends MenuLinkDefault
{
  public function getCacheContexts()
  {
    return ['user', 'route.name'];
  }



  public function getTitle()
  {
    if (CU::get()->isAuthenticated()) return 'Déconnexion';
    return 'Connexion';
  }

  public function getRouteName()
  {
    if (CU::get()->isAuthenticated()) return 'user.logout';
    return 'capytale_auth.idp_list';
  }

  public function isEnabled()
  {
    if (CU::get()->isAuthenticated()) return true;
    $rn = \Drupal::routeMatch()->getRouteName();
    if (empty($rn)) return true;
    return $rn != 'capytale_auth.idp_list';
  }

  public function getDescription()
  {
    $cu = CU::get();
    if ($cu->isAuthenticated()) {
      if ($cu->isSso()) return "Se déconnecter de Capytale. Retour à l'ENT.";
      return "Se déconnecter de Capytale.";
    }
    return "Se connecter à Capytale.";
  }
}
