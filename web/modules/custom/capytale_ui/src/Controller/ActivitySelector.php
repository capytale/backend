<?php

namespace Drupal\capytale_ui\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;

/**
 * Gère les préférences utilisateur sur les
 * activités favorites
 */
class ActivitySelector extends ControllerBase
{

  public function list()
  {
    // get user infos
    $user = \Drupal::currentUser();
    /** @var UserDataInterface $userData */
    $userData                            = \Drupal::service('user.data');
    $userParam['is_pixel_creator']       = $userData->get('capytale', $user->id(), 'is_pixel_creator');
    $userParam['is_codabloc_creator']    = $userData->get('capytale', $user->id(), 'is_codabloc_creator');
    $userParam['is_blockpython_creator'] = $userData->get('capytale', $user->id(), 'is_blockpython_creator');
    $userParam['is_codepuzzle_creator']  = $userData->get('capytale', $user->id(), 'is_codepuzzle_creator');
    $userParam['is_html_creator']        = $userData->get('capytale', $user->id(), 'is_html_creator');
    $userParam['is_console_creator']     = $userData->get('capytale', $user->id(), 'is_console_creator');
    $userParam['is_notebook_creator']    = $userData->get('capytale', $user->id(), 'is_notebook_creator');
    $userParam['is_logic_creator']       = $userData->get('capytale', $user->id(), 'is_logic_creator');
    $userParam['is_sql_creator']         = $userData->get('capytale', $user->id(), 'is_sql_creator');
    $userParam['is_ocaml_creator']       = $userData->get('capytale', $user->id(), 'is_ocaml_creator');
    $userParam['is_nbhosting_creator']   = $userData->get('capytale', $user->id(), 'is_nbhosting_creator');
    $userParam['is_jupyter_creator']     = $userData->get('capytale', $user->id(), 'is_jupyter_creator');
    $userParam['is_linux_creator']       = $userData->get('capytale', $user->id(), 'is_linux_creator');
    $userParam['is_geogebra_creator']    = $userData->get('capytale', $user->id(), 'is_geogebra_creator');
    $userParam['is_web_creator']         = $userData->get('capytale', $user->id(), 'is_web_creator');

    // get server infos or default:"drupal"
    // $session = \Drupal::request()->getSession();
    // if ($session->get('c_a_user')) {
    //   $serverName = $session->get('c_a_server');
    // };

    // $domain = "";
    // if (str_contains($serverName, 'creteil')) {
    //   $domain = "IDF";
    //   $jhub_url = "https://capytale2.ac-paris.fr/jhub94/";
    // } elseif (str_contains($serverName, 'paris')) {
    //   $domain = "IDF";
    //   $jhub_url = "https://capytale2.ac-paris.fr/jhub/";
    // } elseif (str_contains($serverName, 'versailles')) {
    //   $domain = "IDF";
    //   $jhub_url = "https://capytale2.ac-paris.fr/jhub78/";
    // }

    \Drupal::service('page_cache_kill_switch')->trigger();
    $ret = [
      '#theme' => 'activity_selector',
      '#vars' => [
        'userParam' => $userParam,
      ],
    ];

    if (\in_array('tester' , $this->currentUser()->getRoles(true))) {
      $ret['#vars']['tester'] = true;
    }
    if (\in_array('tester_vittscience' , $this->currentUser()->getRoles(true))) {
      $ret['#vars']['tester_vittascience'] = true;
    }
    if (\in_array('tester_mathalea' , $this->currentUser()->getRoles(true))) {
      $ret['#vars']['tester_mathalea'] = true;
    }

    return $ret;
  }
  public function star_activity()
  {
    // get user infos
    $user = \Drupal::currentUser();
    $userData = \Drupal::service('user.data');
    $query = \Drupal::request()->query->get('name');

    $response = new Response();

    if ($query == "pixel") {
      if ($userData->get('capytale', $user->id(), 'is_pixel_creator') == "1") {
        $userData->delete('capytale', $user->id(), 'is_pixel_creator');
        $response->setContent(0);
      } else {
        $userData->set('capytale', $user->id(), 'is_pixel_creator', True);
        $response->setContent(1);
      }
    }
    if ($query == "codabloc") {
      if ($userData->get('capytale', $user->id(), 'is_codabloc_creator')) {
        $userData->delete('capytale', $user->id(), 'is_codabloc_creator');
        $response->setContent(0);
      } else {
        $userData->set('capytale', $user->id(), 'is_codabloc_creator', True);
        $response->setContent(1);
      }
    }
    if ($query == "blockpython") {
      if ($userData->get('capytale', $user->id(), 'is_blockpython_creator')) {
        $userData->delete('capytale', $user->id(), 'is_blockpython_creator');
        $response->setContent(0);
      } else {
        $userData->set('capytale', $user->id(), 'is_blockpython_creator', True);
        $response->setContent(1);
      }
    }
    if ($query == "codepuzzle") {
      if ($userData->get('capytale', $user->id(), 'is_codepuzzle_creator')) {
        $userData->delete('capytale', $user->id(), 'is_codepuzzle_creator');
        $response->setContent(0);
      } else {
        $userData->set('capytale', $user->id(), 'is_codepuzzle_creator', True);
        $response->setContent(1);
      }
    }
    if ($query == "html") {
      if ($userData->get('capytale', $user->id(), 'is_html_creator')) {
        $userData->delete('capytale', $user->id(), 'is_html_creator');
        $response->setContent(0);
      } else {
        $userData->set('capytale', $user->id(), 'is_html_creator', True);
        $response->setContent(1);
      }
    }
    if ($query == "console") {
      if ($userData->get('capytale', $user->id(), 'is_console_creator')) {
        $userData->delete('capytale', $user->id(), 'is_console_creator');
        $response->setContent(0);
      } else {
        $userData->set('capytale', $user->id(), 'is_console_creator', True);
        $response->setContent(1);
      }
    }
    if ($query == "notebook") {
      if ($userData->get('capytale', $user->id(), 'is_notebook_creator')) {
        $userData->delete('capytale', $user->id(), 'is_notebook_creator');
        $response->setContent(0);
      } else {
        $userData->set('capytale', $user->id(), 'is_notebook_creator', True);
        $response->setContent(1);
      }
    }
    if ($query == "logic") {
      if ($userData->get('capytale', $user->id(), 'is_logic_creator')) {
        $userData->delete('capytale', $user->id(), 'is_logic_creator');
        $response->setContent(0);
      } else {
        $userData->set('capytale', $user->id(), 'is_logic_creator', True);
        $response->setContent(1);
      }
    }
    if ($query == "sql") {
      if ($userData->get('capytale', $user->id(), 'is_sql_creator')) {
        $userData->delete('capytale', $user->id(), 'is_sql_creator');
        $response->setContent(0);
      } else {
        $userData->set('capytale', $user->id(), 'is_sql_creator', True);
        $response->setContent(1);
      }
    }
    if ($query == "ocaml") {
      if ($userData->get('capytale', $user->id(), 'is_ocaml_creator')) {
        $userData->delete('capytale', $user->id(), 'is_ocaml_creator');
        $response->setContent(0);
      } else {
        $userData->set('capytale', $user->id(), 'is_ocaml_creator', True);
        $response->setContent(1);
      }
    }
    if ($query == "linux") {
      if ($userData->get('capytale', $user->id(), 'is_linux_creator')) {
        $userData->delete('capytale', $user->id(), 'is_linux_creator');
        $response->setContent(0);
      } else {
        $userData->set('capytale', $user->id(), 'is_linux_creator', True);
        $response->setContent(1);
      }
    }
    if ($query == "geogebra") {
      if ($userData->get('capytale', $user->id(), 'is_geogebra_creator')) {
        $userData->delete('capytale', $user->id(), 'is_geogebra_creator');
        $response->setContent(0);
      } else {
        $userData->set('capytale', $user->id(), 'is_geogebra_creator', True);
        $response->setContent(1);
      }
    }
    if ($query == "web") {
      if ($userData->get('capytale', $user->id(), 'is_web_creator')) {
        $userData->delete('capytale', $user->id(), 'is_web_creator');
        $response->setContent(0);
      } else {
        $userData->set('capytale', $user->id(), 'is_web_creator', True);
        $response->setContent(1);
      }
    }
    $response->setMaxAge(10);
    return $response;
  }

  public function vittascienceList()
  {
    /** @var \Drupal\capytale_activity\Activity\ActivityManager $manager */
    $manager = \Drupal::service('capytale_activity.manager');
    $list = [];
    foreach ($manager->getAllTypes() as $type) {
      if ('lti.vs.' === \substr($type, 0, 7)) {
        $cfg = $manager->getCfg($type);
        $list[] = [
          'name' => $cfg->name,
          'icon' => $manager::build_icon_url($cfg),
          'link' => $manager::buildAddUrl($cfg),
        ];
      }
    }
    $content = [
      '#theme' => 'vittascience_selector',
      '#list' => $list,
    ];
    $response = new AjaxResponse();
    $response->addCommand(new OpenModalDialogCommand('Créer une nouvelle activité Vittascience', $content, ['width' => 'none']));
    return $response;
  }
}
