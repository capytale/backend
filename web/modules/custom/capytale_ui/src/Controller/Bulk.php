<?php

namespace Drupal\capytale_ui\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class Bulk extends ControllerBase
{

  public function bulk_my_actions(Request $request)
  {
    if (!\Drupal::service('csrf_token')->validate($request->request->get('csrf_tk'), 'bulk_workflow')) {
      throw new \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException();
    }
    $request->request->remove('csrf_tk');

    // $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
    $action = $request->request->get('action', 0);
    $nids = $request->request->get('nids', []);


    if (str_contains($action, 'etiquette_add')) {
      $a = explode("-", $action);
      $action = $a[0];
      $etiquette = $a[1];
    }

    // récupère les éventuelles actions individuelles
    $indiv = array();
    $indiv['param'] = $request->request->get('action-param', 0);
    // $indiv['untrash'] = $request->request->get('action-untrash', 0);
    // $indiv['trash'] = $request->request->get('action-trash', 0);
    $indiv['delete'] = $request->request->get('action-delete', 0);
    $indiv['unclonable'] = $request->request->get('action-unclonable', 0);
    $indiv['clonable'] = $request->request->get('action-clonable', 0);
    $indiv['unshared'] = $request->request->get('action-unshared', 0);
    $indiv['shared'] = $request->request->get('action-shared', 0);
    $indiv['clone'] = $request->request->get('action-clone', 0);
    $indiv['del-etiquette'] = $request->request->get('action-del-etiquette', 0);

    // élimine les éléments dot la valeur est 0
    $indiv = \array_diff($indiv, [0]);

    // définit l'action et le node
    if (count($indiv) == 1) {
      $action = array_key_first($indiv);
      $nids = array($indiv[$action]);
    }
    // $this->messenger()->addMessage(implode(",", $indiv));

    $response = $this->redirect('capytale_ui.my');

    if (empty($nids)) {
      $this->messenger()->addWarning('Aucun élément sélectionné.');
      return $response;
    }

    switch ($action) {
      case "param":
        return $this->redirect(
          'entity.node.edit_form',
          ['node' => $nids[0]]
        );
        break;
      case "delete":
        foreach ($nids as $n) {
          $this->delete_node($n);
        }
        break;
      case "csv":
        return new Response($this->csv($nids), 200, [
          'Content-Type' => 'text/csv',
          'Content-Description' => 'File Download',
          'Content-Disposition' => 'attachment',
        ]);

        break;
      case "unclonable":
        foreach ($nids as $n) {
          $this->unclonable_node($n);
        }
        break;
      case "clonable":
        foreach ($nids as $n) {
          $this->clonable_node($n);
        }
        break;
      case "unshared":
        foreach ($nids as $n) {
          $this->unshared_node($n);
        }
        break;
      case "shared":
        foreach ($nids as $n) {
          $this->shared_node($n);
        }
        break;
      case "clone":
        foreach ($nids as $n) {
          \Drupal\capytale_ui\Controller\Misc::static_clone_entity($n);
        }
        break;
      case "del-etiquette":
        $pieces = explode("-", $nids[0]);
        // $this->messenger()->addMessage(implode(",", $nids));
        $this->delete_etiquette($pieces[0], $pieces[1]);
        break;
      case "etiquette_add":
        // $this->messenger()->addMessage("etiqme ".implode(",", $nids));
        foreach ($nids as $n) {
          $this->add_etiquette($etiquette, $n);
        }
        break;
    }
    // $this->messenger()->addMessage('Modifications enregistrées.');
    \Drupal::service('entity.memory_cache')->deleteAll();
    return $response;
  }

  private function clonable_node($n)
  {
    $node = \Drupal\node\Entity\Node::load($n);
    if ($node->bundle() === 'notebook') {
      $node->get('field_studentaccess')->setValue(1);
    } else {
      $node->get('field_status_clonable')->setValue(1);
    }
    $node->save();
  }

  private function unclonable_node($n)
  {
    $node = \Drupal\node\Entity\Node::load($n);
    if ($node->bundle() === 'notebook') {
      $node->get('field_studentaccess')->setValue(0);
    } else {
      $node->get('field_status_clonable')->setValue(0);
    }
    $node->save();
  }
  private function shared_node($n)
  {
    $node = \Drupal\node\Entity\Node::load($n);
    if ($node->bundle() === 'notebook') {
      $node->get('field_teacheraccess')->setValue(1);
    } else {
      $node->get('field_status_shared')->setValue(1);
    }
    $node->save();
  }

  private function unshared_node($n)
  {
    $node = \Drupal\node\Entity\Node::load($n);
    if ($node->bundle() === 'notebook') {
      $node->get('field_teacheraccess')->setValue(0);
    } else {
      $node->get('field_status_shared')->setValue(0);
    }
    $node->save();
  }

  private function delete_etiquette($tid, $nid)
  {
    $node = \Drupal\node\Entity\Node::load($nid);
    $f_etq = $node->get('field_etiquettes');
    $etq = $f_etq->getValue();
    // Get the index to remove
    $to_remove = array_search($tid, array_column($etq, 'target_id'));
    // Actually remove the item
    $f_etq->removeItem($to_remove);
    $node->save();
  }

  private function add_etiquette($tid, $nid)
  {
    $node = \Drupal\node\Entity\Node::load($nid);
    $f_etq = $node->get('field_etiquettes');
    $etq = $f_etq->getValue();
    // check if not already there
    if (!in_array($tid, array_column($etq, 'target_id'))) {
      // Actually add the item
      $f_etq->appendItem($tid);
    }
    $node->save();
  }

  private function csv($nids)
  {
    $sql =
      "SELECT
      nfda.nid as act_nid,
      nfd.nid as sa_nid,
      nfda.title as title,
      nfd.uid as uid,
      ufc.field_classe_value as classe,
      ufn.field_nom_value as nom,
      ufp.field_prenom_value as prenom,
      nfe.field_evaluation_value as eval
    FROM node_field_data nfda
    LEFT JOIN node__field_activity nfa on nfa.field_activity_target_id = nfda.nid
    LEFT JOIN node_field_data nfd on nfd.nid = nfa.entity_id
    LEFT JOIN node__field_evaluation nfe on nfe.entity_id = nfa.entity_id
    LEFT JOIN user__field_nom ufn on ufn.entity_id = nfd.uid
    LEFT JOIN user__field_prenom ufp on ufp.entity_id = nfd.uid
    LEFT JOIN user__field_classe ufc on ufc.entity_id = nfd.uid
    WHERE nfda.nid IN (:nids[])";
    $db = \Drupal::database();
    $result = $db->query($sql, [':nids[]' => $nids]);

    $act_list = [];
    $student_list = [];
    while ($sa = $result->fetchObject()) {
      if (!\array_key_exists($sa->act_nid, $act_list)) $act_list[$sa->act_nid] = $sa->title;
      if (!empty($sa->uid)) {
        $e = \strip_tags($sa->eval);
        if (\array_key_exists($sa->uid, $student_list)) {
          $student_list[$sa->uid]['e'][$sa->act_nid] = $e;
        } else {
          $student_list[$sa->uid] = [
            'c' => $sa->classe,
            'n' => $sa->nom,
            'p' => $sa->prenom,
            'e' => [$sa->act_nid => $e]
          ];
        }
      }
    }
    $actIds = \array_keys($act_list);

    $header = ["CLASSE", "NOM", "PRENOM"];
    foreach ($actIds as $actId) {
      $header[] = $act_list[$actId];
    }
    $tab = [$header];

    foreach ($student_list as $student) {
      $row = [$student['c'], $student['n'], $student['p']];
      $evals = $student['e'];
      foreach ($actIds as $actId) {
        if (\array_key_exists($actId, $evals)) {
          $row[] = $evals[$actId];
        } else {
          $row[] = '';
        }
      }
      $tab[] = $row;
    }

    $csv = "";
    foreach ($tab as $line) {
      $csv .= implode(';', $line) . "\n";
    }
    return $csv;
  }

  private function delete_node($n)
  {
    // Cas d'une entité de type node
    $node = \Drupal\node\Entity\Node::load($n);
    if (\Drupal::currentUser()->id() == $node->getOwnerId()) {
      $access = $this->Get_access($node);
      if ($access['nb_access'] == 0) {
        // s'il n'y a aucune vue, on supprime
        if (!$node->access('delete')) return;
        $node->delete();
      } else {
        // s'il y a des vues, on anonymise
        $node->get('uid')->setValue(0);
        $node->save();
      }
    } else {
      // si pas proprio, on retire de la liste des associés
      $f_assoc = $node->get('field_associates');
      $assoc = $f_assoc->getValue();
      $uid = \Drupal::currentUser()->id();
      // Get the index to remove
      $to_remove = array_search($uid, array_column($assoc, 'target_id'));
      // Actually remove the item
      $f_assoc->removeItem($to_remove);
      $node->save();
    }
  }

  private function Get_access($n)
  {
    // get last_access and nb_access
    $tab = array();

    // get last_access and nb_access
    $database = \Drupal::database();
    $query = $database->select('node_field_data', 'n');
    $query->join('node__field_activity', 'nfa', 'n.nid = nfa.entity_id');
    $query->condition('nfa.field_activity_target_id', $n->id());
    $query->addExpression('MAX(n.changed)', 'last_access');
    $query->addExpression('COUNT(*)', 'count');
    $record = $query->execute()->fetchObject();
    $tab['last_access'] = $record->last_access;
    $tab['nb_access'] = $record->count;
    $tab['last_access'] = ($tab['last_access'] == 0) ? $n->changed->value : $tab['last_access'];
    return $tab;
  }
}
