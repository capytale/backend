<?php

namespace Drupal\capytale_ui\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Component\Utility\Xss;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException as NFHE;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException as ADHE;


class Assignments extends ControllerBase
{

  public function teacher_page($nid)
  {
    // No cache
    \Drupal::service('page_cache_kill_switch')->trigger();

    $node = \Drupal\node\Entity\Node::load($nid);
    if (!$node) {
      throw new NFHE();
    }
    if (!$node->access('c-sa:review')) {
      throw new ADHE();
    }

    $uid = $node->getOwner()->id();
    $current_uid = \Drupal::currentUser()->id();
    if ($uid == $current_uid) {
      $status = "Auteur";
    } else {
      $status = "Vous êtes associé·e à cette activité";
    }

    /** @var \Drupal\capytale_activity\Activity\ActivityManager $c_act_manager */
    $c_act_manager = \Drupal::service('capytale_activity.manager');
    $activityBunch = $c_act_manager->getActivityBunch($node);
    if (!$activityBunch) throw new NFHE();
    if ($activityBunch->isSa()) throw new NFHE();
    $act_type = $activityBunch->getType();
    $cfg = $activityBunch->getCfg();
    $icon = $c_act_manager->buildIconUrl($act_type);
    $jsSettings = [
      'nid' => \intval($nid),
      'type' => $act_type,
      'exportable' => $cfg->exportable,
      'detailedEvaluation' => $cfg->detailedEvaluation,
      'icon' => $icon,
    ];

    // look for student assignments
    //
    // Remarque : la methode suivante ne permet pas de
    // aux associés de récupérer les copies
    // $query = \Drupal::entityQuery('node')->condition('field_activity', $nid);
    // $nids = $query->execute();
    // $list = \Drupal::entityManager()->getStorage('node')->loadMultiple($nids);
    //
    // Donc on fait à la main :
    $database = \Drupal::database();
    $query = $database->select('node__field_activity', 'fa');
    $query->fields('fa', array('entity_id'));
    $query->condition('field_activity_target_id', $nid);
    $list = $query->execute()->fetchAll();

    $tab = [];
    foreach ($list as $sa) {
      $sa_nid = $sa->entity_id;
      $n = \Drupal\node\Entity\Node::load($sa_nid);
      $sa_uid = $n->getOwner()->id();
      $sa_user = \Drupal\user\Entity\User::load($sa_uid);

      // $appr = Xss::filter($n->get('field_appreciation')->value);
      // $eval = Xss::filter($n->get('field_evaluation')->value);
      $appr =$n->get('field_appreciation')->value;
      $eval = $n->get('field_evaluation')->value;

      $tab[] = array(
        'changed' => $n->get('changed')->value,
        'sa_nid' => $sa_nid,
        'sa_uid' => $sa_uid,
        'hidden' => $this->is_in_corbeille($sa_nid),
        'nom' => $sa_user->get('field_nom')->value,
        'prenom' => $sa_user->get('field_prenom')->value,
        'classe' => $sa_user->get('field_classe')->value,
        'etab' => $sa_user->get('field_etab')->target_id,
        'status' => $n->get('field_status_read_only')->value,
        'workflow' => $n->get('field_workflow')->value,
        'appreciation' => $appr,
        'evaluation' => $eval,
        'player' => Url::fromRoute('c-act.play', ['nid' => $sa_nid, 'mode' => 'review'])->toString(),
      );
    }

    $ret = [
      '#theme' => 'assignments',
      '#vars' => [
        'icon' => $icon,
        'status' => $status,
        'status_clonable' => $node->get('field_status_clonable')->getString(),
        'access_tr_mode' => $node->get('field_access_tr_mode')->getString(),
        'is_in_time_range' => IsInTimerange($node),
        'dt_deb' => strtotime($node->get('field_access_timerange')->value),
        'dt_fin' => strtotime($node->get('field_access_timerange')->end_value),
        'title' => $node->getTitle(),
        'nid' => $nid,
        'tab' => $tab,
        'csrf_tk' => \Drupal::service('csrf_token')->get('bulk_workflow'),
      ],
      '#attached' => ['drupalSettings' => ['c_act' => $jsSettings]],
    ];
    return $ret;
  }

  public function is_in_corbeille($nid)
  {
    $user = \Drupal::currentUser();
    $user_entity = \Drupal\user\Entity\User::load($user->id());
    $roles = $user_entity->getRoles();
    $corbeilleTid = $this->get_corbeille_tid();
    if (!in_array('administrator', $roles)) {
      $tags = $this->Get_tags($nid, $user);
      foreach ($tags as $tag) {
        if ($tag['id'] == $corbeilleTid) return True;
      }
      return False;
    }
    return False;
  }

  public function Get_tags($nid, $user)
  {
    $tags = array();
    $n = \Drupal\node\Entity\Node::load($nid);

    $terms = $n->field_etiquettes->referencedEntities();
    foreach ($terms as $term) {
      // Get tags owners.
      $database = \Drupal::database();
      $query = $database->select('user_term', 'ut');
      $query->condition('ut.tid', $term->id());
      $query->fields('ut', ['uid']);
      $record = $query->execute()->fetchObject();
      // display only user's tagss
      if ($record->uid == $user->id()) {
        $tags[] = array(
          'id' => $term->id(),
          'title' => $term->label(),
          'color' => $term->get('field_color')->getString(),
        );
      }
    }
    return $tags;
    // return array(1=>['id'=>"o", 'title'=>$n->get('type')->getString(), 'color'=>'#123']);
  }
  private function add_etiquette($tid, $nid)
  {
    $node = \Drupal\node\Entity\Node::load($nid);
    $f_etq = $node->get('field_etiquettes');
    $etq = $f_etq->getValue();
    $changed = $node->get('changed')->value;
    // check if not already there
    if (!array_search($tid, array_column($etq, 'target_id'))) {
      // Actually add the item
      $f_etq->appendItem($tid);
    }
    // pour ne pas changer l'heure on ajoute une 1s
    $node->get('changed')->setValue($changed + 1);
    $node->save();
  }

  private function delete_etiquette($tid, $nid)
  {
    $connection = \Drupal::database();
    $connection->delete('node__field_etiquettes')
      ->condition('entity_id', $nid)
      ->condition('field_etiquettes_target_id', $tid)
      ->execute();
    $node = \Drupal\node\Entity\Node::load($nid);
    $changed = $node->get('changed')->value;
    $node->get('changed')->setValue($changed + 1);
    $node->save();
  }

  public function get_corbeille_tid()
  {
    $vid = 'private_tags';
    $service = \Drupal::service('entity_type.manager');
    $terms = $service->getStorage('taxonomy_term')->loadTree($vid);
    $recycle_label = 'Corbeille';
    $maxweight = 0;
    foreach ($terms as $term) {
      if ($term->name == $recycle_label) return $term->tid;
      $maxweight = max($maxweight, $term->weight);
    }
    $this->add_recycle($vid, $recycle_label, $maxweight + 1);
    return $this->get_corbeille_tid();
  }

  protected function add_recycle($vid, $label, $weight)
  {
    $term_storage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
    $term = $term_storage->create([
      'name' => $label,
      'vid' => $vid,
      'weight' => $weight
    ]);
    $term->save();
  }

  public function edit_ap_ev(Request $request)
  {
    $json = $request->getContent();;
    $json = json_decode($json, True, 3);
    $node = \Drupal\node\Entity\Node::load($json['nid']);
    if (!$node->access('update')) return new Response('', Response::HTTP_FORBIDDEN);

    $value = $json['value'];

    if ($json['task'] == "ap_ch") {
      $node->get('field_appreciation')->setValue($value);
    }
    if ($json['task'] == "ev_ch") {
      $node->get('field_evaluation')->setValue($value);
    }

    $node->save();
    return new Response('', 204);
  }

  public function bulk_change_workflow($nid, Request $request)
  {
    if (!\Drupal::service('csrf_token')->validate($request->request->get('csrf_tk'), 'bulk_workflow')) {
      throw new \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException();
    }

    $request->request->remove('csrf_tk');
    $action = $request->request->get('action-tag', 0);
    $wf_value = $request->request->get('workflow', 0);
    if (str_contains($action, 'tag') || str_contains($wf_value, 'tag')) {
      $nids = $request->request->get('nids', []);
      $tid = $this->get_corbeille_tid();
      if (!$nids) {
        $a = explode("-", $action);
        $action = $a[0];
        $nids = [$a[1]];
      } else {
        $action = $wf_value;
      }
      foreach ($nids as $sa) {
        // $this->messenger()->addMessage("$tid sur $sa");
        if (str_contains($action, 'add')) {
          // $this->messenger()->addMessage("Add");
          $this->add_etiquette($tid, $sa);
        } elseif (str_contains($action, 'del')) {
          // $this->messenger()->addMessage("Del");
          $this->delete_etiquette($tid, $sa);
        }
      }
    } else {
      $wf_value = $request->request->get('workflow', 0);
      $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
      $nids = $request->request->get('nids', []);
      if (empty($wf_value)) {
        $this->messenger()->addWarning('Veuillez choisir un état.');
      } else if (empty($nids)) {
        $this->messenger()->addWarning('Aucun élément sélectionné.');
      } else {
        foreach ($nids as $sa) {
          $node = \Drupal\node\Entity\Node::load($sa);
          // TODO : add condition : teacher
          // if ($user->id() == $node->getOwner()->id() ){
          $node->get('field_workflow')->setValue($wf_value);
          $node->save();
          // }
        }
        $this->messenger()->addMessage('Modifications enregistrées.');
      }
    }
    $search = \Drupal::request()->query->get('search');

    return $this->redirect('capytale_ui.teacher', ['nid' => $nid], ['query' => ['search' => $search]]);
  }
}
