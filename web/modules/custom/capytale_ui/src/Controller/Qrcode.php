<?php

namespace Drupal\capytale_ui\Controller;

use Drupal\Core\Controller\ControllerBase;

class Qrcode extends ControllerBase
{

  public function display($code)
  {
    $user = \Drupal::currentUser();
    $roles = $user->getRoles();

    if ($user->isAnonymous()) {
      return $this->redirect('capytale_auth.idp_list');
    }

    return [
      '#theme' => 'my_qrcode',
      '#vars' => [
        'csrf_tk' => \Drupal::service('csrf_token')->get('bulk_workflow'),
        'roles' => $roles,
        'code' => $code,
      ],
    ];
  }
}
