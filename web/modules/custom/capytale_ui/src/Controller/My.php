<?php

namespace Drupal\capytale_ui\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;

class My extends ControllerBase
{
  public function My_ajax()
  {
    $user = \Drupal::currentUser();
    $roles = $user->getRoles();

    if ($user->isAnonymous()) {
      return $this->redirect('capytale_auth.idp_list');
    }

/*
    $form = [
      '#create_placeholder' => TRUE,
      '#lazy_builder' => [
        'capytale_ui.lazybuilders:render',
        ['Drupal\capytale_ui\Form\CodeForm'],
      ],
    ];
*/
    $form = \Drupal::formBuilder()->getForm('Drupal\capytale_ui\Form\CodeForm');

    // ksm($form);
    $header = [
      "#theme" => 'my_code',
      '#form' => $form,
    ];

    if (in_array("teacher", $roles)) {
      $output = [
        '#theme' => 'my_ajax',
        '#attached' => ['library' => ['capytale_ui/myTeacher']],
        '#vars' => [
          'csrf_tk' => \Drupal::service('csrf_token')->get('bulk_workflow'),
          'roles' => $roles,
          'mode' => 'teacher',
        ],
      ];
      return [$header, $output];
    } elseif (in_array("student", $roles)) {
      $output = [
        '#theme' => 'my_ajax',
        '#attached' => ['library' => ['capytale_ui/myStudent']],
        '#vars' => [
          'csrf_tk' => \Drupal::service('csrf_token')->get('bulk_workflow'),
          'roles' => $roles,
          'mode' => 'student',
        ],
      ];
      return [$header, $output];
    } else {
      return [
        '#plain_text' => 'Vous êtes bien authentifié par l\'ENT mais nous n\'avons pas pu déterminer votre statut (enseignant ou élève). Veuillez prendre contact avec le service en charge de votre ENT afin que le nécessaire soit fait.',
        '#cache' => ['max-age' => 0],
      ];
    }
  }
}
