<?php

namespace Drupal\capytale_ui\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Drupal\capytale_activity\Activity\ActivityManager;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Drupal\capytale_bib\GenealogyHooks;


class Misc extends ControllerBase
{

  public function clone_entity($nid)
  {
    $clone = self::static_clone_entity($nid);
    return new RedirectResponse(Url::fromRoute('capytale_ui.my')->toString());
  }

  public function clone_entity_from_player($nid)
  {
    $clone = self::static_clone_entity($nid);
    return new Response('', 204);
  }

  /**
   * fonction statique qui est appelée depuis la page my
   * @return \Drupal\node\NodeInterface le clone
   */
  public static function static_clone_entity($nid)
  {
    $entity = \Drupal\node\Entity\Node::load($nid);
    // verifie l'accès
    if (!$entity) throw new AccessDeniedHttpException();
    if (!$entity->access('c-a:clone')) throw new AccessDeniedHttpException();

    $uid = \Drupal::currentUser()->id();

    #------ Clone l'entité
    $duplicate = $entity->createDuplicate();


    # Celui qui clone devient propriétaire du clone
    $duplicate->get('uid')->setValue(\Drupal::currentUser()->id());
    # Le clone a un nom un peu différent
    $duplicate->setTitle("Copie de " . $duplicate->getTitle());
    # Le clone n'est pas partagé dans la bibliothèque
    $duplicate->get('field_status_shared')->setValue(0);
    # Le clone démarre avec 0 clone comptabilisés
    $duplicate->get('field_nb_clone')->setValue(0);
    # Le clone n'est pas partagé avec des associés
    $duplicate->set('field_associates', []);
    # Le clone n'est pas étiqueté par les étiquettes d'autres utilisateurs
    $etiqField = $duplicate->get('field_etiquettes');
    if (!$etiqField->isEmpty()) {
      $tids = [];
      foreach ($etiqField as $v) {
        $tids[] = $v->target_id;
      }
      $tids = implode(',', $tids);
      if (\preg_match('/[^0-9,]/', $tids)) throw new \Exception();
      $db = \Drupal::database();
      $sql =
        "SELECT tid
FROM {user_term}
WHERE tid IN ({$tids})
AND uid = :uid";
      $tids = $db->query($sql, [':uid' => $uid])->fetchCol();
      $etiqField->setValue($tids);
    }

    # Remplacement du code de partage fait automatiquement

    $duplicate->save();

    if ($entity->get('type')->getString() == 'activity') {
      /** @var \Drupal\capytale_activity\Activity\ActivityManager $mng */
      $mng = \Drupal::service('capytale_activity.manager');
      $type = $mng->get_type($entity);
      $mng->prepare_activity($type, $duplicate);
      $cfg = $mng->getCfg($type);
      if (isset($cfg->lti)) {
        /** @var \Drupal\lti_player\LtiActivity\LtiActivityServiceInterface $ltiActivitySvc */
        $ltiActivitySvc = \Drupal::service('lti_player.activity');
        $ltiActivity = $ltiActivitySvc->getLtiActivity($duplicate->id());
        if (!empty($ltiActivity)) {
          $sourceLtiActivity = $ltiActivitySvc->getLtiActivity($entity->id());
          if (!empty($sourceLtiActivity)) {
            $ltiActivity->cloneFrom($sourceLtiActivity);
          }
        }
      }
    }

    // clone les champs FS
    /** @var \Drupal\capytale_activity\Activity\Fields $fs */
    $fs = \Drupal::service('capytale_activity.fields');
    $fs->clone_fields($entity, $duplicate);

    #------ Incrémente le compteur de clones
    $nb = (int)$entity->get('field_nb_clone')->value;
    $entity->get('field_nb_clone')->setValue($nb + 1);
    $entity->save();

    #------ Renseigne la table clone_genealogy
    GenealogyHooks::filiationAdd($entity, $duplicate);



    return $duplicate;
  }

  public function activity_access($nid)
  {
    // No cache
    \Drupal::service('page_cache_kill_switch')->trigger();

    // get node
    $node = \Drupal\node\Entity\Node::load($nid);
    if (!$node) throw new AccessDeniedHttpException();
    if (!$node->access('view')) throw new AccessDeniedHttpException();

    /** @var \Drupal\capytale_activity\Activity\ActivityManager $manager */
    $manager = \Drupal::service('capytale_activity.manager');
    $act_type = $manager->get_activity_type($node);
    if (!$act_type) throw new AccessDeniedHttpException();
    $url = $manager->buildPlayerUrl($act_type, $nid, ActivityManager::CREATE);
    return new TrustedRedirectResponse($url);
  }



}
