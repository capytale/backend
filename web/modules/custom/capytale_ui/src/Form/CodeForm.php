<?php

/**
 * @file
 * Contains \Drupal\capytale_ui\Form/CodeForm.
 */

namespace Drupal\capytale_ui\Form;

use \Drupal\node\Entity\Node;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\capytale_activity\Activity\ActivityManager;

/**
 * Contribute form.
 */
class CodeForm extends FormBase
{
  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'capytale';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $code = "")
  {

    $form['#attributes'] = array('class' => 'form-inline');

    $form['code'] = array(
      '#type' => 'textfield',
      '#title' => $this->t(''),
      '#default_value' => $code,
      '#size' => 10,
      '#attributes' => array(
        'placeholder' => 'a12b-345678',
      ),
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => 'Go !',
      '#attributes' => array(
        'class' => ['go_button'],
      ),
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    if (!$this->currentUser()->isAuthenticated()) {
      $error_msg = 'Vous devez d\'abord vous connecter pour accéder à un notebook.';
      $form_state->setErrorByName('code', $this->t($error_msg));
      return;
    }
    $userCode = trim($form_state->getValue('code'));
    /** @var \Drupal\capytale_activity\Activity\ActivityManager $c_act_manager */
    $c_act_manager = \Drupal::service('capytale_activity.manager');
    $activity = $c_act_manager->findActivityBunchFromCode($userCode);
    if (empty($activity)) {
      $error_msg = 'Ce code n\'est pas conforme.';
      $form_state->setErrorByName('code', $this->t($error_msg));
      return;
    }
    $form_state->set('activity', $activity);
    $assignment = $activity->getAssignment($this->currentUser());
    if (empty($assignment)) {
      // L'élève n'a pas de student_assignment. As-t-il le droit de création ?
      if (!$activity->getActivityNode()->access('c-sa:create')) {
        $error_msg = 'La création de copie n\'est pas autorisée.';
        $form_state->setErrorByName('code', $this->t($error_msg));
        return;
      }
    } else {
      // L'élève a déjà un student_assignment. As-t-il au moins le droit de lecture ?
      if (!$assignment->getAssignmentNode()->access('view')) {
        $error_msg = 'L\'accès à cette activité n\'est pas autorisé.';
        $form_state->setErrorByName('code', $this->t($error_msg));
        return;
      }
      $form_state->set('assignment', $assignment);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    /** @var \Drupal\capytale_activity\Activity\ActivityBunchInterface $assignment */
    $assignment = $form_state->get('assignment');
    if (empty($assignment)) {
      /** @var \Drupal\capytale_activity\Activity\ActivityBunchInterface $activity */
      $activity = $form_state->get('activity');
      $assignment = $activity->createAssignment($this->currentUser());
    }
    $form_state->setResponse(
      new TrustedRedirectResponse($assignment->getPlayerUrl(ActivityManager::ASSIGNMENT))
    );
  }
}
