<?php

namespace Drupal\capytale_ui;

use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Security\TrustedCallbackInterface;

class LazyBuilderService implements TrustedCallbackInterface {

  public static function TrustedCallbacks() {
    return ['render'];
  }

  /**
   * @var FormBuilderInterface $form_builder
   */
  protected $form_builder;

  public function __construct(FormBuilderInterface $form_builder) {
    $this->form_builder = $form_builder;
  }

  public function render($formId) {
    return $this->form_builder->getForm($formId);
  }
}
