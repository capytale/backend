# capytale

## Basé sur Drupal 9.
Ne sont commités sur ce repo que les fichiers ajoutés ou modifiés (hors paramètres de sécurité) par rapport à un drupal 9 de base.

    
    ├── config
    │   └── staging
    ├── web
    └── wwwroot
### config/staging 
Ce répertoire contient l'export de la config Drupal normalement stockée en BDD.
La commande `drush config:export` ou `drush cex` permet d'exporter la config sous forme de fichiers plats
qui peuvent être commités.
On peut restaurer avec la commande `drush config:import` ou `drush cim`.

### web
Ce répertoire contient le code source.

    web
    ├── libraries
    ├── modules
    │   └── custom
    └── themes
        └── capytale5
#### web/modules/custom
Les modules métier développés spécialement pour Capytale

    web/modules/custom
    ├── capytale
    ├── capytale_activity
    ├── capytale_bib
    ├── capytale_ui
    └── tag_tree

#### web/themes/capytale5
Le thème de capytale basé sur bootstrap5

### wwwroot
DocumentRoot du serveur http



# Installation
## Installation de LAMP (si nécessaire)

    sudo apt install apache2 php8.1 mariadb-server phpmyadmin

Module rewrite pour clean URLs

    a2enmod rewrite
    sudo vim /etc/apache2/sites-enabled/000-default.conf

    <Directory /path/to/web/dir>
      Options Indexes FollowSymLinks
      AllowOverride All
      Require all granted
    </Directory>

    sudo systemctl restart apache2

## Installation de composer

    php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
    php -r "if (hash_file('sha384', 'composer-setup.php') === '55ce33d7678c5a611085589f1f3ddf8b3c52d662cd01d4ba75c0ee0459970c2200a51f492d557530c71c15d8dba01eae') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
    php composer-setup.php
    php -r "unlink('composer-setup.php');"
    sudo mv composer.phar /usr/local/bin/composer

## Installer la commande Drush

    wget https://github.com/drush-ops/drush-launcher/releases/latest/download/drush.phar
    chmod +x drush.phar
    sudo mv drush.phar /usr/local/bin/drush
    sudo chown root: /usr/local/bin/drush

## Cloner capytale-frontend

    mkdir capy && cd capy 
    git clone git@gitlab.in.ac-paris.fr:capytale/FrontEndCapytale.git .
    cd capy

## Installation de Drupal

### Dépendances

    composer i

### Application des droits unix adaptés

[voir](https://www.drupal.org/node/244924#script-based-on-guidelines-given-above)

    sudo scripts/fix-permissions.sh --drupal_path=web --drupal_user=USER --httpd_group=www-data

### Configuration Base de Données

Renseigner les paramètres de l'accès au serveur de base de données dans :

    web/sites/default/settings.php

### Procédure d'installation Drupal

    sudo runuser -u www-data -- drush site:install

Noter les identifiants de l'utilisateur administrateur

## Capytalification du drupal

### Réglages suppl sur la conf

    drush edel shortcut && drush edel shortcut_set

### Exporter la config de base

    drush cex

### Régler la conf du site

    drush config:set system.site name Capytale
    drush config:set system.site page.front '/home'
    drush capytale:create-capytale-links

### Importer la config Capytale

    mv config/staging config/staging_tmp
    mkdir config/staging
    drush cex
    cp config/staging/system.site.yml config/staging_tmp/
    rm -rf config/staging
    mv config/staging_tmp config/staging
    drush cim

### Utilisateurs

    drush user:password admin "MY_STRONG_PA$$WORD"
    drush user:role:add 'teacher' admin
    drush user:create proftest --mail='proftest@example.com' --password='azerty'
    drush user:role:add 'teacher' proftest

